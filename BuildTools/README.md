# Performance Enhancing Proxy (PEP) Installer

# Windows Installation

## Additional Apps Bundled with Installer

* **Microsoft Visual C++ 2008 x64 runtime**: https://www.microsoft.com/en-us/download/details.aspx?id=26368
* **Microsoft Visual C++ Compiler for Python 2.7**: https://www.microsoft.com/en-us/download/details.aspx?id=44266
* **RabbitMQ**
* **Erlang**

All of the above prerequisites are bundled with the installer.

## Standard MSI Installation

Double click on **gpep_with_dependencies.exe**, then follow through the prompts to install GPEP.

**Note** - a default password is setup for the Windows service account. To update the password run these commands:

* `net.exe user [service account name] [new password]`
* `sc.exe config "[servicename]" obj= "[.\service account name]" password= "[new password]"`


## MSI Silent Installation

The following properties may be set via the command line:

| Property Name    | Default     | Description |
| -----------------|-------------|-------------|
| SvcName          | GPEP        | The windows service name |
| SvcUser          | GPEP        | The windows service account username (will be created on install) |
| SvcUserPwd       |             | The password for the windows service account (must meet min security requirements). |

They may be set with the following command line:
`gpep_with_dependencies.exe -silent SvcName=John_GPEP SvcUser=Johnny SvcUserPwd=veryextrasupersecretpassword%234!!`

Any of the property fields may be omitted, in which case those properties will assume their default values.

Note:  Make sure to run from a shell with Administrator privileges!

# Configuration

The configuration file is installed by default to `C:\ProgramData\GPEP\config\pep.cfg`. The following settings are provided:


| Section | Setting | Allowed Values | Description |
| ------- | ------- | -------------- | ----------- |
| Environment | configuration | standalone, * | If standalone is specified, the login requirement will be disabled. |
| Directories | apps | A directory path | Where service yaml configuration files will be created. |
| Directories | cache | A directory path | Where service cached tiles are stored. |
| Directories | logs | A directory path | Where the logs are stored. |
| Directories | templates | A directory path | Where service yaml configuration file templates are stored. |
| Files | default_yaml | Path to default.yaml | The default service yaml template. |
| Files | mapproxy-seed  | Path mapproxy-seed tool. | The full path to the MapProxy seed tool. |
| Classification | text  | * | The classification and restriction text. |
| Classification | bgcolor | Color hex code e.g. white = ffffff | Color value for classification banner background. |
| Classification | fgcolor | Color hex code e.g. white = ffffff | Color value for classification banner text. |

## Operating behind a reverse proxy

When operating behind a reverse proxy using HTTPS make sure to set `url_scheme` in `C:\Program Files\gpep\wsgi-app.py` to `https` like this:

    serve(mapproxyapp, port='7080', url_scheme='https', url_prefix='/gpep')

## Cache location

The location of the cache is setup in the config file. Ensure the account that runs the service has read/write/delete privilges to the new location if it is changed.

## Logs

The server and seed logs are stored in the log directory specified in the configuration file.

## Disabling Authentication

When ArcGIS Portal is not present authentication may be disabled by editing the configuration file setting `configuration` to `standalone` in the `Environment` section of the configuration file.

# Building the Installer
We use Jenkins to manage our builds.

* **To trigger a build** push to master.
* **To retrieve a build** navigate to "\10.0.3.8\gpep\pep_dist" (can be done using the a File Explorer)
* **To check on build progress**, in a browser go to: http://10.0.3.71/job/PEP/

The gpep_with_dependencies.exe installer file is tested to work on not locked-down Windows 7. It has not been tested on the military's locked down Windows 7.

## Manually Triggering a Building
Go into the Jenkins Windows machine through VCENTER.

Link to the Jenkins Machine:
https://gfe-vcenter2.rgi-corp.local:9443/vsphere-client/?csp#extensionId=vsphere.core.vm.gettingStarted;context=com.vmware.core.model%3A%3AServerObjectRef~CA271398-F379-4057-8CD9-6213806EB4F4%3AVirtualMachine%3Avm-2965~core

In C:/wksp/pep/BuildTools/platform/win, run make-all.ps1.  This will generate gpep_with_dependencies.exe in C:/wksp/pep/BuildTools/dist.

If the build machine is branch new, then to set it up:

  1. Install NodeJS
  2. Install the Wix Installer tools
  3. Run install-env.ps1 (with admin rights)
  4. run make-all.ps1 to build the installer
