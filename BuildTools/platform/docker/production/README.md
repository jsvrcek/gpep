TO BUILD GPEP DOCKER:

First: install-gpep.sh contains a reference to <repo_path> which needs to be updated with your repo address.

Second: Environment variables for $GITLAB_USERNAME and $GITLAB_BUILD_TOKEN need to be set.

Third: run the build-command script to build the docker file. note this will create an image tagged as gpep. feel free
        to update that as you please.


