NOTE: Docker deployment is still a work in progress.

Setting up docker deployment for GPEP

For the back end:

 - Building:
    - Run the following command: docker build -t <repository_name>:<image_name> --build-arg
                                                 GITLAB_USERNAME=<GITLAB_USERNAME>
                                                 --build-arg GITLAB_BUILD_TOKEN=<GITLAB_BUILD_TOKEN> .
      <repository_name> is the name of/path to the docker registry you are using.
      <image_name> is the name of the image.
      <GITLAB_USERNAME> is the name you want to use to authenticate yourself with GitLab
      <GITLAB_BUILD_TOKEN> is a token used to authenticate your GitLab user

      Be in the directory with the correct dockerfile in it. If the file name is different from "Dockerfile", -f must
      be used to specify a filename.

      This will build and add the image to the specified repository under the specified name. The image has an
      entrypoint built in, so manually setting an entrypoint in a docker compose file is not necessary.

For the front end:

 - Building:
    - Run the following command: docker build -t <repository_name>:<image_name>
                                                 --build-arg GITLAB_USERNAME=<GITLAB_USERNAME>
                                                 --build-arg GITLAB_BUILD_TOKEN=<GITLAB_BUILD_TOKEN> .
      See the description on building the back end for an explanation of what all the variables mean and steps on how to
      build.

Running GPEP:

 - Use docker-compose.yml to run GPEP. cd into the directory with that file and type "docker-compose up". The value
   after "image:" must be changed to <repository name/path>:<image name>.
