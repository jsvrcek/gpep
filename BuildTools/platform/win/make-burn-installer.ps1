# The burn installer bundles together several dependency installers, along
# with the actual standalone GPEP installer, all into one exe installer package.

# NOTE!!!! this requires that the GPEP installer has already been created.  All this
# script does is take that already created gpep.msi installer, and bundle it with 
# dependencies.  If you wish to pump out a new burn installer which reflects changes
# to the pep repo, use make-all.ps1

# Bundled Installers:
#  - Erlang
#  - RabbitMQ
#  - Visual C++ for Windows
#  - Visuall C++ for Pyton 2.7

param (
    [string]$build = ""
)

try {
    Write-Output "Initiating the burn installer make process."

    # imports constants for directory locations
    $script_dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
    . "$script_dir\paths.ps1"
    
    # ensure all dependency installers are downloaded
    & "$script_dir\download-dependencies.ps1" | Out-Null
  
    ################################################## Build Burn Installer EXE #############################################################
    
    $env:WORKSPACE = $buildtools_dir
    
    & $wix_candle -ext WixUtilExtension -ext WixBalExtension -arch x64 -out "$burn_build_dir\" "$wix_dir\pep_burn_installer.wxs"  
    & $wix_light  -ext WixBalExtension -out "$dist_dir\gpep_with_dependencies_build-$build.exe" -b "$burn_build_dir\prereq_installers" "$burn_build_dir\pep_burn_installer.wixobj"
    Write-Output("gpep_with_dependencies_build-$build.exe installer file exported to $dist_dir")
    
} catch {
	Write-Output ("Make failed, the error was:`n`n'"+ 
                $_.Exception.Message + "`n" +
                $_.InvocationInfo.PositionMessage  
    )
	exit 1
}