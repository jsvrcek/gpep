﻿
# ensures that this script has admin rights
If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
    [Security.Principal.WindowsBuiltInRole] “Administrator”))
{
    Write-Warning “You do not have Administrator rights to run this script.`nPlease re-run this script as an Administrator.”
    Write-Output "Press any key to exit."
    $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
    Break
}

# asks the user to type yes or no ('y' or 'n')
function Did-User-Type-Yes ($message) {
    $yes_or_no = 'undecided'
    do {
         $yes_or_no = Read-Host -Prompt ($message)
         if(($yes_or_no -eq 'y') -Or ($yes_or_no -eq 'n')) {
                $valid_response = $TRUE
         } else {
                $valid_response = $FALSE
                Write-Host 'Invalid.  Type y or n.'
         }
    } until ($valid_response)

    return $yes_or_no -eq 'y'
}


$base_uninstall_message = '  Type "y" to uninstall, type "n" to keep.  Finally, press enter'
$uninstall_gpep     = $TRUE
$uninstall_erlang   = Did-User-Type-Yes('Uninstall Erlang?  ' + $base_uninstall_message)
$uninstall_rabbitmq = Did-User-Type-Yes('Uninstall RabbitMQ?' + $base_uninstall_message)

echo ""
if($uninstall_gpep)     {echo "GPEP will be uninstalled."}     else {echo "GPEP will be kept."}
if($uninstall_erlang)   {echo "Erlang will be uninstalled."}   else {echo "Erlang will be kept."}
if($uninstall_rabbitmq) {echo "RabbitMQ will be uninstalled."} else {echo "RabbitMQ will be kept."}
echo ""

if(-Not (Did-User-Type-Yes "If the above is correct, press 'y' to proceed. Press 'n' to exit")) {
    exit
}

#################### GPEP Uninstaller #####################

# gets the app handle for the uninstaller with name $name in the control panel
function Get-App($name) {
    $app = Get-WmiObject -Class Win32_Product | Where-Object {
        $_.Name -match “$name”
    }
    return $app
}

if($uninstall_gpep) {
    # stop the GPEP service
    Stop-Service 'GPEP' | Out-Null
    Start-Sleep -s 2
    Write-Output "GPEP Service stopped."

    # remove the GPEP service
    $nssm = "C:\Program Files\GPEP\nssm.exe"
    & $nssm stop GPEP | Out-Null
    & $nssm remove GPEP confirm | Out-Null
    Write-Output "GPEP Windows Service removed."
}


if($uninstall_rabbitmq) {
    Stop-Service 'RabbitMQ'

    Write-Output "Running Windows uninstall process for RabbitMQ"
    
    & "C:\Program Files\RabbitMQ Server\uninstall.exe" /S | Out-Null
    
    Write-Output "Manually clearing any leftover files."
    Remove-Item -Force -Recurse -Path 'C:\Program Files\RabbitMQ Server\*' | Out-Null
    Remove-Item -Force -Recurse -Path 'C:\Program Files\RabbitMQ Server' | Out-Null
    Write-Output "RabbitMQ Service 3.6.5 Uninstalled."
}

if($uninstall_erlang) {

    Write-Output "Running Windows uninstall process for GPEP"
    Write-Output "Force killing all Erlang related processes."

    # low level force killing is needed for all Erlang related process
    # these buggers are nasty
    & cmd.exe /c "wmic process where name=`"erl.exe`" delete"    | Out-Null
    & cmd.exe /c "wmic process where name=`"erlsrv.exe`" delete" | Out-Null
    & cmd.exe /c "wmic process where name=`"epmd.exe`" delete"   | Out-Null
    
    Write-Output "Erlang processes ended (erl.exe, erlsrv.exe, epmd.exe)."
    Start-Sleep -s 2

    #remove Erlang firewall exceptions on uninstall
    & cmd.exe /c "netsh.exe advfirewall firewall delete rule name=`"ErlEXE`""
    & cmd.exe /c "netsh.exe advfirewall firewall delete rule name=`"EpmdEXE`""
    Write-Output "Erlang firewall exceptions removed."

    Write-Output "Running Erlang's builtin installer."
    & "C:\Program Files\erl8.2\Uninstall.exe" /S | Out-Null

    # make sure that the uninstaller didn't leave anything behind. necessary, windows uninstall doesn't always work.
    Write-Output "Manually clearing any leftover files."
    Start-Sleep -s 2
    Remove-Item -Path "C:\Program Files\erl8.2" -Force -Recurse | Out-Null
    Start-Sleep -s 2
    & cmd.exe /c "del /s /q `"C:\Program Files\GPEP\*.*`"" | Out-Null
    Start-SLeep -s 2
    & cmd.exe /c "rmdir /s /q `"C:\Program Files\erl8.2`"" | Out-Null
    Write-Output "Erlang OTP 19 (8.1) Uninstalled."
}

