# this script downloads the several external dependencies required by GPEP

# imports constants for directory locations
$script_dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
. "$script_dir\paths.ps1"
   
# necessary for downloading files from the internet
Import-Module BitsTransfer

Write-Output "Checking that all required dependencies are downloaded..."

# if the directory for holding the downloaded installers does not exist, create it
if (-Not (Test-Path $burn_installer_dependencies_dir)) {
    New-Item -ItemType Directory -Path $burn_installer_dependencies_dir -Force
}

# for each installer, check if it's already downloaded.  if it's not, then download it.

# erlang
if (-Not (Test-Path "$burn_installer_dependencies_dir\$erlang_filename")) {
    Write-Output "Downloading the Erlang installer."
    Start-BitsTransfer -Source $erlang_url `
    -Destination "$burn_installer_dependencies_dir\$erlang_filename" `
    -Description "Downloading the Erlang installer."
}

# rabbitmq
if (-Not (Test-Path "$burn_installer_dependencies_dir\$rabbitmq_filename")) {
    Write-Output "Downloading the RabbitMQ installer."
    Start-BitsTransfer -Source $rabbitmq_url `
    -Destination "$burn_installer_dependencies_dir\$rabbitmq_filename" `
    -Description "Downloading the RabbitMQ installer."
}

# visual c++ for windows
if (-Not (Test-Path "$burn_installer_dependencies_dir\$vcpp_for_windows_filename")) {
    Write-Output "Downloading the Visual C++ For Windows installer."
    Start-BitsTransfer -Source $vcpp_for_windows_url `
     -Destination "$burn_installer_dependencies_dir\$vcpp_for_windows_filename" `
     -Description "Downloading the Visual C++ For Windows installer."
}

# visual c++ for python
if (-Not (Test-Path "$burn_installer_dependencies_dir\$vcpp_for_python_filename")) {
    Write-Output "Downloading the Visual C++ For Python 2.7 installer."
    Start-BitsTransfer -Source $vcpp_for_python_url `
     -Destination "$burn_installer_dependencies_dir\$vcpp_for_python_filename" `
     -Description "Downloading the Visual C++ For Python 2.7 installer."
}

# visual c++ 2013 for Erlang
if (-Not (Test-Path "$burn_installer_dependencies_dir\$vcpp_2013_filename")) {
    Write-Output "Downloading the Visual C++ 2013 installer."
    Start-BitsTransfer -Source $vcpp_2013_url `
     -Destination "$burn_installer_dependencies_dir\$vcpp_2013_filename" `
     -Description "Downloading the Visual C++ 2013 installer."
}

# winpython
if (-Not (Test-Path "$burn_installer_dependencies_dir\$winpython_filename")) {
    Write-Output "Downloading WinPython."
    $wc = New-Object System.Net.WebClient
    $wc.DownloadFile($winpython_url, "$burn_installer_dependencies_dir\$winpython_filename")
}

# FIXME node installer not working in silent mode.  has been manually installer for now.
# in the future, running install-env.ps1 should automatically ensure that both wix and nodejs are installed
# node
#if (-Not (Test-Path "$burn_installer_dependencies_dir\$node_filename")) {
#    Write-Output "Downloading NodeJS"
#    Start-BitsTransfer -Source $node_url `
#     -Destination "$burn_installer_dependencies_dir\$node_filename" `
#     -Description "Downloading the Node v6.9.2-x64 installer."
#}

