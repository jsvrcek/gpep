REM This file gets run at the beginnig of the installer process by the Wix burn installer
REM Gives two Erlang Programs access to get past the firewall: erl.exe and epmd.exe

netsh.exe advfirewall firewall add rule name="EpmdEXE" program="C:\program files\erl8.2\erts-8.2\bin\epmd.exe" protocol=tcp dir=in enable=yes action=allow
netsh.exe advfirewall firewall add rule name="ErlEXE"  program="C:\program files\erl8.2\bin\erl.exe"           protocol=tcp dir=in enable=yes action=allow

