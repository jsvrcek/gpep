﻿# sets up the installer build machine with a proper python environment,
# so the installer can be built

Param([bool]$quickInstall=$false)

# imports constants for directory locations
$script_dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
. "$script_dir\paths.ps1"

# ensure all dependency installers are downloaded
& "$script_dir\download-dependencies.ps1" | Out-Null

# shortcuts to visual C++ installer files
$vcpp_for_windows_installer = "$burn_installer_dependencies_dir\$vcpp_for_windows_filename"
$vcpp_for_python_installer = "$burn_installer_dependencies_dir\$vcpp_for_python_filename"
$node_installer = "$burn_installer_dependencies_dir\$node_filename"

# delete GPEP from Program Data
if (Test-Path $pep_dir) {rmdir -force -Recurse $pep_dir}

############################### Remove old python installations ###############################

# Stops and removes the GPEP Windows Service
$svc = Get-Service -Name GPEP -EA 0
if ($svc) {$svc | Stop-Service -force; sc.exe delete GPEP}
$svc_path = 'HKLM:\SYSTEM\CurrentControlSet\services\GPEP'
if (Test-Path $svc_path) {Remove-Item $svc_path -Force -Recurse}

# Uninstalls previously installed python dependencies
& cmd.exe /c "$vcpp_for_windows_installer" /qu
& cmd.exe /c msiexec.exe /x "$vcpp_for_python_installer" /qb

#if (Test-Path "${dl_dir}\vcredist_x64.exe") {& cmd.exe /c "${dl_dir}\vcredist_x64.exe" /qu}
#if (Test-Path "${dl_dir}\VCForPython27.msi") {& cmd.exe /c msiexec.exe /x "${dl_dir}\VCForPython27.msi" /qb}

################################## Installation of Python ######################################

# Get windows  pre-req

# makes sure you can write to a download directory
if (Test-Path $dl_dir) {$tdir = gi $dl_dir} else {$tdir = New-Item -ItemType Directory -Path $dl_dir -Force}
[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true } 
$wc = New-Object System.Net.WebClient

# Install Windoes pre-reqs
& cmd.exe /c "$vcpp_for_windows_installer" /q
& cmd.exe /c msiexec /i "$vcpp_for_python_installer" /qb
# not installer not working in silent mode, I have manually installed it
#& cmd.exe /c msiexec /i "$node_installer" /qn
