 # running this script will generate an msi in the BuildTools/dist directory, bundling
# GPEP with all dependencies

# it first builds the standalone GPEP installer, then packages that into the bundled
# installer

param (
    [string]$build = ""
)

Write-Output "Beginning the make process for the GPEP installer."
Write-Output "Build $build"

$script_dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition

# ensure all dependency installers are downloaded
& "$script_dir\download-dependencies.ps1" | Out-Null

# make the GPEP installer (installs only GPEP)
& "$script_dir\make-gpep-installer.ps1"

# make the bundled installer (bundles the GPEP installer together with all its required dependencies)
$params = @{build=$build}
& "$script_dir\make-burn-installer.ps1" @params