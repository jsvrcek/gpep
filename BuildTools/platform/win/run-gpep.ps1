# current location of script
$script_dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition

# actual python distribution included with the winpython
$pyexe = "$script_dir\winpython\python-2.7.12.amd64\python.exe"

$env:PYTHONHOME = "$script_dir\winpython\python-2.7.12.amd64"

# cleans the django database
# assumes that gpepsvc.py is in the same directory as this powershell script
& $pyexe "$script_dir\gpepsvc.py" --start --production