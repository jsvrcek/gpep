﻿# Builds the GPEP .msi installer file
# (only installs GPEP, not the various dependencies it requires)

$ErrorActionPreference = 'Continue'

try {
    Write-Output "Initiating the make process for the GPEP component installer, without dependencies."

    # imports constants for directory locations
    $script_dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
    . "$script_dir\paths.ps1"

    #ensure that npm is installed on the system
    if (-Not (Test-Path "$npm")) {
        Write-Error "npm not found at $npm.  Please install NodeJS: https://nodejs.org/en/download/"
        exit 1
    }

    # ensure all dependency installers are downloaded
    & "$script_dir\download-dependencies.ps1" | Out-Null

    # ensure mapproxy exists
    if(-Not (Test-Path $mapproxy_dir)) {
        Write-Error "$mapproxy_dir not found.  Please clone nsg-mapproxy to that location, then restart the build process."
        exit 1
    }

    # ensure winpython is fully download before proceeding
    while (-Not (Test-Path "$burn_installer_dependencies_dir\$winpython_filename")) {
        Write-Output("Waiting for WinPython download to complete...")
        Start-Sleep -s 5
    }
    Write-Output "WinPython download complete."

    ####################################### Clear out some old directories ##########################################
   
    # delete the previously built winpython environment (we're starting from scratch), then reinstall it
    if (Test-Path $winpython_dir) {rmdir -force -Recurse $winpython_dir}            #uninstalls WinPython
    & "$burn_installer_dependencies_dir\$winpython_filename" /S /D="$winpython_dir" | Out-Null #installs WinPython
    
    # ensure that the build directory exists in the repo
    if (! (Test-Path $build_dir)) {New-Item -ItemType Directory -Path $build_dir -Force | Out-Null}

    # ensure the wix build heat directory exists and is empty
    if (! (Test-Path $build_wixheat_dir)) {New-Item -ItemType Directory -Path $build_wixheat_dir -Force | Out-Null}
    rm "$build_wixheat_dir\*" -recurse -force -ea 0

    ######################### Pip Install dependencies ####################################

    Set-Location "$repo_dir"

    # ensure winpython is fully installed before proceeding
    while (!(Test-Path "$winpython_dir\scripts\env.bat") -Or !(Test-Path "$pyexe")) {
        Write-Output("Waiting for WinPython installation to complete...")
        Start-Sleep -s 5
    }
    
    Write-Output("WinPython installation complete.");
    
    # WinPython sets environmental python variables
    & "$winpython_dir\scripts\env.bat"

    # BUG:  cannot upgrade pip, causes pip to uninstall for unknown reason
    #& $pyexe -m pip install -U pip # ensures pip is upgraded

    # remove gpep if it wasn't already uninstalled above
    #if ($quickInstall)
    #{
    #    & $pip uninstall -y gpep
    #}

    ############# Reinstall GPEP #############

    # uninstall GPEP
    & $pyexe -m pip uninstall -y gpep

    #install python requirements for gpep and nsg-mapproxy, into the WinPython environment
    & $pyexe -m pip install -r "$repo_dir\requirements.txt"

    # reinstall GPEP
    & $pyexe -m pip install "$modules_dir\gpep"

    ########### Reinstall MapProxy ###########

    # uninstall mapproxy
    & $pyexe -m pip uninstall -y mapproxynsg

    # reinstall mapproxy
    & $pyexe -m pip install "$mapproxy_dir"
    
    
    ########################## Build Frontend, copy to static directory in GPEP Module ##########################

    # set up frontend files
    Set-Location $client_dir

    & cmd.exe /c $npm install bower -g
    & cmd.exe /c $npm install
    #& cmd.exe /c bower install
    & cmd.exe /c $npm run build-app-css
    
    # move frontend files to static directory so they can be served from there
    New-Item -ItemType Directory -Path "$frontend_static_dir" -Force
    Copy-Item -Path "$client_dir\app" -Destination "$frontend_static_dir" -Recurse -Force
    Copy-Item -Path "$modules_static_dir\*" -Destination "$frontend_static_dir" -Recurse -Force
  
    ####################### Transfer winpython to harvesting directory #############################

    # copy the built winpython directory into the wixheat building directory, for wix heat harvesting
    #### & robocopy source destination [flags which suppress tons of verbose output]
    & robocopy $winpython_dir $build_wixheat_dir /e /NFL /NDL /nc /ns

    ###################################### Wix Building ###########################################

    # ensure the distribution directory exists
    if (! (Test-Path $dist_dir)) {
        New-Item -ItemType Directory -Path $dist_dir -Force | Out-Null
    }
    
    # delete gpep.msi if it exists in the distribution directory
    if (Test-Path "$dist_dir\gpep.msi") {
        Remove-Item "$dist_dir\gpep.msi" -Force
    }
 
    # wix build process, which ultimately produces an msi installer for gpep
    & $wix_heat dir $build_wixheat_dir -cg pythonfiles -srd -gg -template fragment -out "$build_dir\pythonfiles.wxs" -sw5150 -dr WinPythonLocation -var "env.WixHeatDir"
    & $wix_candle -ext WixUtilExtension -arch x64 -out "$build_dir\" "$wix_dir\pep.wxs" "$build_dir\pythonfiles.wxs"
    & $wix_light  -out "$dist_dir\gpep.msi" -b "$build_dir" -ext WixUtilExtension "$build_dir\pep.wixobj" "$build_dir\pythonfiles.wixobj"
    Write-Output ("gpep.msi exported to $dist_dir")

} catch {
	Write-Output ("Make failed, the error was:`n`n'"+ 
                    $_.Exception.Message + "`n" +
                    $_.InvocationInfo.PositionMessage
                )
	exit 1
}
