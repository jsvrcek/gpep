REM This script is used by the wix uninstaller, makes a call out to the powershell uninstall script for the GPEP Bundle

REM Triggers main uninstall script for GPEP.  Manages uninstallation of RabbitMQ and Erlang
start /WAIT powershell -executionpolicy unrestricted "& """C:\Program Files\GPEP\uninstall.ps1"""

REM Removes all files from Program Files\GPEP
timeout /T 2 /NOBREAK > NUL
del /s /q "C:\Program Files\GPEP\*.*"
timeout /T 2 /NOBREAK > NUL
rmdir /s /q "C:\Program Files\GPEP"

REM Removes all files from ProgramData\GPEP
timeout /T 2 /NOBREAK > NUL
del /s /q "C:\ProgramData\GPEP*.*"
timeout /T 2 /NOBREAK > NUL
rmdir /s /q "C:\ProgramData\GPEP"

REM Removes Erlang cookies
del /s /q "C:\WINDOWS\.erlang.cookie"
del /s /q "%HOMEDRIVE%%HOMEPATH%\.erlang.cookie"
