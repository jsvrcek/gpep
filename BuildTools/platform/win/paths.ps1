# Here are predefined paths relating to the installation process for GPEP #

$script_dir = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition

# project root directory, for pep.
$repo_dir = (Resolve-Path "$script_dir\..\..\..").Path

# all files related to production deployment are stored here
$buildtools_dir = "$repo_dir\BuildTools"

$mapproxy_dir = (Resolve-Path "$repo_dir\..\nsg-mapproxy").Path

# html, css, and js files relating to the frontend
$client_dir = $repo_dir + '\Client'


$dl_dir = $env:tmp
$pep_dir = 'C:\ProgramData\GPEP'
$reqs_dir = Join-Path $script_dir reqs
$req_modules_dir = Join-Path $reqs_dir modules
$modules_dir = "$repo_dir\Modules"
$modules_static_dir = "$modules_dir\gpep\gpep_apps\static"

$winpython_building_dir = "$buildtools_dir\winpython_building_env"
# folder where winpython installs a few utilities
$winpython_dir = "$winpython_building_dir\winpython"

# actual python distribution included with the winpython
$py_dir = $winpython_dir + '\python-2.7.12.amd64'

# all frontend files are copied into here for final production deployment,
# as CherryPy serves the frontend files from this directory
$frontend_static_dir = "$py_dir\Lib\site-packages\gpep_apps\static"

#python paths
$pip =   "$py_dir\Scripts\pip.exe"
$pyexe = "$py_dir\python.exe"         # path to WinPython's python.exe

# for some reason, Jenkins powershell cannot access the Windows Path variable
# on our target install machine, so we need to hardcode the path to npm
$npm = 'C:\Program Files\nodejs\npm'

# where all wix-related things are kept
$build_dir = Join-Path $repo_dir 'build'

# we put big directories required for installation here. we have wix heat harvest this
# directory into a .wxs file.
# note:  wix heat is used to generate a .wxs file based on the file hierarchies of directories passed
# to it, so we don't have to manually specify hierarchies
$build_wixheat_dir = Join-Path $build_dir 'wixheat'

# where all .wsx (wix xml installer description files) are stored
$wix_dir = Join-Path $script_dir 'wix'

# where the intermetiate wix files for the burn installer are stored
# also, where the burn installer files are downloaded and stored
$burn_build_dir = "$repo_dir\BuildTools\burn_build"

$burn_installer_dependencies_dir = "$burn_build_dir\prereq_installers"

# where the final installer packages are exported
$dist_dir = "$repo_dir\BuildTools\dist" 

$build_num = $env:BUILD_NUMBER
if (!$build_num) {$build_num = 0}
# windows installer uses only the first 3 to detect an upgrade, previous will be removed automatically on upgrade
$build_ver = "1.0.${build_num}" 

$env:WixHeatDir = $build_wixheat_dir
$env:BuildVersion = $build_ver
$env:WORKSPACE = $buildtools_dir

################ TODO TODO TODO
#### get environmental variables to work
try {
    #$wix_heat = (gcm "$env:WIX\bin\heat.exe").Definition
    #$wix_candle = (gcm "$env:WIX\bin\candle.exe").Definition
    #$wix_light = (gcm "$env:WIX\bin\light.exe").Definition
    
    $wix_env_dir = "C:\Program Files (x86)\WiX Toolset v3.10"
    $wix_heat = (gcm "$wix_env_dir\bin\heat.exe").Definition
    $wix_candle = (gcm "$wix_env_dir\bin\candle.exe").Definition
    $wix_light = (gcm "$wix_env_dir\bin\light.exe").Definition
} catch { 
    $ErrorMessage = $_.Exception.Message
    $FailedItem = $_.Exception.ItemName
    throw 'Could not find one more required wix tools. error message: ' + $ErrorMessage
}

###################################### for download_dependencies.ps1 #####################################

# urls for various installers of dependencies for GPEP
$erlang_url   = 'http://erlang.org/download/otp_win64_19.2.exe'
$rabbitmq_url = 'https://www.rabbitmq.com/releases/rabbitmq-server/v3.6.6/rabbitmq-server-3.6.6.exe'
$vcpp_for_windows_url = 'https://download.microsoft.com/download/5/D/8/5D8C65CB-C849-4025-8E95-C3966CAFD8AE/vcredist_x64.exe'
$vcpp_for_python_url = 'https://download.microsoft.com/download/7/9/6/796EF2E4-801B-4FC4-AB28-B59FBF6D907B/VCForPython27.msi'
$winpython_url = 'https://github.com/winpython/winpython/releases/download/1.7.20161101/WinPython-64bit-2.7.12.4Zero.exe'
$node_url = 'https://nodejs.org/dist/v6.9.2/node-v6.9.2-x64.msi'
$vcpp_2013_url = 'https://download.microsoft.com/download/2/E/6/2E61CFA4-993B-4DD4-91DA-3737CD5CD6E3/vcredist_x64.exe'

# file name for each installer file
$erlang_filename = 'otp_win64_19.2.exe'
$rabbitmq_filename = 'rabbitmq-server-3.6.6.exe'
$vcpp_for_windows_filename = 'vcredist_x64.exe'
$vcpp_for_python_filename = 'VCForPython27.msi'
$winpython_filename = 'WinPython-64bit-2.7.12.4Zero.exe'
$node_filename = "node-v6.9.2-x64.msi"
$vcpp_2013_filename = 'vcredist_x64_2013.exe'