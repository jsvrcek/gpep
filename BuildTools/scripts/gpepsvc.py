# windows installer

import argparse
import cherrypy
import django
import logging
import os
import sys

from django.core.management import call_command
from subprocess import Popen
from gpep_apps.pepapi.celery import app as celery_app


def setup_django():
    """Initializes django"""
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gpep_apps.pepapi.settings")
    django.setup()


def setup():
    from gpep_apps.pepapi.wsgi import application as app
    # in practice, you will want to specify a value for
    # log.error_file below or in your config file.  If you
    # use a config file, be sure to use an absolute path to
    # it, as you can't be assured what path your service
    # will run in.

    from gpep_apps.config import gpepconfig as conf, models
    # this is to make sure the config model instance gets recreated on startup. if that isn't done, updates to the
    # config won't take effect
    models.Config.objects.all().delete()

    config_object = conf.instance()
    needed_paths = [config_object.app_dir, config_object.cache_dir, config_object.log_dir, config_object.template_dir,
                    config_object.temp_dir, config_object.output_dir, config_object.clean_seed_dir]
    for path in needed_paths:
        if not os.path.exists(path):
            os.makedirs(path)

    # for all of GPEP
    gpep_log_file = os.path.join(config_object.log_dir, 'gpep.log')
    django_log_file = os.path.join(config_object.log_dir, 'django.log')


    # for cherrypy
    log_file_server = os.path.join(config_object.log_dir, 'server.log')

    # config file should contain these lines:
    # [/static]
    # tools.staticdir.on = True
    # tools.staticdir.dir = "C:\python27\Lib\site-packages\gpep\static"
    path = os.path.dirname(os.path.abspath(__file__))

    # serve over port specified by GPEP_PORT if it is defined as an environmental variable...
    # otherwise, default to port 8080 if it's not defined
    socket_port = int(os.getenv("GPEP_PORT", 8080))

    print "The GPEP Frontend can be accessed by a browser at:  https://localhost:" + str(socket_port) + "/pep"
    print "If the GPEP server fails to start or the frontend is unvailable, another program may be using that port. "
    print "try changing the port from 8080 to something else, by setting a system environmental variable"
    print "GPEP_PORT to another number, then restarting the GPEP server."

    # set the ssl directory.  location depends on whether in dev environment, or windows production
    if(args['production']):
        ssl_directory = os.path.realpath(os.path.dirname(__file__))
    else:
        ssl_directory = os.path.realpath(os.path.dirname(__file__) + "/../../Client/ssl")
    ssl_cert_path = os.path.join(ssl_directory, "cert.pem")
    ssl_privkey = os.path.join(ssl_directory, "privkey.pem")

    config = {
        'global':{
            'log.screen': False,
            'engine.autoreload.on': False,
            'engine.SIGHUP': None,
            'engine.SIGTERM': None,
            'environment':'embedded',
            # remove any limit on the request body size; cherrypy's default is 100MB
            'server.max_request_body_size': 0,
            'server.socket_port': socket_port,
            'server.ssl_module': 'builtin',
            'server.ssl_certificate': ssl_cert_path,
            'server.ssl_private_key': ssl_privkey,
            # increase server socket timeout to 60s; cherrypy's defult is 10s
            'server.socket_timeout': 60,
            'log.access_file': '',
            'log.error_file': '', # disabled so cherrypy doesn't duplicate errors
        }
    }

    # force kill CherryPy access logging, but allow cherrypy to still log errors
    # comment out this line if you want CherryPy server to log every single GET request (very verbose)
    cherrypy.log.access_log.propagate = False

    cherrypy.config.update(config)

    logging.basicConfig(filename=gpep_log_file,
                        level=logging.WARN,
                        filemode='a',
                        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # makes any module logs in gpep_apps logs to gpep.log
    gpep_handler = logging.FileHandler(gpep_log_file)
    gpep_handler.setFormatter(formatter)
    gpep_logger = logging.getLogger("gpep_apps")
    gpep_logger.addHandler(gpep_handler)

    cherrypy.tree.graft(app, '/gpep')

    # use cherrpy to serve frontend
    from gpep_apps.config.gpepconfig import get_frontend_static_resource_path_production
    from gpep_apps.config.gpepconfig import get_frontend_static_resource_path_development

    # if the environmental variable GPEP_FRONTEND_FILES is set to a valid path, look there for the html/css/js files
    special_location_for_frontend_static_files = os.environ.get('GPEP_FRONTEND_FILES')
    if special_location_for_frontend_static_files is not None and \
            os.path.exists(special_location_for_frontend_static_files):
        frontend_static_files_path = os.path.abspath(special_location_for_frontend_static_files)

    # otherwise, if gpep set to run in windows production mode,
    # look in a special winpython location for the html/css/js files
    elif args['production']:
        # path on a windows production environment (buried in Program Files)
        frontend_static_files_path = get_frontend_static_resource_path_production()

    # otherwise, look in the default PROJECT_ROOT/Client/app for the html/css/js files
    else:
        # path on dev environment (just normal pep/Client spot in the repo)
        frontend_static_files_path = get_frontend_static_resource_path_development()

    cherrypy.tree.mount(None, '/', {'/': {
        'tools.staticdir.dir': frontend_static_files_path,
        'tools.staticdir.on': True,
        'tools.staticdir.index': "index.html"
    }})

    cherrypy.tree.mount(None, '/downloads')


def application(environ, start_response):
    setup()
    return cherrypy.tree(environ, start_response)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--username', help="specify username for rabbitmq")
    parser.add_argument('--password', help="specify password for rabbitmq")
    parser.add_argument('--vhost', help="specify chost for rabbitmq")
    parser.add_argument('--conf', action='store_true', help="configuration file location")
    parser.add_argument('-c', '--cleandb', action='store_true', help="cleans %(prog)s database")
    parser.add_argument('-p', '--populate', action='store_true', help="populates %(prog)s database")
    parser.add_argument('-s', '--start', action='store_true', help="starts %(prog)s")
    parser.add_argument('-x', '--cherrypy-serve-frontend', action='store_true',
                        help="Deprecated.  Does nothing now, as cherrypy always serves frontend.")
    parser.add_argument('-o', '--production', action='store_true',
                        help="Tells CherryPy to serve frontend in production mode.")

    args = vars(parser.parse_args(sys.argv[1:]))
    if args['username']:
        os.environ.setdefault("RABBITMQ_USERNAME", args['username'])

    if args['password']:
        os.environ.setdefault("RABBITMQ_PASSWORD", args['password'])

    if args['vhost']:
        os.environ.setdefault("RABBITMQ_VHOST", args['vhost'])

    if args['conf']:
        os.environ.setdefault("GPEP_CONF", args['conf'])

    setup_django()
    if args['cleandb']:
        call_command('cleandb')
        # purges all jobs from message broker celery is hooked up to
        celery_app.control.purge()

    if args['populate']:
        call_command('populatedb')

    if args['start']:
        # kill any currently running celery workers
        if 'LINUX' in sys.platform.upper():
            os.system("ps auxww | grep 'celery worker' | awk '{print $2}' | xargs kill -9")
        else:
            os.system("taskkill /F /im celery.exe")

        celery_call = "\"{0}\" -m celery -A gpep_apps.pepapi worker -l warning".format(sys.executable)
        Popen(celery_call, shell=True)
        setup()
        call_command('calculatecachesize') # kicks off a celery task to periodically calculate the cache size
        cherrypy.config.update({'server.socket_host':'0.0.0.0'})
        cherrypy.engine.start()
        cherrypy.engine.block()
