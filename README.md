Geospatial Performance Enhancing Proxy (GPEP)
=============================================
[![build status](https://gitlab.com/GitLabRGI/nsg/gpep/badges/master/build.svg)](https://gitlab.com/GitLabRGI/pep/commits/master)

[![coverage report](https://gitlab.com/GitLabRGI/nsg/gpep/badges/master/coverage.svg)](https://gitlab.com/GitLabRGI/pep/commits/master)

What is GPEP?
=============

GPEP, which stands for Geospatial Performance Enhancing Proxy, is designed to
pull in map tiles from a remote server, then serve them back out in an
accelerated fashion.

GPEP's primary behavior can be broken into a few simple steps:
1.	**Connect GPEP to map services.** GPEP is supplied with URL’s (standard OGC web service endpoints) to different map services. This gives GPEP access to map image data from these services.
2.	**GPEP downloads map images from these services**. GPEP is told to download specific geographic regions of map data from these map services. Downloaded map data is “cached”. In GPEP, both “seeding” and “packaging” operations cause GPEP to cache map data. Additionally, if a service has caching enabled, then viewing any data from that service through pep will cache that data automatically
3.	**Perform reprojections/transformations.** (optional) GPEP can perform dynamic reprojections of downloaded map data.
4.	**Serve map data.** GPEP can proxy services (passing through without saving) or serving out cached map data. GPEP acts as a man in the middle, downloading map data, then re-serving (proxying it). It does not need to be connected to the internet to serve map data it already has cached.


View the Repositories in GitLab
===============================

There are two repositories to be concerned with:

1. [gpep](https://gitlab.com/GitLabRGI/nsg/gpep).  This is a convenience/utility wrapper around the map data proxy
server/image engine MapProxy.  It outfits MapProxy with a web API and
a web-based GUI interface, and adds a queuing sytem for MapProxy jobs.

2. [gpep-mapproxy](https://gitlab.com/GitLabRGI/nsg/gpep-mapproxy).  This is the actual map server, which downloads and
stores tile data in caches, then serves that data back out.  It can also convert
the tiles between spatial reference systems.  This is a fork of the main [MapProxy project](https://mapproxy.org/), with our own
custom additions.

Installation Binaries
=====================

We build Windows installers when master is updated, but
these binaries are not currently hosted.  If you have an interest in a binary,
you can contact Reinventing Geospatial.

We also keep updated Docker images, which come with GPEP already set up.

Installation Instructions
=========================

Prerequisites
-------------

* python 2.7
* RabbitMQ
* [pip](https://pip.pypa.io/en/stable/installing/)
* virtualenv (pip install virtualenv)
* note, requirements.txt needs to be synced with the [Docker Repo](https://gitlab.com/GitLabRGI/gpep-docker)

Clone the repositories
----------------------

In a terminal:

```
git clone https://gitlab.com/GitLabRGI/nsg/gpep.git
git clone https://gitlab.com/GitLabRGI/nsg/gpep-mapproxy.git
```
You will see two folders: `gpep`, and `gpep-mapproxy`.

Go into the `gpep` folder. `cd gpep`


## Django backend

```
# create a virtual environement in the root of your gpep directory
pip install virtualenv
virtualenv env

# activate your virtual environment

# windows users
./env/scripts/activate.bat

# mac/linux
source ./env/bin/activate

# you should now see (env) to the left of your command line

# install all gpep python dependencies
# make sure you are at the root of the gpep folder, which you cloned earlier
pip install -r requirements.txt

# install gpep as an editable dependency
pip install -e ./Modules/gpep

# install gpep-mapprxoy as an editable dependency
# cd to the folder where you cloned gpep and gpep-mapproxy
pip install -e gpep-mapproxy
```

Now all the dependencies are installed in the development environment, so it is time to initialize the Django database

```
cd gpep/Modules/gpep
python manage.py cleandb

# optionally, if you want to include existing mapproxy apps into the django db run populatedb
python manage.py populatedb

```
We recommend using an IDE like pycharm to run the backend. In pycharm use the python interpreter found in pep/env/scripts (or pep/env/bin for mac/linux). See instructions [here](https://www.jetbrains.com/help/pycharm/2016.1/configuring-python-interpreter-for-a-project.html) for adding a python interpreter for a project. For the run configuration, run the script pep/Build/scripts/gpepsvc.py

## Initialize Web App
download [node](https://nodejs.org/en/download/)


```
cd gpep/Client

# ensures all frontend javascript libraries are downloaded and up to date
npm install

# ensures that all SCSS files are compiled to CSS files for browser consumption
npm run build
```

## Install RabbitMQ
For windows users, follow the steps [here](https://www.rabbitmq.com/install-windows-manual.html) for windows. Note that first you will need to install erlang.
For linux users, look [here](http://www.rabbitmq.com/download.html)

Follow these instructions to add a user (admin), password(admin), vhost(gpep), and then add the user to the vhost (If you run into issues on Windows, then type
'setx HOMEDRIVE "C:\"' in your cmd prompt to make sure Erlang is accessing your C: drive rather than another default drive like H:).
Right now these values are stored in plain text here, and in the settings.py file. They will need to be encrypted etc before deploying to production.
http://docs.celeryproject.org/en/latest/getting-started/brokers/rabbitmq.html#setting-up-rabbitmq

Note:  Currently, we don't auto-generate a django secret key.  You will need to create a
system environmental variable DJANGO_SECRET_KEY and set it to a random string. Something like:

`DJANGO_SECRET_KEY = 8+-2jd)k$5x75)f8i(ax7!(k!h2u8^l4!2w)gdke_f6$pn418s`

# Run GPEP
* First, ensure the virtual environment is activate:

```
# windows users
gpep/env/scripts/activate.bat

# mac/linux
source gpep/env/bin/activate
```
* Run `python gpep\BuildTools\scripts\gpepsvc.py --start`
* Open browser at https://localhost:8080, see the GPEP user interface
* To turn GPEP off, simply control-c the python process at the terminal.

# Debugging Celery
run Celery
`celery -A gpep_apps.pepapi worker -l info`         

see Celery admin http://localhost:5555/
`flower -A gpep_apps.pepapi --port=5555`
