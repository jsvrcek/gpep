"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import mapproxy
import os
import yaml
from django.core.validators import RegexValidator
from django.db import models

from gpep_apps.api.gpep_exception import GPEPException
from gpep_apps.config import gpepconfig
from gpep_apps.mpas.fields import JSONField

INFO_FORMAT_CHOICES = (
    ('image/png', 'image/png'),
    ('image/jpeg', 'image/jpeg'),
    ('mixed', 'mixed')
)

RESAMPLING_METHOD_CHOICES = (
    ('nearest', 'nearest'),
    ('bilinear', 'bilinear'),
    ('bicubic', 'bicubic')
)

ORIGIN_TYPE_CHOICES = (
    ('nw', 'nw'),
    ('sw', 'sw')
)

DEFAULT_IGNORED_NAMES = ('id', 'map_proxy_app', 'key')
DEFAULT_GRID_NAMES = ('EPSG3395', 'EPSG3857', 'EPSG4326')


def model_to_dict(instance, show_nones=False, ignores=None, **add):
    """
    Dictifies the given model
    :param instance: the instance of the model to dictify
    :param show_nones: if fields that are None should be put into the dict
    :param ignores: fields that should be ignored altogether
    :param add: extra key-value pairs to add to the dict
    :return: the dict of the model
    """
    assert isinstance(instance, models.Model)
    out_dict = add
    if ignores is None:
        ignores = []
    for field in instance._meta.get_fields():
        if field.name not in ignores and field.name not in DEFAULT_IGNORED_NAMES:
            try:
                attr = getattr(instance, field.name)
                if (attr and attr != 'None') or show_nones:
                    if isinstance(attr, YamlModel):
                        attr = attr.to_dict()
                    out_dict[field.name] = attr
            except AttributeError:
                pass
    return out_dict


def _get_source_model_from_str(map_proxy_app, source_str, verify=True):
    """
    Gets the source or cache whose key is the given string
    :param map_proxy_app: the app to get the source from
    :param source_str: the key of the source to get
    :return: the source or cache
    """
    sources = map_proxy_app.get_sources(key=source_str)
    caches = map_proxy_app.get_caches(key=source_str)
    if not verify:
        if caches:
            return caches[0]
        if sources:
            return sources[0]
        return None
    if sources:
        assert len(sources) == 1
        assert not caches
        return sources[0]
    if len(caches) != 1:
        raise GPEPException("Could not find a cache or source with name {}".format(source_str))
    return caches[0]


class YamlModel(models.Model):
    def to_dict(self, **kwargs):
        """
        Gets a dict representation of this model
        :param kwargs: various extra arguments
        :return: a dict with all of the fields in this model
        """
        return model_to_dict(self)

    class Meta:
        abstract = True


class BaseImage(YamlModel):
    mode = models.TextField(blank=True, null=True)
    colors = models.IntegerField(blank=True, null=True)
    transparent = models.NullBooleanField(blank=True, null=True)
    resampling_method = models.CharField(choices=RESAMPLING_METHOD_CHOICES, max_length=16, blank=True, null=True)
    format = models.TextField(blank=True, null=True)
    encoding_options = models.TextField(blank=True, null=True)
    merge_method = models.TextField(blank=True, null=True)

    class Meta:
        abstract = True


class ImageOpts(BaseImage):
    pass


class SourceImage(BaseImage):
    opacity = models.IntegerField(blank=True, null=True)
    transparent_color = models.TextField(blank=True, null=True)
    transparent_color_tolerance = models.FloatField(blank=True, null=True)


class MapServerOpts(YamlModel):
    binary = models.TextField(blank=True, null=True)
    working_dir = models.TextField(blank=True, null=True)


class HTTPOpts(YamlModel):
    method = models.CharField(max_length=16, blank=True, null=True)
    client_timeout = models.IntegerField(blank=True, null=True)
    ssl_ca_certs = models.CharField(max_length=255, blank=True, null=False)
    ssl_no_cert_checks = models.NullBooleanField(blank=True, null=True)
    headers = models.TextField(blank=True, null=True)
    access_control_allow_origin = models.TextField(blank=True, null=True)


class Global(YamlModel):
    image = models.TextField(blank=True, null=True)
    http = models.ForeignKey(HTTPOpts, blank=True, null=True)
    cache = JSONField(blank=True, null=True)
    grid = models.TextField(blank=True, null=True)
    srs = models.TextField(blank=True, null=True)
    tiles = models.TextField(blank=True, null=True)
    mapserver = models.ForeignKey(MapServerOpts, blank=True, null=True)
    renderd = models.TextField(blank=True, null=True)

    def to_dict(self, **kwargs):
        return model_to_dict(self, ignores=('Globals',))


class DemoService(YamlModel):
    pass


class KMLService(YamlModel):
    use_grid_names = models.NullBooleanField(blank=True, null=True)


class TMSService(YamlModel):
    use_grid_names = models.NullBooleanField(blank=True, null=True)
    origin = models.TextField(blank=True, null=True)


class WMTSService(YamlModel):
    kvp = models.NullBooleanField(blank=True, null=True)
    restful = models.NullBooleanField(blank=True, null=True)
    restful_template = models.TextField(blank=True, null=True)
    md = JSONField(blank=True, null=True)


class WMSService(YamlModel):
    srs = JSONField(blank=True, null=True)
    bbox_srs = models.TextField(blank=True, null=True)
    image_formats = models.TextField(blank=True, null=True)
    attribution = models.TextField(blank=True, null=True)
    featureinfo_types = models.TextField(blank=True, null=True)
    featureinfo_xslt = models.TextField(blank=True, null=True)
    on_source_errors = models.CharField(blank=True, null=True, max_length=16)
    max_output_pixels = models.TextField(blank=True, null=True)
    strict = models.NullBooleanField(blank=True, null=True)
    md = JSONField(blank=True, null=True)
    inspire_md = models.TextField(blank=True, null=True)
    versions = models.TextField(blank=True, null=True)


class Service(YamlModel):
    demo = models.ForeignKey(DemoService, blank=True, null=True)
    kml = models.ForeignKey(KMLService, blank=True, null=True)
    tms = models.ForeignKey(TMSService, blank=True, null=True)
    wmts = models.ForeignKey(WMTSService, blank=True, null=True)
    wms = models.ForeignKey(WMSService, blank=True, null=True)

    def to_dict(self, **kwargs):
        return model_to_dict(self, show_nones=True, ignores=('Services',))


class MapProxyApp(YamlModel):
    validator = RegexValidator(r'^[A-Za-z0-9_-]*$', 'Service Names may only contain ["A-Z","0-9","-","_"].')
    name = models.CharField(max_length=255, validators=[validator])
    cache_size = models.FloatField(null=True, blank=True)
    external_url = models.URLField()
    file_location = models.CharField(max_length=255, blank=True, null=True)
    base = models.CharField(max_length=255, blank=True, null=True)
    globals = models.ForeignKey(Global, blank=True, null=True, related_name='Globals')
    services = models.ForeignKey(Service, blank=True, null=True, related_name='Services')
    is_copy = models.NullBooleanField(blank=True, null=True)

    def save_to_disk(self, temp=True):
        """
        Saves this model to disk
        :param temp: if true, will save to the temp directory. if not, the apps directory
        """
        if self.file_location is None:
            config = gpepconfig.instance()
            directory = config.temp_dir if temp else config.app_dir
            self.file_location = os.path.join(directory, '{}.yaml'.format(self.name))
            self.save()
        with open(self.file_location, 'w') as fp:
            fp.write(yaml.safe_dump(self.to_dict()))

    @property
    def type(self):
        return self.get_first_type()

    def cancel_all_tasks(self):
        """
        Cancels all tasks relating to this mapproxy app
        :return:
        """
        from gpep_apps.jobs.models import Task
        from gpep_apps.jobs.runner import cancel_task

        tasks = Task.objects.filter(map_proxy_app=self)
        for task in tasks:
            cancel_task(task.id)

    def get_able_to_be_changed(self):
        """
        Gets if this app is able to be changed
        :return: boolean able to be changed
        """
        from gpep_apps.jobs.models import Task  # necessary here to prevent circular imports

        # WMTS services have to have the cache on, so they should never be changed
        if self.is_wmts():
            return False
        for task in Task.objects.filter(map_proxy_app=self):
            if task.get_progress()['percent'] != 100:
                return False
        return not self.is_wmts()

    def get_caches(self, **kw):
        """
        Gets the caches under this map proxy app
        :param kw: extra arguments to pass to filter query
        :return: QuerySet of layers
        """
        return Cache.objects.filter(map_proxy_app=self, **kw)

    def cache_enabled(self):
        """
        Gets if caching is enabled for this map proxy app
        :return: boolean cache enabled
        """
        return any(cache for cache in self.get_caches() if isinstance(cache.get_source(), SourceCommons) or
                   cache.imported_geopackage())

    def get_caches_dict(self, add_temp_caches=True, **kw):
        """
        Gets dict of this map proxy app's caches
        :param add_temp_caches: if the temporary extra caches should be added
        :param kw: extra parameters to pass to get_caches
        :return: dict of caches
        """
        return self.prep_for_dict(self.get_caches(**kw), nested=True, add_temp_caches=add_temp_caches)

    def get_grids(self, **kw):
        """
        Gets this map proxy app's grids
        :param kw: extra arguments to pass to the query
        :return: the grids under this map proxy app
        """
        return Grid.objects.filter(map_proxy_app=self, **kw)

    def get_grids_dict(self):
        """
        Gets a dict of all of the grids
        :return: grids dict
        """
        return self.prep_for_dict(self.get_grids())

    def get_sources(self, **kw):
        """
        Gets a list of all of the sources
        :param kw: extra arguments to filter by
        :return: list of sources
        """
        source_types = [WMSSource, MapServerSource, TileSource, MapnikSource, DebugSource]
        out_list = []
        for source_type in source_types:
            for item in source_type.objects.filter(map_proxy_app=self, **kw):
                out_list.append(item)
        return out_list

    def get_sources_dict(self):
        """
        Gets the dict of all of the sources
        :return: sources dict
        """
        return self.prep_for_dict(self.get_sources())

    def prep_for_dict(self, model_objects, nested=False, **kwargs):
        """
        Converts a list of model objects to a list or dict dependent on if they have a key field
        :param model_objects: list of objects to convert to list of dict or dict of dicts
        :param nested: if expecting to get multiple items back when calling to_dict
        :param kwargs: extra arguments to pass to each item's to_dict method
        :return: dict of dicts if models have a key or list of dicts if they do not
        """
        if model_objects:
            if getattr(model_objects[0], 'key', None):
                # if models have keys, put them into a dict
                if nested:
                    return {key: model for model_list in model_objects
                            for key, model in model_list.to_dict(**kwargs).iteritems()}
                return {model.key: model.to_dict(**kwargs) for model in model_objects}
            else:
                # if the models don't have keys, put into a list
                if nested:
                    return [model for model_list in model_objects for model in model_list.to_dict(**kwargs)]
                return [model.to_dict(**kwargs) for model in model_objects]
        return dict()

    def get_layers(self, flatten=True, **kw):
        """
        Gets the layers under
        :param flatten: if true, all layers will returned in the root of the dict
        :param kw: extra arguments to pass to the filter query
        :return: QuerySet of layers
        """
        if flatten:
            return Layer.objects.filter(map_proxy_app=self, **kw).exclude(
                source_source__isnull=True, cache_source__isnull=True)
        return Layer.objects.filter(map_proxy_app=self, parent_layer=None, **kw)

    def get_layers_list(self):
        """
        Gets flattened list of layers for use on front-end
        :return: list of layer
        """
        return self.prep_for_dict(self.get_layers(flatten=True))

    def get_first_type(self):
        """
        Gets the type of the first source
        :return: the first source's type
        """
        sources = self.get_sources()
        if sources:
            return sources[0].type
        return None

    def get_service_type(self):
        services = self.get_services()
        if services is None:
            return
        services_dict = services.to_dict()
        for key, value in services_dict.iteritems():
            if value is not None:
                return key

    def get_services(self):
        """
        Gets this app's services
        :return: this app's services
        """
        return self.services

    def is_wmts(self):
        """
        Checks if this MapProxyApp has a WMTS source.
        :return: true if wmts, false if not
        """
        return TileSource.objects.filter(map_proxy_app=self).exists()

    def to_dict(self, add_temp_caches=True, **kwargs):
        out_dict = {
            'base': self.base,
            'globals': self.globals.to_dict(),
            'services': self.get_services().to_dict(),
            'caches': self.get_caches_dict(add_temp_caches=add_temp_caches),
            'grids': self.get_grids_dict(),
            'sources': self.get_sources_dict(),
            'layers': self.prep_for_dict(self.get_layers(flatten=False), add_temp_caches=add_temp_caches)
        }
        return {key: value for key, value in out_dict.iteritems() if value}


class MapProxyChild(YamlModel):
    map_proxy_app = models.ForeignKey(MapProxyApp)
    key = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        abstract = True


class ScaleHints(MapProxyChild):
    min_res = models.FloatField(blank=True, null=True)
    max_res = models.FloatField(blank=True, null=True)
    min_scale = models.FloatField(blank=True, null=True)
    max_scale = models.FloatField(blank=True, null=True)

    class Meta:
        abstract = True


class Watermark(models.Model):
    text = models.CharField(max_length=255, blank=True, null=True)
    opacity = models.IntegerField(blank=True, null=True)
    font_size = models.IntegerField(blank=True, null=True)
    color_list = models.TextField(blank=True, null=True)
    color_hex = models.CharField(max_length=10, blank=True, null=True)
    spacing = models.CharField(max_length=255, blank=True, null=True)


class Grid(MapProxyChild):
    name = models.CharField(max_length=255, blank=True, null=True)
    srs = models.CharField(max_length=10, blank=True, null=True)
    tile_size = JSONField(blank=True, null=True)
    res = JSONField(blank=True, null=True)
    res_factor = models.IntegerField(blank=True, null=True)
    threshold_res = models.TextField(blank=True, null=True)
    bbox = JSONField(blank=True, null=True)
    bbox_srs = models.TextField(max_length=10, blank=True, null=True)
    origin = models.CharField(choices=ORIGIN_TYPE_CHOICES, max_length=50, blank=True, null=True)
    num_levels = models.IntegerField(blank=True, null=True)
    min_res = models.FloatField(blank=True, null=True)
    max_res = models.FloatField(blank=True, null=True)
    stretch_factor = models.FloatField(blank=True, null=True)
    max_shrink_factor = models.FloatField(blank=True, null=True)
    base = models.CharField(max_length=255, blank=True, null=True)

    def get_epsg_num(self):
        if self.srs:
            list = self.srs.split(":")
            if len(list) >= 2:
                return int(list[1])

    def tile_grid(self):
        return mapproxy.grid.tile_grid(**model_to_dict(self, show_nones=False, ignores=('base')))


class Coverage(YamlModel):
    bbox = JSONField(blank=True, null=True)
    srs = models.CharField(max_length=10, blank=True, null=True)

    def to_dict(self, **kwargs):
        return model_to_dict(self, ignores=('sourcecommons',))


class SourceCommons(ScaleHints):
    type = models.CharField(max_length=16)
    coverage = models.ForeignKey(Coverage, blank=True, null=True)
    seed_only = models.NullBooleanField(blank=True, null=True)
    concurrent_requests = models.IntegerField(blank=True, null=True)

    def get_bbox(self):
        if self.coverage and self.coverage.bbox:
            return self.coverage.bbox
        return [-180.0, -90.0, 180.0, 90.0]

    def to_dict(self, **kwargs):
        return model_to_dict(self, ignores=('sourcecommons_ptr',))


class Cache(MapProxyChild):
    cache_name = models.CharField(max_length=255, blank=True, null=True)
    cache_source = models.ForeignKey('self', blank=True, null=True, related_name="cacheSource")
    source_source = models.ForeignKey(SourceCommons, blank=True, null=True)
    format = models.CharField(choices=INFO_FORMAT_CHOICES, max_length=16, blank=True, null=True)
    request_format = models.CharField(choices=INFO_FORMAT_CHOICES, max_length=16, blank=True, null=True)
    link_single_color_images = models.NullBooleanField(blank=True, null=True)
    minimize_meta_requests = models.NullBooleanField(blank=True, null=True)
    watermark = models.ForeignKey(Watermark, blank=True, null=True)
    grids = JSONField(blank=True, null=True)
    meta_size = JSONField(blank=True, null=True)
    meta_buffer = models.IntegerField(blank=True, null=True)
    use_direct_from_level = models.IntegerField(blank=True, null=True)
    use_direct_from_res = models.IntegerField(blank=True, null=True)
    disable_storage = models.NullBooleanField(blank=True, null=True)
    cache_dir = models.CharField(max_length=255, blank=True, null=True)
    cache = JSONField(blank=True, null=True)

    def get_grids(self):
        """
        Gets this cache's grids
        :return: this cache's grids
        """
        return self.map_proxy_app.get_grids(key__in=self.grids)

    def set_source(self, source):
        """
        Sets the source
        :param source: the source for the cache
        """
        if isinstance(source, basestring):
            source = _get_source_model_from_str(self.map_proxy_app, source)
        if source == self:
            raise GPEPException('The source for a cache cannot be itself!')
        if isinstance(source, Cache):
            self.cache_source = source
            self.source_source = None
        else:
            self.source_source = source
            self.cache_source = None
        self.save()

    def get_source(self):
        """
        Gets this caches's source
        :return: this cache's source
        """
        if self.cache_source:
            return self.cache_source
        return self.source_source

    def get_source_name(self):
        """
        Gets the name of the source
        """
        source = self.get_source()
        return source.key if source else source

    def get_base_source(self):
        """
        Gets the base source of this cache
        :return: either a source object or None
        """
        src = self.get_source()
        while isinstance(src, Cache):
            src = src.get_source()
        return src

    def generate_sub_caches(self):
        """
        Generates sub caches for each grid
        :return: the generated caches
        """
        caches = {}
        for key in DEFAULT_GRID_NAMES:
            if key in self.grids:
                continue
            caches['{}_{}'.format(self.key, key)] = {
                'disable_storage': True,
                'sources': [self.key],
                'grids': [key]
            }
        return caches

    def generate_sub_cache_names(self):
        """
        Generates the names of the sub caches
        :return: the names of the sub caches
        """
        return ['{}_{}'.format(self.key, key) for key in DEFAULT_GRID_NAMES if key not in self.grids]

    def to_dict(self, add_temp_caches=True, **kwargs):
        params = {
            'ignores': ('sourcecommons_ptr', 'cache_source', 'cacheSource', 'cache_name', 'source_source')
        }
        if self.get_source():
            params['sources'] = [self.get_source_name()]
        returned_dict = model_to_dict(self, **params)
        try:
            del returned_dict['cache']['coverage_bbox']
        except KeyError:
            pass
        source = self.get_base_source()
        if(add_temp_caches and gpepconfig.instance().default_projection in self.grids[0] and
           source and hasattr(source, 'tilesource')):
            sub_caches = self.generate_sub_caches()
            sub_caches[self.key] = returned_dict
            return sub_caches
        return {self.key: returned_dict}

    def get_bbox(self):
        src = self.get_source()
        if src is None:
            return [-180.0, -90.0, 180.0, 90.0]
        return src.get_bbox()

    def cache_type(self):
        try:
            return self.cache['type']
        except (TypeError, KeyError):  # cache type was not filled out in the config or cache field is None
            return 'file'

    def imported_geopackage(self):
        try:
            return self.cache['imported']
        except (TypeError, KeyError):
            return False


class WMSOpts(YamlModel):
    version = models.CharField(max_length=255, blank=True, null=True)
    map = models.NullBooleanField(blank=True, null=True)
    featureinfo = models.NullBooleanField(blank=True, null=True)
    legendgraphic = models.NullBooleanField(blank=True, null=True)
    legendurl = models.CharField(max_length=255, blank=True, null=True)
    featureinfo_format = models.CharField(
        choices=INFO_FORMAT_CHOICES, max_length=16, blank=True, null=True)
    featureinfo_xslt = models.CharField(max_length=255, blank=True, null=True)

    def to_dict(self, **kwargs):
        return model_to_dict(self, ignores=('WMSopts', 'WMSOpts'))


class WMSSource(SourceCommons):
    wms_opts = models.ForeignKey(WMSOpts, blank=True, null=True)
    image = models.ForeignKey(SourceImage, blank=True, null=True)
    supported_formats = models.TextField(blank=True, null=True)
    supported_srs = JSONField(blank=True, null=True)
    http = models.ForeignKey(HTTPOpts, blank=True, null=True)
    forward_req_params = models.TextField(blank=True, null=True)
    # required
    req = JSONField()
    # new option added by GPEP (vanilla MapProxy only supports it for WMTS) Set this to ignore missing tile complaints.
    on_error = JSONField(blank=True, null=True)


class MapServerSource(SourceCommons):
    wms_opts = models.ForeignKey(WMSOpts, blank=True, null=True)
    image = models.ForeignKey(SourceImage, blank=True, null=True)
    supported_formats = models.TextField(blank=True, null=True)
    supported_srs = JSONField(blank=True, null=True)
    forward_req_params = models.TextField(blank=True, null=True)
    # required
    req = JSONField()
    mapserver = models.ForeignKey(MapServerOpts, blank=True, null=True)


class TileSource(SourceCommons):
    # required
    url = models.CharField(max_length=255)
    transparent = models.NullBooleanField(blank=True, null=True)
    image = models.ForeignKey(ImageOpts, blank=True, null=True)
    grid = models.TextField(blank=True, null=True)
    request_format = models.TextField(blank=True, null=True)
    http = models.ForeignKey(HTTPOpts, blank=True, null=True)
    on_error = JSONField(blank=True, null=True)


class MapnikSource(SourceCommons):
    # required
    mapfile = models.CharField(max_length=255)
    transparent = models.NullBooleanField(blank=True, null=True)
    image = models.ForeignKey(ImageOpts, blank=True, null=True)
    layers = models.TextField(blank=True, null=True)
    use_mapnik2 = models.NullBooleanField(blank=True, null=True)
    scale_factor = models.FloatField(blank=True, null=True)


class DebugSource(MapProxyChild):
    type = models.CharField(max_length=16)


class Layer(ScaleHints):
    parent_layer = models.ForeignKey('self', blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    source_source = models.ForeignKey(SourceCommons, blank=True, null=True)
    cache_source = models.ForeignKey(Cache, blank=True, null=True)
    legendurl = models.URLField(blank=True, null=True)
    md = JSONField(blank=True, null=True)
    dimensions = models.TextField(blank=True, null=True)
    is_base_layer = models.BooleanField(default=False)

    def delete_all_caches(self):
        """
        Deletes all of this layer's caches
        """
        while isinstance(self.get_source(), Cache):
            self.delete_cache()

    def delete_cache(self):
        """
        Deletes this layer's cache
        """
        source = self.get_source()
        assert isinstance(source, Cache)
        source_source = source.get_source()
        for layer in self.map_proxy_app.get_layers(flatten=True, cache_source=source):
            if layer == self:
                continue
            if layer.get_source() == source:
                layer.set_source(source_source)
        for cache in self.map_proxy_app.get_caches(cache_source=source):
            cache.set_source(source_source)
        self.set_source(source_source)
        source.delete()

    def generate_coverage_name(self):
        """
        Generates a name to use as this object's coverage name
        :return: a name to use for coverage
        """
        return 'coverage_{0}'.format(self.name)

    def generate_seed_name(self):
        """
        Generates a name to use as this object's seed name
        :return: a name to use for seed
        """
        return '{}_package'.format(self.name)

    def get_child_layers(self):
        """
        Gets all the layers that have this layer set as its parent
        :return: all the children of this layer
        """
        layers = Layer.objects.filter(parent_layer=self)
        for layer in layers:
            layer.layers = layer.get_child_layers()
        return layers

    def get_source_name(self):
        """
        Gets the name of the source
        :return: the name of the source
        """
        source = self.get_source()
        return source.key if source else source

    def get_source(self):
        """
        Gets the source for this layer.
        If source is a model, returns the model. If its a string, tries to
        find the model with the matching string, and returns that.
        """
        if self.source_source:
            return self.source_source
        return self.cache_source

    def set_source(self, source):
        """
        Sets the source from either a django model or a string
        :param source: Either a string, Source object, or Cache object
        """
        if isinstance(source, basestring):
            source = _get_source_model_from_str(self.map_proxy_app, source)
        if isinstance(source, SourceCommons):
            self.source_source = source
            self.cache_source = None
        else:
            if not isinstance(source, Cache) and source is not None:
                raise GPEPException('A source or a cache does not exist with the name {}'.format(source))
            self.cache_source = source
            self.source_source = None
        self.save()

    def get_bbox(self):
        """
        Gets the bbox of this layer
        :return: this layer's bbox
        """
        src = self.get_source()
        if src is None:
            return [0, 0, 0, 0]
        return src.get_bbox()

    def get_tile_sources(self):
        source = self.get_source()
        if isinstance(source, Cache) and hasattr(source.get_base_source(), 'tilesource'):
            names = source.generate_sub_cache_names()
            names.append(source.key)
            return names
        return None

    def to_dict(self, add_temp_caches=True, **kwargs):
        """
        Gets a dictionary representation of this object
        :param add_temp_caches: if temp_caches should be added
        :param kwargs: extra arguments
        :return: diction representation
        """
        layers = []
        for layer in self.get_child_layers():
            layers.append(layer.to_dict(add_temp_caches=add_temp_caches, **kwargs))

        kwargs['ignores'] = ('parent_layer', 'cacheSource', 'source_source', 'cache_source')

        source = self.get_source()
        if source:
            kwargs['sources'] = [self.get_source_name()]

        if layers:
            kwargs['layers'] = layers

        out_dict = model_to_dict(self, **kwargs)
        tile_sources = self.get_tile_sources()
        if add_temp_caches and tile_sources:
            out_dict['tile_sources'] = tile_sources
        return out_dict

    def get_grids_layer(self):
        """
        Gets all grid objects pertaining to this layer.
        :return: List of grids corresponding to this layer.
        """
        source = self.get_source()
        if not isinstance(source, Cache):
            raise ValueError('Cannot get seeding estimate on a service with no cache.')
        return source.get_grids()

    def get_size_estimate(self, options_dict):
        """
        Gets the estimate of the size of a job on this layer
        :param options_dict: params to the query
        :return: size estimate in tiles, then number of bytes
        """
        tile_grids = [grid.tile_grid() for grid in self.get_grids_layer()]
        total_tiles = 0
        bbox = (options_dict['min_x'], options_dict['min_y'], options_dict['max_x'], options_dict['max_y'])
        srs = mapproxy.srs.SRS(4326)
        for tile_grid in tile_grids:
            converted_bbox = bbox if tile_grid.srs == srs else mapproxy.grid.grid_bbox(bbox, srs, tile_grid.srs)
            for level in range(options_dict['min_zoom'], options_dict['max_zoom'] + 1):
                # get_affected_level_tiles returns a tuple with first item being bbox,
                # second item being a tuple with (width, height) of tile grid, third
                # item being a list of tile coordinates
                result = tile_grid.get_affected_level_tiles(converted_bbox, level)
                total_tiles += result[1][0] * result[1][1]
        return total_tiles, self.get_estimate_in_bytes(total_tiles)

    def get_estimate_in_bytes(self, total_tiles, tile_width=256, tile_height=256):
        """
        Gets an estimate of the size of a number of tiles in bytes
        :param total_tiles: the number of size
        :param tile_width: width of the tiles in pixels
        :param tile_height: height of the tiles in pixels
        :return: size estimate in bytes
        """
        # the literal number there is the average pixels/byte ratio for tiles.
        # I got it by taking ~30 sample tiles from a sample cache, averaging
        # their size, and dividing the pixels per tile by average size.

        # @TODO: have dimensions be read from grid settings. this was not really being done in version 1
        bytes_per_pixel_constant = 0.302036202936
        return total_tiles * tile_width * tile_height * bytes_per_pixel_constant
