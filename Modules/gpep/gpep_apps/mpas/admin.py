"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

from django.contrib import admin
from .models import MapProxyApp, Cache, Service, Watermark
from .models import Global, Coverage, WMSSource, TileSource, Grid, Layer  # , Dimension, WmsOpts
# from .models import  GlobalsImage, GlobalsCache


admin.site.register(MapProxyApp)
admin.site.register(Cache)
admin.site.register(Service)
admin.site.register(Watermark)
admin.site.register(Global)
admin.site.register(Coverage)
admin.site.register(WMSSource)
admin.site.register(TileSource)
# admin.site.register(WmsOpts)
admin.site.register(Grid)
admin.site.register(Layer)
# admin.site.register(Dimension)
# admin.site.register(GlobalsImage)
# admin.site.register(GlobalsCache)
