"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import logging
import os
import urlparse
import mimetypes

from django.http import StreamingHttpResponse
from wsgiref.util import FileWrapper
from django.db.models import Prefetch

from gpep_apps.api import serializers
from gpep_apps.api.gpep_exception import GPEPException
from gpep_apps.jobs import clean, djcachemanager, djgpep, export, package, runner, seed
from gpep_apps.jobs.models import Job, Task, TaskLayers
from gpep_apps.mpas.models import Layer, MapProxyApp
from gpep_apps.config import gpepconfig
from gpep_apps.status.models import Cache
from gpep_apps.status import tasks as status_tasks
from gpep_apps.api import named_locks

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from django.core.cache import cache as django_cache
from django.core.exceptions import ObjectDoesNotExist


class CreateJob(APIView):
    """
    Creates a new job
    """
    def create_job(self, request, job_type, serializer):
        """
        Creates a new job
        :param request: the request object passed into the post
        :param job_type: one of the four possible job types
        :param serializer: a job type serializer object initialized with the request object
        :return: the created job
        """

        job = Job.objects.create(job_type=job_type)
        serializer.is_valid(raise_exception=True)
        serializer.save(job=job)

        for service in request.data['services']:
            map_proxy_app = MapProxyApp.objects.get(pk=service.get('id'))
            task = Task.objects.create(job=job, map_proxy_app=map_proxy_app)
            if job_type in ['seed', 'package', 'clean']:
                for layer_id in service['layer_ids']:
                    layer = Layer.objects.get(pk=layer_id)
                    if layer.get_source() is None:
                        raise GPEPException("Cannot create a job on a layer that does not have a source.")
                    TaskLayers.objects.create(task=task, layer=layer)

        django_cache.delete('jobs')
        return job

    def post(self, request):
        """
        Creates a new job
        :param request:
        job_type: string representing type of job. We currently support export, seed, package, and clean
        min_zoom: interger minimum zoom level
        max_zoom: integer maximum zoom level
        min_x: integer minimum x value
        min_y: integer minimum y value
        max_x: integer maximum x value
        max_y: integer maximum y value
        num_cacle_processes: integer number of processes to use when executing this job
        services: list of services to cache
            id: integer representing id of service to cache at least one layer of
            layer_ids: list of layers to include in the job each under the
        package_type: (PACKAGING ONLY) string representing the package type to package into
        fetch_tiles: (PACKAGING ONLY) bolean if tiles should be fetched from the source for this
        srs: (PACKAGING ONLY) string for which srs the geopackage should be in
        date: (CLEANING ONLY) date stored as YYYY-MM-DD which defines the date before-which all tiles should be removed
        :return: True if job created successfully, 400 if not
        """
        job_serializer = serializers.JobTypeSerializer(data=request.data)
        job_serializer.is_valid(raise_exception=True)
        job_type = job_serializer.validated_data['job_type']

        if job_type == 'seed':
            serializer = serializers.SeedSerializer(data=request.data)
            seed.run_seed_job(self.create_job(request, job_type, serializer))

        elif job_type == 'clean':
            serializer = serializers.CleanSerializer(data=request.data)
            clean.run_clean_job(self.create_job(request, job_type, serializer))

        elif job_type == 'package':
            serializer = serializers.PackageSerializer(data=request.data)
            package.run_package_job(self.create_job(request, job_type, serializer))

        else:
            assert job_type == 'export'
            serializer = serializers.ExportSerializer(data=request.data)
            export.run_export_job(self.create_job(request, job_type, serializer))

        return Response(True)


class AvailableForDownload(APIView):
    """
    Checks if the file created by the given task is available for download.
    get:
        params:
            task_id: integer task id
        return: True if file is available for download. 400 if it is not.
    """
    def get(self, request):
        serializer = serializers.DownloadSerializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)

        task = Task.objects.get(pk=serializer.data['task_id'])
        if task.get_progress()['percent'] != 100:
            return Response('The requested task has not finished processing. Please wait for the job to '
                            'complete before initiating a download.', status=status.HTTP_400_BAD_REQUEST)
        if not os.path.isfile(task.result_filename):
            return Response('The requested task did not generate a file.', status=status.HTTP_400_BAD_REQUEST)
        return Response(True)


class Download(APIView):
    """
    Attempts to download the file result from the task with the given id
    """
    def get(self, request):
        """
        Gets the result of the task with the given id
        :param request:
        params:
            task_id: integer task id that created the file
            filename: string name to give to the file
        :return: File with the given name if it is finished downloading. If not, returns a 400
        """
        task_id = request.GET['task_id']
        filename = request.GET['filename']

        task = Task.objects.get(pk=task_id)
        if task.get_progress()['percent'] != 100:
            return Response('The requested task has not finished processing. Please use the available_for_download '
                            'endpoint to check whether the task is complete before initiating a download.',
                            status=status.HTTP_400_BAD_REQUEST)
        if not os.path.isfile(task.result_filename):
            return Response('The requested task did not generate a file.', status=status.HTTP_400_BAD_REQUEST)

        chunk_size = 8192
        content_type = mimetypes.guess_type(task.result_filename)[0]
        response = StreamingHttpResponse(FileWrapper(open(task.result_filename, 'rb'),
                                                     chunk_size), content_type=content_type)
        response['Content-Length'] = os.path.getsize(task.result_filename)
        response['Content-Disposition'] = "attachment; filename=%s" % filename
        return response


class SizeEstimate(APIView):
    """
    Gets the estimates for size and number of tiles required for creating a job with the given parameters
    """
    def get(self, request):
        """
        Gets the size estimate for a request with the given parameters
        :param request:
            min_zoom: interger minimum zoom level
            max_zoom: integer maximum zoom level
            min_x: integer minimum x value
            min_y: integer minimum y value
            max_x: integer maximum x value
            max_y: integer maximum y value
            get_time: boolean if true, the time to download will be estimated and returned in get_time
            layer_ids: list of integers
        :return:
            num_tiles: integer representing the number of tiles that will have to be downloaded
            size: integer representing the size in bytes that the new data will take up on disk
            get_time: float representing the expected time it will take to download the tiles.
                this is only created if get_time is true
        """
        estimate = {'num_tiles': 0, 'size': 0}
        serializer = serializers.EstimateSerializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        layers = Layer.objects.filter(id__in=serializer.validated_data['layer_ids'])
        for layer in layers:
            single_layer_estimate = layer.get_size_estimate(serializer.validated_data)
            estimate['num_tiles'] += single_layer_estimate[0]
            estimate['size'] += single_layer_estimate[1]
        if serializer.validated_data['get_time']:
            # @TODO: this is the quick and dirty way. Later implement method
            # @TODO: where small temporary cache is seeded and deleted for each request
            # @TODO: to test download speeds.
            # The literal number here is a seconds/tile value calculated by performing a few different downloads at
            # average speeds and taking the average time per tile between them all.
            estimate['time_sec'] = estimate['num_tiles'] * 0.29301075
        return Response(estimate)


class JobListView(APIView):
    """
    Gets the details for all of the jobs
    """
    def get(self, request):
        """
        Gets the details for all of the jobs
        :param request: unused
        :return: list of dicts for each of the jobs with fields described in gpep_apps.api.serializers
        """

        jobs_cache = django_cache.get('jobs')

        if not jobs_cache:
            # prefetch related is used to optimize the queries. this is much, much faster
            jobs = Job.objects.all().order_by('-id').prefetch_related(
                Prefetch('task_set', queryset=Task.objects.select_related('map_proxy_app')),
            ).select_related('seed', 'package', 'clean', 'export')
            jobs = [job for job in jobs if job.task_set.exists()]
            serializer = serializers.JobSerializer(jobs, many=True)
            django_cache.set("jobs", serializer.data, 5)

        return Response(django_cache.get('jobs'))


class CancelJob(APIView):
    """
    Cancels the job with the given job_id
    """
    def post(self, request):
        """
        Cancels the job with the given job_id
        :param request:
            job_id: integer id of job to cancel
        :return: True if successful
        """
        job_id = request.data.get('job_id')

        with named_locks.get("job_" + str(job_id)):
            job = Job.objects.filter(pk=job_id).first() if job_id else None
            if not job_id or not job:
                return Response("Invalid Job ID", status=status.HTTP_400_BAD_REQUEST)
            if job.finished:
                return Response("Jobs may not be canceled if they are complete.", status=status.HTTP_400_BAD_REQUEST)
            runner.cancel_job(job_id)
            return Response(True)


class JobDetailsView(APIView):
    """
    Gets the details of the job with the given id
    """
    def get(self, request, pk):
        """
        Gets the details of the job with the given id
        :param request: unused
        :param pk: integer id of the job to get the details of
        :return: all of the fields in the serializer for the job type of the job with the given id
        """
        # not using Job.objects.get because need to do prefetching etc to improve query
        # speed for jobs with many tasks
        job = Job.objects.filter(id=pk).prefetch_related(
            Prefetch('task_set', queryset=Task.objects.select_related('map_proxy_app')),
        ).select_related('seed', 'package', 'clean').first()
        if job:
            serializer = serializers.JobSerializer(job, many=False, context={'return_layers': True})
            return Response(serializer.data)
        return Response("This job does not exist in GPEP.", status=status.HTTP_404_NOT_FOUND)


class AddMapProxyApp(APIView):
    """
    Creates a new Map Proxy App with the given name from the given url
    """
    def post(self, request):
        """
        Creates a new Service
        :param request: service_name: string name of the service to get
        :return: dict the created Map Proxy App
        """

        with named_locks.get("adding_service_with_name: " + request.data['name']):
            with named_locks.get("adding_service_with_url: " + request.data['external_url']):
                serializer = serializers.AddMapProxyAppSerializer(data=request.data)
                serializer.is_valid(raise_exception=True)

                ex_url = serializer.validated_data['external_url']
                parsed = urlparse.urlparse(ex_url)
                request.META['HTTP_HOST'] = request.META.get('HTTP_HOST', parsed.hostname)
                try:
                    map_proxy_app = djgpep.add_map_proxy_app(
                        service_name=serializer.validated_data['name'],
                        service_url=serializer.validated_data['external_url'],
                        cache_service=serializer.validated_data['enable_cache'],
                    )
                except ValueError as e:
                    return Response(e.message, status=status.HTTP_400_BAD_REQUEST)
                serializer = serializers.MapProxyAppListSerializer(map_proxy_app, many=False,
                                                                   context={'request': request})
                django_cache.delete('services')
                return Response(serializer.data)


class GetMapProxyApp(APIView):
    """
    Gets the Map Proxy App with the given name
    """
    def get(self, request):
        """
        Gets the mapproxyapp with the given name
        :param request: service_name: string name of the service to get
        :return: dict Map Proxy App
        """
        service_name = request.GET.get('service_name')
        mpa = MapProxyApp.objects.filter(name=service_name, is_copy=False).first()
        if mpa is None:
            raise GPEPException('No apps with name {}'.format(service_name))
        return Response(mpa.to_dict())


class MapProxyAppList(APIView):
    """
    Gets a list of all of the mapproxyapps with all of their details
    """
    def get(self, request):
        """
        Gets a list of all of the mapproxyapps
        :param request: unused
        :return: list of all of the map proxy apps each with the fields defined in
            serializers.MapProxyAppListSerializer.Meta.fields
        """

        services = django_cache.get('services')

        # if there is no cache for services (has expired or been invalidated) then recalculate it
        if not services:
            map_proxy_apps = MapProxyApp.objects.filter(is_copy=False)
            serializer = serializers.MapProxyAppListSerializer(map_proxy_apps, many=True, context={'request': request})
            services = serializer.data
            django_cache.set('services', services, 10)

        return Response(services)


class DeleteMapProxyApp(APIView):
    """
    Deletes the map proxy app with the given id along with its associated files
    """
    def post(self, request):
        """

        :param request: name: string name of the map proxy app to delete
        :return: True if successful, 400 if error
        """

        serializer = serializers.MapProxyAppSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        name = serializer.validated_data['name']
        map_proxy_app = MapProxyApp.objects.filter(name=name, is_copy=False).first()
        id = map_proxy_app.id

        with named_locks.getServiceLock(map_proxy_app.id):
            # make sure the desired app to delete still exists
            try:
                MapProxyApp.objects.get(id=id)
            except ObjectDoesNotExist:
                return Response(data="Service with name \"{}\" does not exist.".format(name), status=400)

            djcachemanager.delete_map_proxy_app(map_proxy_app, log=logging.getLogger(str(map_proxy_app.id)))
            MapProxyApp.objects.filter(name=serializer.validated_data['name'], is_copy=True).delete()
            return Response(True)


class DeleteTask(APIView):
    """
    Deletes the task with the given id
    """
    def post(self, request):
        """
        Deletes the task with the given id
        :param request: id: integer task id
        :return: True if successful, 400 if not
        """
        try:
            Task.objects.get(pk=request.data['id']).delete()
        except ValueError as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)

        django_cache.delete('jobs')
        return Response(True)


class DeleteJob(APIView):
    """
    Deletes the job with the given id
    post:
        params:
            id: integer job id
        returns:
            True if successful, 400 if not
    """
    def post(self, request):
        try:
            Job.objects.get(pk=request.data['id']).delete()
        except ValueError as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)

        django_cache.delete('jobs')
        return Response(True)


class CacheSize(APIView):
    """
    Gets the size of the cache for a given service or the whole cache along with the space available
    """
    def get(self, request):
        """
        :param request: id: integer map proxy app id (optional)
        :return:
            size: integer size in bytes of cache for service with given id if id is provided, if one is not,
                returns the size of the whole cache that can be seen by services
            free_space: integer size in bytes of available space on disk
        """
        cache_status = Cache.objects.all().first()

        # if cache has been calculated yet, return calculated values
        if cache_status:
            return Response({'size': cache_status.used_space,
                             'modified_timestamp': cache_status.modified_timestamp,
                             'free_space': cache_status.free_space})
        # otherwise, return null for all values
        else:
            return Response(False)


class AddGeopackageService(APIView):
    """
    Creates a new service from a geopackage
    """
    def post(self, request):
        """
        Create a new service from a geopackage
        :param request: dict with keys being service names and values being geopackage file
        :return: names of all of the created services
        """
        return_vals = []
        for key, value in request.FILES.iteritems():
            key = key.replace('.gpkg', '') if '.gpkg' in key else key
            if MapProxyApp.objects.filter(name=key).exists():
                raise GPEPException("Cannot have services with duplicate service names")
            return_vals.append(djgpep.add_geopackage_service(key, value).name)
        return Response(return_vals)


class ImportPep(APIView):
    """
    Imports a .pep file into the backend
    """
    def post(self, request):
        """
        Imports a .pep file into the backend
        :param request: for any number of pep files, key is service name and value is .pep file
        :return: list of the names of the new services or error
        """
        data_dict = [{'service_name': service_name, 'pep_file': pep_file} for
                     service_name, pep_file in request.FILES.iteritems()]
        for data_item in data_dict:
            value = str(data_item['service_name'])
            if '.pep' in value:
                value = value.replace('.pep', '')
            if not value:
                raise GPEPException('Must provide a valid service name')
            if MapProxyApp.objects.filter(name=value).exists():
                raise GPEPException('An app already exists with that name')
            data_item['service_name'] = value
        return Response([djgpep.import_pep(**args).name for args in data_dict])


class CachingEnabled(APIView):
    """
    Getter/setter for caching enabled for a given service
    """
    def get(self, request):
        """
        Gets the status of the cache for a service and if it can be changed
        :param request: id: integer mapproxyapp model id
        :return:
            has_cache: boolean if caching for this service is enabled
            able_to_change: boolean if there are any jobs running on that service, will return false, if there are not
        """
        app = MapProxyApp.objects.filter(id=request.GET.get('id')).first()
        if app is None:
            raise GPEPException("No service with id '{}'".format(request.GET.get('id')))
        return Response({'has_cache': app.cache_enabled(), 'able_to_change': app.get_able_to_be_changed()})

    def post(self, request):
        """
        Setter for caching enabled
        :param request:
            id: integer map proxy app model id
            enable_cache: boolean will disable the cache if false and enable it if true
        :return: True
        """
        with named_locks.get("CachingEnabled"):
            serializer = serializers.CachingEnabledSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            app = MapProxyApp.objects.filter(id=serializer.validated_data['id']).first()
            assert app
            enable_cache = serializer.validated_data['enable_cache']
            if enable_cache == app.cache_enabled():
                return Response(True)
            if not app.get_able_to_be_changed():
                raise GPEPException("All outstanding tasks need to be finished or stopped.")
            if enable_cache is True:
                djcachemanager.create_cache(app, log=logging.getLogger(str(app.id)))
            else:
                djcachemanager.disable_cache(app)
            app.save()
            return Response(True)


class Ping(APIView):
    """
    Getter, which simply returns true.
    Used to test a connection with the GPEP server.
    """
    def get(self, request):
        """
        Tests if the server is up
        :param request: unused
        :return: True
        """
        return Response(True)


class SetBaseLayer(APIView):
    """
    Can be used to either make a layer a base layer, or make it not be a base layer.
    Note:  layers are not base layers by default.
    """

    def post(self, request):
        """
        Can be used to either make a layer a base layer, or make it not be a base layer.
        Only one layer can be the base layer at a time.
        :param request:
            id: An integer layer id
            is_base_layer: True if that layer should become a base layer, False if it should become a non-base layer.
        :return: True if the operation is successful, 400 otherwise.
        """
        with named_locks.get("SetBaseLayer"):
            id = request.data['id']
            layer = Layer.objects.filter(id=id).first()
            old_base_layer = Layer.objects.filter(is_base_layer=True).first()

            if layer is None:
                return Response("The target layer with id {} does not exist.".format(id),
                                status=status.HTTP_400_BAD_REQUEST)

            # if making a new layer the default base layer, unset previous default base layer
            if(request.data['is_base_layer']):
                # old default base layer is no longer default base layer
                if(old_base_layer is not None):
                    old_base_layer.is_base_layer = False
                    old_base_layer.save()

            layer.is_base_layer = request.data['is_base_layer']
            layer.save()
            return Response(True)


class GetDefaultBaseLayer(APIView):
    """
    Used to retrieve the information about the base layer, as defined in pep.cfg.
    Note:  This simply consists of a title, url, and max zoom level. It is not a proper
    layer model object.
    """

    def get(self, request):
        """
        Returns the default base layer as defined in pep.cfg
        :return:
            title:    string a human readable title for the layer
            url:      leaflet formatted tileLayer url for a layer
            max_zoom: maximum zoom level which the server supports for this layer
        """

        return Response({
            'title': gpepconfig.instance().default_basemap_title,
            'url': gpepconfig.instance().default_basemap_url,
            'max_zoom': gpepconfig.instance().default_basemap_max_zoom_level
        })
