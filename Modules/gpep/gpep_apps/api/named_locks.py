from threading import Lock, RLock

# named_locks library provides a centralized place to store threading.Lock objects by name

locks = {}
GET_LOCK = Lock()
GET_SERVICE_LOCK = Lock()


def get(lock_name):
    """
    Returns a thread lock object associated with the given lock name
    :param lock_name:  String name used to identify the lock.
    :return: a threading.Lock object
    """
    with GET_LOCK:
        lock_name = "custom_" + str(lock_name)
        if lock_name not in locks:
            locks[lock_name] = RLock()
        return locks[lock_name]


def getServiceLock(id):
    """
    Acquire a lock for a service with a particular ID
    :param id: ID of a MapProxyApp
    :return: a threading.Lock object associated with a particular service's ID
    """
    with GET_SERVICE_LOCK:
        return get("service_" + str(id))
