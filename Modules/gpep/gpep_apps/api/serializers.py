"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

from rest_framework import serializers

from gpep_apps.api.gpep_exception import GPEPException
from gpep_apps.jobs.models import BoxAndZoomJob, Clean, Export, Job, Package, Seed, Task, TaskLayers
from gpep_apps.mpas.models import Layer, MapProxyApp, Service


class DownloadSerializer(serializers.Serializer):
    """Validates the parameters for the Download endpoint."""
    filename = serializers.CharField()
    task_id = serializers.IntegerField()

    def validate_task_id(self, data):
        if Task.objects.filter(id=data).exists():
            return data
        raise GPEPException('Could not find a corresponding task.')


class JobTypeSerializer(serializers.ModelSerializer):
    """Ensures that job_type is only one of the approved job types."""
    class Meta:
        model = Job
        fields = ('job_type',)


class BaseJobTypeSerializer(serializers.ModelSerializer):
    """Provides some of the fields that all of the other job type serializers need."""
    id = serializers.SerializerMethodField()
    job_type = serializers.SerializerMethodField()
    tasks = serializers.SerializerMethodField()
    created_at = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    def get_status(self, obj):
        return obj.job.get_status()

    def get_id(self, obj):
        return obj.job.id

    def get_job_type(self, obj):
        return obj.job.job_type

    def get_tasks(self, obj):
        if self.context.get("return_layers"):
            return TaskProgressLayerSerializer(obj.job.task_set.all(), many=True).data
        else:
            return TaskProgressSerializer(obj.job.task_set.all(), many=True).data

    def get_created_at(self, obj):
        return obj.job.created_at

    class Meta:
        abstract = True


class CleanSerializer(BaseJobTypeSerializer):
    """Validates parameters when creating a clean job."""
    class Meta:
        model = Clean
        fields = ('id', 'created_at', 'job_type', 'min_x', 'min_y', 'max_x', 'max_y',
                  'min_zoom', 'max_zoom', 'date', 'tasks', 'status')


class SeedSerializer(BaseJobTypeSerializer):
    """Validates parameters when creating a seed job."""
    class Meta:
        model = Seed
        fields = ('id', 'created_at', 'job_type', 'min_x', 'min_y', 'max_x', 'max_y',
                  'min_zoom', 'max_zoom', 'tasks', 'status')


class PackageSerializer(BaseJobTypeSerializer):
    """Validates parameters when creating a package job."""
    class Meta:
        model = Package
        fields = ('id', 'created_at', 'job_type', 'min_x', 'min_y', 'max_x', 'max_y',
                  'min_zoom', 'max_zoom', 'package_type', 'fetch_tiles', 'srs', 'tasks', 'status')


class ExportSerializer(BaseJobTypeSerializer):
    """Validates parameters when creating a export job."""
    class Meta:
        model = Export
        fields = ('id', 'created_at', 'job_type', 'tasks', 'status')


class JobSerializer(serializers.ModelSerializer):
    """Converts job going through it from their base form to their real representation."""
    status = serializers.SerializerMethodField()

    def get_status(self, obj):
        return obj.job.get_status()

    class Meta:
        model = Job

    def to_representation(self, obj):
        # pass the context because it contains an optional 'return_layers' arg
        if hasattr(obj, 'clean'):
            return CleanSerializer(obj.clean, context=self.context).data
        elif hasattr(obj, 'seed'):
            return SeedSerializer(obj.seed, context=self.context).data
        elif hasattr(obj, 'package'):
            return PackageSerializer(obj.package, context=self.context).data
        else:
            assert hasattr(obj, 'export')
            return ExportSerializer(obj.export, context=self.context).data


class LayerSerializer(serializers.ModelSerializer):
    """Gets a subset of the fields in a layer to return to the front-end."""
    bbox = serializers.SerializerMethodField()

    def get_bbox(self, obj):
        if obj.cache_source and obj.cache_source.imported_geopackage() is True:
            return obj.cache_source.cache['coverage_bbox']
        return obj.get_bbox()

    class Meta:
        model = Layer
        fields = ('id', 'name', 'title', 'bbox', 'is_base_layer')


class MapProxyAppSerializer(serializers.ModelSerializer):
    """Ensures that ids for map proxy apps corrospond to one in the database."""
    def validate_name(self, data):
        if not MapProxyApp.objects.filter(name=data).exists():
            raise GPEPException('Could not find a service with this name.')
        return data

    class Meta:
        model = MapProxyApp
        fields = ('id', 'name')


class MapProxyAppListSerializer(serializers.ModelSerializer):
    """Gets the list of all of teh map proxy apps with the correct host."""
    layers = serializers.SerializerMethodField()

    def get_layers(self, obj):
        layers = obj.get_layers()
        return LayerSerializer(layers, many=True).data

    url = serializers.SerializerMethodField()
    cache_type = serializers.SerializerMethodField()
    imported_geopackage = serializers.SerializerMethodField()

    def get_imported_geopackage(self, obj):
        if obj.cache_enabled():
            return obj.get_caches()[0].imported_geopackage()
        return False

    def get_url(self, obj):
        return "{}/service".format(obj.name)

    def get_cache_type(self, obj):
        try:
            return obj.get_caches()[0].cache_type()
        except IndexError:  # there are no caches present
            return 'Caching disabled for this service.'

    class Meta:
        model = MapProxyApp
        fields = ('id', 'name', 'url', 'layers', 'cache_type', 'cache_size', 'imported_geopackage')


class AddMapProxyAppSerializer(serializers.ModelSerializer):
    """Validates request to add a map proxy app from the given url."""
    enable_cache = serializers.BooleanField(required=True)

    def validate_name(self, value):
        if MapProxyApp.objects.filter(name=value).exists():
            raise GPEPException('Service with name {0} already exists.'.format(value))
        return value

    def validate_external_url(self, value):
        if MapProxyApp.objects.filter(external_url=value).exists():
            raise GPEPException("A Service with that URL has already been added with the name {0}."
                                .format(MapProxyApp.objects.filter(external_url=value)[0].name))
        return value

    class Meta:
        model = MapProxyApp
        fields = ('name', 'external_url', 'enable_cache')


class TaskLayersSerializer(serializers.ModelSerializer):
    """Specifies the data to return when serializing a task's layer."""
    id = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()

    def get_id(self, obj):
        return obj.layer.id

    def get_name(self, obj):
        return obj.layer.name

    def get_title(self, obj):
        return obj.layer.title

    class Meta:
        model = TaskLayers
        fields = ('id', 'name', 'title')


class TaskProgressSerializer(serializers.ModelSerializer):
    """Serializes the progress report for a task."""
    progress = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()

    def get_progress(self, obj):
        return obj.get_progress()

    def get_name(self, obj):
        return obj.map_proxy_app.name

    class Meta:
        model = Task
        fields = ('id', 'start_date', 'progress', 'name', 'status', 'failure_message')


class TaskProgressLayerSerializer(TaskProgressSerializer):
    layers = serializers.SerializerMethodField()

    def get_layers(self, obj):
        task_layers = TaskLayers.objects.filter(task=obj)
        return TaskLayersSerializer(task_layers, many=True).data

    class Meta:
        model = Task
        fields = ('id', 'start_date', 'progress', 'name', 'layers', 'status', 'failure_message')


class EstimateSerializer(serializers.ModelSerializer):
    """Validates get request for size estimate for a service."""
    layer_ids = serializers.ListField()
    get_time = serializers.BooleanField()

    def validate_layer_ids(self, value):
        if len(value) == 0:
            raise GPEPException('Must provide at least one layer id.')
        if len(set(value)) != len(value):
            raise GPEPException('List cannot have any duplicates.')
        if len(Layer.objects.filter(id__in=value)) == len(value):
            return value
        raise GPEPException('Invalid layer id received.')

    class Meta:
        model = BoxAndZoomJob
        fields = ('min_zoom', 'max_zoom', 'min_x', 'min_y', 'max_x', 'max_y', 'get_time', 'layer_ids')


class CachingEnabledSerializer(serializers.Serializer):
    """Validates post to set caching enabled for a service."""
    enable_cache = serializers.BooleanField()
    id = serializers.IntegerField()

    def validate_id(self, object):
        if MapProxyApp.objects.filter(id=object).exists():
            return object
        raise GPEPException("There is not a MapProxyApp with the id: {}".format(object))
