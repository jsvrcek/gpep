"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import views
from django.conf.urls import url

urlpatterns = [
    url(r'^jobs$',                            views.JobListView.as_view()),
    url(r'^jobs/(?P<pk>\d+)/$',               views.JobDetailsView.as_view()),
    url(r'^create_job$',                      views.CreateJob.as_view()),
    url(r'^cancel_job',                       views.CancelJob.as_view()),
    url(r'^add_service$',                     views.AddMapProxyApp.as_view()),
    url(r'^add_geopackage_service$',          views.AddGeopackageService.as_view()),
    url(r'^service_list$',                    views.MapProxyAppList.as_view()),
    url(r'^get_map_proxy_app$',               views.GetMapProxyApp.as_view()),
    url(r'^delete_map_proxy_app$',            views.DeleteMapProxyApp.as_view()),
    url(r'^download$',                        views.Download.as_view()),
    url(r'^cache_size$',                      views.CacheSize.as_view()),
    url(r'^size_estimate$',                   views.SizeEstimate.as_view()),
    url(r'^available_for_download$',          views.AvailableForDownload.as_view()),
    url(r'^import_pep$',                      views.ImportPep.as_view()),
    url(r'^delete_task$',                     views.DeleteTask.as_view()),
    url(r'^delete_job$',                      views.DeleteJob.as_view()),
    url(r'^caching_enabled$',                 views.CachingEnabled.as_view()),
    url(r'^ping$',                            views.Ping.as_view()),
    url(r'^set_base_layer$',                   views.SetBaseLayer.as_view()),
    url(r'^get_default_baselayer$',            views.GetDefaultBaseLayer.as_view()),
]
