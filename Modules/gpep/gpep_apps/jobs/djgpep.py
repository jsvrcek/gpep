"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import logging
import os
import re
import shutil
import sqlite3
import zipfile

from mapproxy.script.conf import app

from gpep_apps.api.gpep_exception import GPEPException
from gpep_apps.config import gpepconfig
from gpep_apps.jobs import djcachemanager as cachemanager, utils
from gpep_apps.mpas import model_creator, models
from django.core.cache import cache as django_cache


def get_map_proxy_error_file_path():
    """
    Generates a new temporary file path and creates it
    :return: a path to a new file that has been created
    """
    path = utils.get_unique_filename(gpepconfig.instance().log_dir)
    with open(path, 'a+'):
        pass
    return path


def check_and_update_projections(map_proxy_app, supported_srs, log):
    """
    Ensures that all of the sources only support projections that we support
    :param map_proxy_app: the service to update the projections for
    :param supported_srs: the srses that are supported
    :param log: logger
    """
    if map_proxy_app.type == 'wms' or map_proxy_app.type == 'wmts':
        for source in map_proxy_app.get_sources():
            projections = source.supported_srs if map_proxy_app.type == 'wms' else models.Grid.objects.filter(
                map_proxy_app=map_proxy_app, key=source.grid).values_list('srs', flat=True)
            projections = list(set(projections).intersection(supported_srs))
            if projections:
                if map_proxy_app.type == 'wms':
                    source.supported_srs = projections
                    source.save()
            else:
                os.remove(map_proxy_app.file_location)
                log.error('The {} service does not contain one of the compatible projections {}'
                          .format(map_proxy_app.name, supported_srs))
                raise ValueError('{0} does not contain a compatible projection'.format(map_proxy_app.name))


def config_command_caller(**kwargs):
    """
    Super simple wrapper to simplify calls into mapproxy config command
    This also allows for easier testing
    :param kwargs: the params to expand and then pass into config command
    :return: mapproxy's return value
    """
    new_dict = {'--{}'.format(key): kwargs[key] for key in kwargs}
    try:
        return app.config_command([item for pair in new_dict.iteritems() for item in pair])
    except Exception as e:
        raise GPEPException(e.message)


def generate_yaml_file(service_url, service_name, log):
    """
    Generates a yaml file for the service_url using map proxy
    :param service_url: url to a Get Capabilities file
    :param service_name: the name to give to the service
    :param log: logging logger
    :return: the path to the created yaml file
    """
    error_file = get_map_proxy_error_file_path()

    def get_add_map_proxy_app_error():
        # open file which mapproxy will write error messages to, so gpep can have access to these messages
        with open(error_file, 'r') as mapproxy_error_file:
            mapproxy_error_msg = mapproxy_error_file.read()
        msg = 'The URL: {0} is not a valid WMS or WMTS service.   MapProxy error msg: {1}'.format(
            service_url, mapproxy_error_msg)
        log.exception(msg)
        raise GPEPException(msg)

    yaml_path = os.path.join(gpepconfig.instance().app_dir, '{}.yaml'.format(service_name))
    models.MapProxyApp.objects.filter(name=service_name).delete()
    if os.path.exists(yaml_path):
        os.remove(yaml_path)
    try:
        result = config_command_caller(output=yaml_path, base=gpepconfig.instance().template_file,
                                       log=error_file, capabilities=service_url, insecure=not gpepconfig.instance()
                                       .strict_validation)
        log.info('Result of adding the {0} service: {1}'.format(service_name, result))
        if result != 0:
            get_add_map_proxy_app_error()
    finally:
        os.remove(error_file)
    return yaml_path


def add_geopackage_service(name, geopackage):
    """
    Creates a map_proxy_app from the given geopackage with the given name
    :param name: name to give to the new model
    :param geopackage: data to create the map proxy app from
    :return: the created model
    """
    cache_dir = os.path.join(gpepconfig.instance().cache_dir, name)
    if not os.path.exists(cache_dir):
        os.mkdir(cache_dir)

    # Write the database out to disk
    filename = os.path.join(cache_dir, '{}.gpkg'.format(name))
    with open(filename, 'wb+') as f:
        for chunk in geopackage.chunks():
            f.write(chunk)

    # Find the tables in the database
    db = sqlite3.connect(filename)

    cur = db.execute('SELECT table_name, data_type, srs_id FROM gpkg_contents')
    table_names = cur.fetchall()
    cur = db.execute('SELECT min_x, min_y, max_x, max_y FROM gpkg_contents')
    min_x, min_y, max_x, max_y = cur.fetchone()
    coverage_bbox = [min_x, min_y, max_x, max_y]

    # Generates the yaml path using the name given to the file
    yaml_path = os.path.join(gpepconfig.instance().app_dir, '{}.yaml'.format(name))

    # Creates the dict to create the yaml model from
    caches = {}
    layers = []
    grids = {}
    folder_name, geopackage_name = os.path.split(filename)
    for table_name, data_type, srs_id in table_names:
        try:
            cur = db.execute('SELECT srs_id, min_x, min_y, max_x, max_y FROM gpkg_tile_matrix_set '
                             'WHERE table_name = "{}"'.format(table_name))
        except db.OperationalError as e:
            raise GPEPException(e.message)
        srs_id, min_x, min_y, max_x, max_y = cur.fetchone()
        bbox = [min_x, min_y, max_x, max_y]

        cur = db.execute('SELECT zoom_level, pixel_x_size FROM gpkg_tile_matrix '
                         'WHERE table_name = "{}" ORDER BY zoom_level'.format(table_name))
        zoom_level, pixel_x_size = cur.fetchone()
        custom_grid_name = '{}_grid'.format(table_name)

        min_res = pixel_x_size * (2 ** zoom_level)

        # ensures the bbox coordinates utilize EPSG:4326 (latitude longitude)
        if srs_id != 4326:
            from mapproxy.srs import SRS
            bbox = SRS(srs_id).transform_bbox_to(SRS(4326), bbox)
            bbox = list(bbox)  # binding errors if bbox passsed as tuple
            # necessary to convert overall coverage to 4326 for frontend
            coverage_bbox = SRS(srs_id).transform_bbox_to(SRS(4326), coverage_bbox)
            coverage_bbox = list(coverage_bbox)

        grids[custom_grid_name] = {
            'srs': 'EPSG:{}'.format(srs_id),
            'bbox': bbox,
            'bbox_srs': 'EPSG:4326',
            'min_res': min_res,
            'origin': 'nw'
        }

        base_name = '{}'.format(table_name)
        cache_name = '{}_cache'.format(base_name)
        layer_name = '{}_layer'.format(base_name)
        caches[cache_name] = {
                'grids': [custom_grid_name],
                'cache': {
                    'type': 'geopackage',
                    'filename': geopackage_name,
                    'table_name': table_name,
                    'directory': folder_name,
                    'coverage_bbox': coverage_bbox,
                    'bbox_srs': 'EPSG:4326',
                    'imported': True,
                }
            }
        layers.append({
            'name': layer_name,
            'title': layer_name,
            'source': cache_name
        })
    model_dict = {
        'layers': layers,
        'caches': caches,
        'sources': [],
        'grids': grids,
        'base': gpepconfig.instance().template_file
    }

    # Creates the yaml model from the created dict.
    model = model_creator.create_map_proxy_app_from_dict(model_dict, name, path=yaml_path, pull_in_base=True)
    model.save_to_disk()
    return model


def add_map_proxy_app(service_name, service_url, cache_service=False):
    log = logging.getLogger(service_name)
    yaml_path = generate_yaml_file(service_url, service_name, log)

    map_proxy_app = model_creator.create_map_proxy_app_from_file(yaml_path, pull_in_base=True)
    map_proxy_app.external_url = service_url

    # WMTS service may not be added unless they enable caching
    if cache_service is False and map_proxy_app.is_wmts():
        map_proxy_app.delete()
        error_message = 'WMTS services must have caching enabled.'
        log.error(error_message)
        raise ValueError(error_message)

    supported_srs = ['EPSG:3395', 'EPSG:4326', 'EPSG:3857']

    # add service extra logic
    if map_proxy_app.services.wms:
        map_proxy_app.services.wms.srs = supported_srs
        map_proxy_app.services.wms.save()

    check_and_update_projections(map_proxy_app, supported_srs, log)

    if not map_proxy_app.is_wmts():
        for layer in map_proxy_app.get_layers():
            layer.refresh_from_db()  # Without this, the updated values from the database were not being pulled out
            layer.delete_all_caches()

    if cache_service:
        cachemanager.create_cache(map_proxy_app, log=log)
        print "The {0} service will begin caching tiles on demand.".format(service_name)

    for cache in map_proxy_app.get_caches():
        try:
            cache.cache['type']
        except (TypeError, KeyError):
            cache.cache = {'type': 'file'}
            cache.save()

    if gpepconfig.instance().on_error:
        on_error_dict = {404: {'response': 'transparent', 'cache': True},
                         'other': {'response': 'transparent', 'cache': True}}
        sources = map_proxy_app.get_sources()
        for source in sources:
            source.on_error = on_error_dict
            source.save()

    map_proxy_app.save()
    map_proxy_app.save_to_disk()

    django_cache.delete('services')
    return map_proxy_app


def check_for_pep_compliance(zip_file):
    """
    Checks the given zip file for pep compliance
    :param zip_file: the zip file to check
    :return: tuple with the name of the folder and the name of the yaml file
    """
    folder_names = set()
    file_names = []
    for file in zip_file.filelist:
        if '\\' in file.filename or '/' in file.filename:
            path = os.path.relpath(file.filename)
            folder_names.add(path.split(os.sep)[0])
        else:
            file_names.append(file.filename)
    if len(folder_names) != 1:
        raise GPEPException('PEP file must have exactly one directory for a cache')
    if len(file_names) != 1:
        raise GPEPException('PEP file is malformed. There can only be one .yaml file in the root.')
    return folder_names.pop(), file_names[0]


def import_pep(service_name, pep_file):
    """
    Imports the given pep file
    :param service_name: the name to use as the name of the new service
    :param pep_file: the pep file to import
    :return: model created from the pep file
    """
    app_dest_path = os.path.join(gpepconfig.instance().app_dir, '{}.yaml'.format(service_name))

    move_cache_folder = False  # necessary if the folder name inside the zip already is in the cache
    folder_dest_path = os.path.join(gpepconfig.instance().cache_dir, service_name)
    app_source_path, folder_name, folder_source_path = None, None, None
    with zipfile.ZipFile(pep_file, 'r') as zip_file:
        folder_name, file_name = check_for_pep_compliance(zip_file)

        if os.path.exists(os.path.join(gpepconfig.instance().cache_dir, folder_name)):
            # if cache already exists with the same name as the name of the folder in the zip file
            folder_source_path = utils.get_unique_folder(gpepconfig.instance().cache_dir)
            move_cache_folder = True
        else:
            # the folder is just placed in the cache directory
            folder_source_path = gpepconfig.instance().cache_dir

        for file_path in zip_file.filelist:
            path = os.path.relpath(file_path.filename)
            if os.sep in path or '.' not in path:
                file_location = os.path.join(folder_source_path, path)
                if not os.path.exists(os.path.split(file_location)[0]):
                    os.makedirs(os.path.split(file_location)[0])
                zip_file.extract(file_path, folder_source_path)
            else:
                app_source_path = zip_file.extract(file_path, gpepconfig.instance().temp_dir)
    if move_cache_folder or folder_name != service_name:
        shutil.move(os.path.join(folder_source_path, folder_name), folder_dest_path)
        if move_cache_folder:
            shutil.rmtree(folder_source_path)
    with utils.open_file(app_dest_path, 'a') as app_out:
        with utils.open_file(app_source_path, 'r') as app_in:
            for line in app_in:
                # need to change the service_name in all the file paths.
                line = re.sub(r'<GPEP:HOME>/map_cache/[0-9a-zA-Z_]+/', '<GPEP:HOME>/map_cache/{}/'.format(
                    service_name), line)
                line = re.sub(r'<GPEP:HOME>\\map_cache\\[0-9a-zA-Z_]+\\', '<GPEP:HOME>\\\\map_cache\\\\{}\\\\'.format(
                    service_name), line)
                app_out.write(line.replace('<GPEP:HOME>', gpepconfig.ConfigModelCreator().GPEP_HOME))
    os.remove(app_source_path)
    return model_creator.create_map_proxy_app_from_file(app_dest_path, pull_in_base=False)
