"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

from gpep_apps.config import gpepconfig
from gpep_apps.jobs import models, runner
from gpep_apps.jobs.models import Task
from gpep_apps.mpas import model_creator
from gpep_apps.api import named_locks

import yaml


def run_clean_job(clean_job_model):
    """
    Runs a clean task for each task in the given job
    :param clean_job_model: the mpas.jobs.Clean model to pull the tasks from
    """
    clean_job_model.status = "inProgress"
    clean_job = clean_job_model.get_related_object()
    levels = clean_job_model.get_levels()
    for task in clean_job_model.task_set.all():
        with named_locks.getServiceLock(task.map_proxy_app.id):
            start_job(
                map_proxy_app=task.map_proxy_app, levels=levels, layers=task.get_layers(),
                min_x=clean_job.min_x, min_y=clean_job.min_y, max_x=clean_job.max_x,
                max_y=clean_job.max_y, date=clean_job.date, task=task)


def start_job(map_proxy_app, layers, min_x, min_y, max_x, max_y, levels, date, task):
    """
    Creates a new mapproxy clean operation with the given parameters on the given layers
    :param map_proxy_app: The django model for the service
    :param layers: The layers queryset to be cleaned
    :param min_x: Minimum x value of the bounding box
    :param min_y: Minimum y value of the bounding box
    :param max_x: Maximum x value of the bounding box
    :param max_y: Maximum y value of the bounding box
    :param levels: List of levels to be cleaned
    :param date: specify date to start cleaning
    :param task: task to save details of this operation under
    :return: True on success, error string on failure
    """
    log = task.get_logger()
    log.debug("Starting Clean Job")
    # create a copy of existing model to use and modify
    map_proxy_app = model_creator.create_map_proxy_app_from_dict(map_proxy_app.to_dict(), map_proxy_app.name)
    layers = [map_proxy_app.get_layers(name=layer.name).first() for layer in layers]

    # if cache exists, create temp yaml with passed in bounding box, write cleanup yaml, call clean cache method
    temp_yaml = create_temp_yaml(map_proxy_app, layers, min_x, min_y, max_x, max_y)
    clean_yaml = create_cleanup_dict(layers, min_x, min_y, max_x, max_y, levels, date)

    celery_task = runner.clean_cache.delay(temp_yaml, clean_yaml, task.id)
    # update the task with a single sql statement
    Task.objects.filter(id=task.id).update(celery_task_id=celery_task.id)

    return True


def create_temp_yaml(map_proxy_app, layers, min_x, min_y, max_x, max_y):
    """
    Creates a temporary yaml for seeding, exporting, etc.
    :param map_proxy_app: the service for which a yaml is being written
    :param layers: layers to modify with the bounds provided
    :param min_x: the minimum x (longitude) value of the bounding box
    :param min_y: the minimum y (latitude) value of the bounding box
    :param max_x: the maximum x (longitude) value of the bounding box
    :param max_y: the maximum y (latitude) value of the bounding box
    :return: dict with the updated layer bboxes
    """
    for layer in layers:
        layer.bbox = [min_x, min_y, max_x, max_y]
        layer.save()
    return map_proxy_app.to_dict()


def create_cleanup_dict(layers, min_x, min_y, max_x, max_y, levels, date):
    """
    Creates the cleanup dict
    :param layers: The layers to clean
    :param min_x: the minimum x (longitude) value of the bounding box
    :param min_y: the minimum y (latitude) value of the bounding box
    :param max_x: the maximum x (longitude) value of the bounding box
    :param max_y: the maximum y (latitude) value of the bounding box
    :param levels: the levels to clean
    :param date: the date to clean all imagery data prior to
    :return: the cleanup dict
    """
    cleanups = {}
    remove_before = {'time': '{0}T12:00:00'.format(date)}
    coverages = {}
    bbox = [min_x, min_y, max_x, max_y]
    for layer in layers:
        source = layer.get_source_name()
        coverage_name = layer.generate_coverage_name()
        coverages[coverage_name] = {'bbox': bbox, 'srs': 'EPSG:4326'}
        created_dict = {
            'caches': [source],
            'grids': layer.get_source().grids,
            'levels': levels,
            'coverages': [coverage_name]
        }
        if gpepconfig.instance().cache_type.upper() == "FILE":
            created_dict['remove_before'] = remove_before
        cleanups['clean_{0}'.format(source)] = created_dict
    return {'cleanups': cleanups, 'coverages': coverages}
