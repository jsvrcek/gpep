"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import os
import shutil

from gpep_apps.api.gpep_exception import GPEPException
from gpep_apps.config import gpepconfig
from gpep_apps.jobs import utils
from gpep_apps.mpas import model_creator, models
from gpep_apps.pepapi.celery import app as celery_app
from django.core.cache import cache as django_cache


def update_sources(map_proxy_app):
    """
    Updates sources for WMTS services to use grid based on default projection from config file
    :param map_proxy_app: service to update the services for
    """
    assert map_proxy_app.services.wmts
    default_projection = int(gpepconfig.instance().default_projection)
    keys = [grid.key for grid in map_proxy_app.get_grids() if grid.get_epsg_num() == default_projection]
    if keys:
        correct_projection_grid = keys[0]
        for source in map_proxy_app.get_sources():
            if isinstance(source, models.TileSource):
                current_grid = getattr(source, 'grid', None)
                source.grid = correct_projection_grid
                # replace tile matrix set name in url
                source.url = correct_projection_grid[:-len('_grid')].join(
                    source.url.split(current_grid[:-len('_grid')]))
                source.save()


def disable_cache(map_proxy_app):
    """
    Disables the cache for the given map proxy app
    :param map_proxy_app: the service to disable the cache for
    """
    for layer in map_proxy_app.get_layers():
        # deletes layers ensuring the layer's source ends up being a source
        layer.delete_all_caches()
    # deletes the rest of the caches
    map_proxy_app.cache_set.all().delete()
    map_proxy_app.save()
    map_proxy_app.save_to_disk()


def create_cache(map_proxy_app, log=None):
    # meta tile size of 2 x 2 instead of default (4 x 4) so lots of extra tiles are not requested at low zoom levels
    map_proxy_app.globals.cache = {'meta_size': [2, 2]}
    map_proxy_app.globals.save()

    # self.obj_conf.globals = Globals({'cache': {'meta_size': [2, 2]}})

    # this isn't doing anything good, and it has bugs
    # if map_proxy_app.services.wmts:  # or self.map_proxy_app.services.tile:
    #     update_sources(map_proxy_app)

    generate_caches(map_proxy_app)
    utils.create_cache_folder(map_proxy_app, log)

    map_proxy_app.save_to_disk()
    return True


def generate_caches(map_proxy_app):
    """
    Enables a cache for each source
    If the source is wmts: Enables a cache for each source of a WMTS service
    Otherwise:  EPSG:4326, EPSG:3857, EPSG:3395.
    @TODO make sure the cache types and django models are correct
    @TODO note sure that djangos sources is the same as the prior sources
    :param map_proxy_app: service to generate the caches for
    """
    config = gpepconfig.instance()
    default_grid = utils.get_default_grid(config.default_projection)
    for layer in map_proxy_app.get_layers():
        if not map_proxy_app.is_wmts():
            # removes all of the older caches for that layer
            layer.delete_all_caches()
        source = layer.get_source()
        if source is None:
            # don't need to create a cache
            continue
        caches_key = '{}_cache'.format(source.key)
        cache_type = config.cache_type
        grid = default_grid

        if config.cache_type == 'geopackage':
            filename = os.path.join(config.cache_dir, map_proxy_app.name, source.key + ".gpkg")
            cache_details = dict(type='geopackage', filename=filename)
            file_format = 'image/png'
        elif config.cache_type == 'compact':
            directory = os.path.join(config.cache_dir, map_proxy_app.name, caches_key)
            cache_details = dict(type='compact', directory=directory, version=1)
            file_format = None
        else:
            directory = os.path.join(config.cache_dir, map_proxy_app.name, caches_key)
            cache_details = dict(type='file', directory=directory, directory_layout='tms')
            file_format = 'image/png'

        cache = dict(
            cache_name=cache_type, sources=[source.key], format=file_format,
            request_format='image/png', grids=[grid], cache=cache_details)
        layer.set_source(model_creator.create_cache(app=map_proxy_app, key=caches_key, value=cache))


def cleanup_file(folder, file_pattern, map_proxy_app):
    """
    Utility method delete files in the folder with the given file_pattern.
    :param folder: folder to clean up
    :param file_pattern: file pattern to delete
    :param map_proxy_app: the service that generated the file
    """
    # only format the string if it needs formatting
    if '{}' in file_pattern or '{0}' in file_pattern:
        file_pattern = file_pattern.format(map_proxy_app.name)
    try:
        os.remove(os.path.join(folder, file_pattern))
    except OSError:
        pass
        # @TODO: fix this. figure out if this is an issue when deleting new services

        # raise APIException('Unable to clean the {0} file'.format(file_pattern))
        # self.log.exception('Unable to clean the {0} file'.format(file_pattern))


def delete_cache_folder(map_proxy_app, log):
    """
    Removes cache folder from cache directory
    :param map_proxy_app: the service's cache folder that you want to delete
    :param log: logger
    :return: boolean success
    """
    cache_folder = os.path.join(gpepconfig.instance().cache_dir, map_proxy_app.name)
    if not os.path.exists(cache_folder):
        return True
    try:
        # ignore_errors=True makes rmtree more reliable, ignore lccks/safeguards, deletes no matter what
        shutil.rmtree(cache_folder, ignore_errors=True)
        return True
    except OSError:
        log.exception('The {0} cache folder could not be deleted.'.format(map_proxy_app.name))
        return False


def delete_map_proxy_app(map_proxy_app, log):
    """
    Deletes map_proxy_app, as well as all associated caches and .yaml files.
    :param map_proxy_app: service to delete
    :param log: logger
    :return: True if successful
    """
    log.debug('Deleting Map Proxy App')

    map_proxy_app.cancel_all_tasks()

    if os.path.exists(map_proxy_app.file_location):
        try:
            os.remove(map_proxy_app.file_location)
        except OSError:
            log.exception('The file {0} could not be deleted. Please check permissions on the Apps folder.'
                          .format(map_proxy_app.file_location))
            raise GPEPException('A file could not be deleted. Please check permissions on the Apps folder.')
    config = gpepconfig.instance()

    cleanup_file(config.clean_seed_dir, '{}_clean.yaml', map_proxy_app)
    cleanup_file(config.clean_seed_dir, '{}_seed.yaml', map_proxy_app)
    cleanup_file(config.clean_seed_dir, '{}_pinfo.yaml', map_proxy_app)
    cleanup_file(config.temp_dir, '{}_temp.yaml', map_proxy_app)
    cleanup_file(config.log_dir, '{}_clean.log', map_proxy_app)
    cleanup_file(config.log_dir, '{}_seed.log', map_proxy_app)
    cleanup_file(config.log_dir, '{}_progress', map_proxy_app)
    cleanup_file(config.log_dir, '{}_progress.tmp', map_proxy_app)
    cleanup_file(config.output_dir, '{}.pep', map_proxy_app)

    delete_cache_folder(map_proxy_app, log)
    map_proxy_app.delete()
    # @TODO: implement gpkg_sqlserver deletions
    django_cache.delete('services')
    return True
