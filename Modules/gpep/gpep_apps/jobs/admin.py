"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

from django.contrib import admin
from .models import Clean, Job, Seed, Package, Task

# admin.site.register(Job)
admin.site.register(Clean)
admin.site.register(Seed)
admin.site.register(Package)
admin.site.register(Task)


class JobAdmin(admin.ModelAdmin):
    list_display = ('id', 'job_type', 'started', 'finished')
admin.site.register(Job, JobAdmin)

#
# class LayerAdmin(admin.ModelAdmin):
#     list_display = ('service','name', 'title')
# admin.site.register(Layer, LayerAdmin)
#
#
# class CleanAdmin(admin.ModelAdmin):
#     list_display = ('minX','maxX', 'minY', 'maxY', 'minZoom', 'maxZoom', 'date')
# admin.site.register(Clean, CleanAdmin)
#
#
# class SeedAdmin(admin.ModelAdmin):
#     list_display = ('minX','maxX', 'minY', 'maxY', 'minZoom', 'maxZoom')
# admin.site.register(Seed, SeedAdmin)
#
#
# # Register your models here.
