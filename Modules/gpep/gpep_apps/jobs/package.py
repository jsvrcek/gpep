"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import os
import runner
import utils

from gpep_apps.config import gpepconfig
from gpep_apps.jobs.models import Task
from gpep_apps.mpas import model_creator, models
from mapproxy.srs import SRS, get_epsg_num
from gpep_apps.api import named_locks


NSG, NW, STANDARD = ['NSG', 'NW', 'STANDARD']


def run_package_job(job_model):
    """
    Runs all of the tasks under a package job
    :param job_model: the job model to pull the tasks from
    """
    job_model.status = "inProgress"
    package_job = job_model.get_related_object()
    levels = job_model.get_levels()
    for task in job_model.task_set.all():
        with named_locks.getServiceLock(task.map_proxy_app.id):
            start_job(
                map_proxy_app=task.map_proxy_app, levels=levels, layers=task.get_layers(),
                min_x=package_job.min_x, min_y=package_job.min_y, max_x=package_job.max_x, max_y=package_job.max_y,
                srs=package_job.srs, package_type=package_job.package_type, task=task)


def start_job(map_proxy_app, layers, srs, min_x, min_y, max_x, max_y, levels, package_type, task):
    """
    Starts a new package task
    :param map_proxy_app: service to grab layer from
    :param layers: layers queryset
    :param srs: output srs list
    :param min_x: bounding box to export min x
    :param min_y: bounding box to export min y
    :param max_x: bounding box to export max x
    :param max_y: bounding box to export max y
    :param levels: zoom levels to export. list
    :param package_type: the type of package to package into
    :param task: the parent task
    :return: boolean success
    """
    log = task.get_logger()
    log.debug("Starting Package Job")
    map_proxy_app = model_creator.create_map_proxy_app_from_dict(map_proxy_app.to_dict(), map_proxy_app.name)
    layers = [map_proxy_app.get_layers(name=layer.name, title=layer.title).first() for layer in layers]

    service_dict = create_service_dict(
        map_proxy_app, package_type, srs, layers, [min_x, min_y, max_x, max_y], task, levels)

    if package_type == 'NW':
        service_dict['grids']['EPSG3857']['bbox'] = list(SRS(4326).transform_bbox_to(SRS(3857), (min_x, min_y, max_x,
                                                                                                 max_y)))

    seed_yaml_file = create_seed_package_dict(levels, layers, min_x, min_y, max_x, max_y)

    # not doing obj version for seed_package (not necessary)
    celery_task = runner.seed_package.delay(service_dict, seed_yaml_file, task.id)
    Task.objects.filter(id=task.id).update(celery_task_id=celery_task.id)
    return True


def create_service_dict(map_proxy_app, package_type, srs, layers, bbox, task, levels):
    """
    Creates a service yaml file for exporting the given layers (in the given package type/srs)
    to the correct geopackage output type. overwrites existing file, so make sure a temp file is used.
    :param map_proxy_app: map proxy app to create the service dict from
    :param package_type: profile of geopackage to export
    :param srs: srs to output geopackage in
    :param layers: layers to include in the geopackage
    :param bbox: bbox in the form [min_x,min_y,max_x,max_y]
    :param task: the parent task
    :param levels: list of levels to package
    :return: dict with all the services
    """
    # generates new caches and adds them to map_proxy_app
    _generate_caches(map_proxy_app, layers, package_type, srs, bbox, task, levels)

    map_proxy_app.get_services().wms = None
    map_proxy_app.get_services().wmts = None
    map_proxy_app.get_services().tms = None
    map_proxy_app.save()

    return map_proxy_app.to_dict(add_temp_caches=False)


def _generate_caches(map_proxy_app, layers, package, srs, bbox, task, levels):
    """
    Enables a cache for each source with the provided SRS
    :param map_proxy_app: base app to generate caches for
    :param layers: layers to generate caches for
    :param package: GeoPackage package type, accepts 'NW','NSG','STANDARD'.
    :param srs: SRS to package to. in the form of 'EPSG:4326'.
    :param bbox: bounding box for the new caches
    :param task: the parent task
    :param levels: list of levels to cache
    """
    # If srs is a list, gets the least of all of them
    srs = sorted(srs)[0] if isinstance(srs, list) and srs else srs

    for layer in layers:
        source = layer.get_source()
        if isinstance(source, models.Cache):
            assert source.key.endswith('_cache')
            base_source_name = source.key[:-len('_cache')]
        elif isinstance(source, models.SourceCommons):
            base_source_name = source.key
        else:
            assert source is None
            continue

        # set package name
        cache_name = '{}_package'.format(base_source_name)

        # set table name
        table_name = 'tiles' if package is NW else str(source.key)

        grid = NW if package is NW else utils.get_default_grid(str(get_epsg_num(srs)))
        filename = utils.get_unique_filename(gpepconfig.instance().output_dir, map_proxy_app.name, 'gpkg')
        shortname = os.path.basename(filename)
        # writing the filename now to reserve it because of async processes
        with open(filename, 'w') as fp:
            fp.write('')

        task.result_filename = filename
        task.save()

        cache = create_cache(map_proxy_app, source, grid, package, table_name, bbox, cache_name, shortname, levels)
        layer.set_source(cache)


def create_cache(map_proxy_app, source, grid, package, table_name, bbox, cache_name, filename, levels):
    """
    Generates a cache with the given parameters
    :param map_proxy_app: the app to create the cache inside of
    :param source: source for the new cache
    :param grid: grid name
    :param package: package type
    :param table_name: table name
    :param bbox: bounding box
    :param cache_name: name of the cache to use
    :param filename: the filename to go into cache.cache
    :param levels: list of levels to cache
    :return: models.Cache object filled in with the given parameters
    """
    inner_cache = dict(type='geopackage', table_name=table_name, profile=package,
                       directory=gpepconfig.instance().output_dir, filename=filename, bbox=bbox,
                       bbox_srs='EPSG:4326', levels=False, limited_levels=levels)
    cache = dict(sources=[source.key], format='image/png', request_format='image/png',
                 meta_size=[1, 1], grids=[grid], cache=inner_cache)
    return model_creator.create_cache(app=map_proxy_app, key=cache_name, value=cache)


def create_seed_package_dict(levels, layers, min_x, min_y, max_x, max_y):
    """
    Creates a seed yaml file used for seed tasks.
    :param levels: the array of levels that are to be seeded (type: list)
    :param layers: the layers to be seeded (if None, defaults to all) (type: list)
    :param min_x: the minimum x (longitude) value of the bounding box (type: float)
    :param min_y: the minimum y (latitude) value of the bounding box (type: float)
    :param max_x: the maximum x (longitude) value of the bounding box (type: float)
    :param max_y: the maximum y (latitude) value of the bounding box (type: float)
    :return: boolean success
    """
    coverages = {}
    seeds = {}
    bbox = [min_x, min_y, max_x, max_y]
    levels_arr = [level for level in levels]

    for layer in layers:
        source = layer.get_source_name()
        coverage_name = layer.generate_coverage_name()
        coverages[coverage_name] = {
            'bbox': bbox,
            'srs': 'EPSG:4326'
        }
        seed_name = layer.generate_seed_name()
        seeds[seed_name] = {
            'caches': [source],
            'levels': levels_arr,
            'coverages': [coverage_name]
        }
    return {'seeds': seeds, 'coverages': coverages}
