"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import coverage
from django.core.management.base import BaseCommand
from django.core.management import call_command


class Command(BaseCommand):
    help = 'Runs the tests and generates coverage reports for them'

    # allowing the use of additional arguments
    def add_arguments(self, parser):
        # the -m flag on coverage report prints out all line numbers that aren't covered
        parser.add_argument('-m', dest='show_linenums', action='store_const', const='-m', default='')

    def handle(self, *args, **options):
        sources = ['gpep_apps']
        omissions = [
            '*/migrations/*',
            '*/test_*',
            '*/__init__.py',
            '*/management/*',
            '*/apps.py',
            '*/admin.py',
            '*/urls.py',
            '*/wsgi.py',
            '*/pepapi/celery.py'
        ]
        cov = coverage.coverage(branch=True, source=sources, omit=omissions)
        cov.start()
        call_command('test')
        cov.stop()
        cov.save()
        show_missing = options.get('show_linenums') == '-m'
        cov.report(show_missing=show_missing)
        cov.html_report()

if __name__ == "__main__":
    import os
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gpep_apps.pepapi.settings")
    Command().handle()
