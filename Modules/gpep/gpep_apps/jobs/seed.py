"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import os
import runner

from gpep_apps.config import gpepconfig
from gpep_apps.jobs.models import Task
from gpep_apps.mpas import model_creator
from gpep_apps.api import named_locks


def run_seed_job(job_model):
    """
    Runs the seed tasks for each task in the model
    :param job_model: the model to pull the tasks from
    """

    job_model.status = "inProgress"
    seed_job = job_model.get_related_object()
    levels = job_model.get_levels()
    for task in job_model.task_set.all():
        with named_locks.getServiceLock(task.map_proxy_app.id):
            start_job(
                map_proxy_app=task.map_proxy_app, levels=levels, layers=task.get_layers(),
                num_cache_processes=2, min_x=seed_job.min_x, min_y=seed_job.min_y,
                max_x=seed_job.max_x, max_y=seed_job.max_y, task=task)


def start_job(map_proxy_app, levels, layers, num_cache_processes, min_x, min_y, max_x, max_y, task):
    """
    Seeds the cache and writes the seed file with the parameters entered into the dialog box.
    :param map_proxy_app: the service whose cache is being seeded
    :param levels: the list of levels that are to be seeded
    :param num_cache_processes: the number of cache processes that should be run for this seeding
    :param min_x: the minimum x (longitude) value of the bounding box
    :param min_y: the minimum y (latitude) value of the bounding box
    :param max_x: the maximum x (longitude) value of the bounding box
    :param max_y: the maximum y (latitude) value of the bounding box
    :param task: the parent task
    :return: boolean success
    """
    log = task.get_logger()
    log.debug("Starting Seed Job")
    map_proxy_app = model_creator.create_map_proxy_app_from_dict(map_proxy_app.to_dict(), map_proxy_app.name)
    layers = [map_proxy_app.get_layers(name=layer.name).first() for layer in layers]

    seed_yaml = create_seed_dict(levels, layers, min_x, min_y, max_x, max_y)
    mapproxy_yaml = create_yaml_dict(map_proxy_app)

    map_proxy_app.save_to_disk()

    celery_task = runner.seed_cache.delay(num_cache_processes, mapproxy_yaml, seed_yaml, task.id)
    Task.objects.filter(id=task.id).update(celery_task_id=celery_task.id)

    return True


def create_yaml_dict(map_proxy_app):
    """
    Creates a temporary yaml for seeding, exporting, etc.
    :param map_proxy_app: app to create temp dict from
    :return: temp yaml as a dict
    """
    for cache in map_proxy_app.get_caches():
        if not cache.cache:
            cache.cache = {}
        cache.cache['directory'] = os.path.join(gpepconfig.instance().cache_dir, map_proxy_app.name, cache.key)
        cache.cache['directory_layout'] = cache.cache.get('directory_layout', 'tms')
        cache.cache['type'] = cache.cache.get('type', 'file')
        cache.save()
    return map_proxy_app.to_dict()


def create_seed_dict(levels, layers, min_x, min_y, max_x, max_y):
    """
    Creates a seed yaml file used for seed tasks.
    :param levels: the array of levels that are to be seeded
    :param layers: the django layers to be seeded (if None, defaults to all)
    :param min_x: the minimum x (longitude) value of the bounding box
    :param min_y: the minimum y (latitude) value of the bounding box
    :param max_x: the maximum x (longitude) value of the bounding box
    :param max_y: the maximum y (latitude) value of the bounding box
    :return: seed yaml as a dict
    """
    coverages = {}
    seeds = {}
    bbox = [min_x, min_y, max_x, max_y]

    for layer in layers:
        coverage_name = layer.generate_coverage_name()
        coverages[coverage_name] = {'bbox': bbox, 'srs': 'EPSG:4326'}
        source_name = layer.get_source_name()
        seeds[source_name] = {'caches': [source_name],
                              'coverages': [coverage_name],
                              'levels': levels}
    return {'seeds': seeds, 'coverages': coverages}
