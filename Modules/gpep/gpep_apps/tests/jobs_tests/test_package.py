"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

from gpep_apps.jobs import package
from gpep_apps.mpas import model_creator
from gpep_apps.tests import test_utils


class PackageTest(test_utils.JobTest):
    def test_integration_nc(self):
        self._package_test('nc')

    def test_integration_evwhs(self):
        self._package_test('evwhs')

    def test_update_no_sources(self):
        model_creator.create_map_proxy_app_from_file(test_utils.NC_PATH).get_grids()
        model_creator.create_map_proxy_app_from_file(test_utils.EVWHS_PATH).get_grids()

    def test_write_seed_package_file(self):
        self.create_yaml_model()
        seed_package_dict = package.create_seed_package_dict([2], self.yaml_model.get_layers(), 0, 1, 0, 1)
        expected_dict = {
            'coverages': {
                'coverage_Orthoimagery_Latest': {
                    'bbox': [0, 1, 0, 1],
                    'srs': 'EPSG:4326'
                }
            },
            'seeds': {
                'Orthoimagery_Latest_package': {
                    'coverages': ['coverage_Orthoimagery_Latest'],
                    'levels': [2],
                    'caches': ['Orthoimagery_Latest_cache']
                }
            }
        }
        self.assertEqual(expected_dict, seed_package_dict)

    def test_create_service_dict_no_caches(self):
        """Tests the create_service_dict function with no caches in the map proxy app."""
        self.create_yaml_model()
        for layer in self.yaml_model.get_layers():
            layer.delete_all_caches()
        self.assertEqual(0, len(self.yaml_model.get_caches()))
        package.create_service_dict(self.yaml_model, "NW", "EPSG:4326", self.yaml_model.get_layers(),
                                    [0, 0, 1, 1], self.create_task(self.yaml_model), [1, 2])
        self.assertEqual(len(self.yaml_model.get_layers()), len(self.yaml_model.get_caches()))

    def test_package_no_sources(self):
        self.create_yaml_model()
        layers = self.yaml_model.get_layers()
        for layer in layers:
            layer.set_source(None)
        self.yaml_model.get_caches().delete()
        self.assertEqual(0, len(self.yaml_model.get_caches()))
        package.create_service_dict(self.yaml_model, "NW", "EPSG:4326", layers,
                                    [0, 0, 1, 1], self.create_task(self.yaml_model), [1, 2])
        self.assertEqual(0, len(self.yaml_model.get_caches()))
