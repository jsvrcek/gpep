"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import mock

from gpep_apps.tests import test_utils
from gpep_apps.jobs import export


class ExportTest(test_utils.JobTest):
    @mock.patch("gpep_apps.jobs.runner.export_service.delay")
    @mock.patch("{}.open".format(export.__name__))
    def test_start_job_bad_filename(self, open_mock, export_service_mock):
        open_mock.side_effect = IOError
        export_service_mock.return_value.id = 0
        self.create_yaml_model()
        task = self.create_task(self.yaml_model, 'export')

        with mock.patch("logging.Logger.error") as log_mock:
            export.start_job(self.yaml_model, task)
            self.assertTrue(log_mock.called)
        task.refresh_from_db()
