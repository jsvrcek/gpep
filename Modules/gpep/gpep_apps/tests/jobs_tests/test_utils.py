"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import mock
import os
import shutil

from gpep_apps.jobs import utils
from gpep_apps.tests import test_utils


class TestUtils(test_utils.CacheManagerTest):

    def test_unique_filename_does_not_exist(self):
        path = os.path.join(self.resources_dir, 'test.txt')
        # ensure that path does not exist
        if os.path.exists(path):
            os.remove(path)
        returned_path = utils.get_unique_filename(self.resources_dir, 'test', 'txt')
        self.assertEqual(path, returned_path)

    def test_unique_filename_one_already_exists(self):
        path = os.path.join(self.resources_dir, 'test.txt')
        # ensure that path does not exist
        with open(path, 'a'):
            pass
        returned_path = utils.get_unique_filename(self.resources_dir, 'test', 'txt')
        self.assertNotEqual(path, returned_path)

    def test_unique_filename_folder_does_not_exist(self):
        path = os.path.join(self.resources_dir, 'unique_filename_folder')
        # ensure that dir does not exist
        if os.path.exists(path):
            shutil.rmtree(path)
        returned_path = utils.get_unique_filename(path, 'test', 'txt')
        self.assertTrue(os.path.exists(path))
        if os.path.exists(path):
            shutil.rmtree(path)
        self.assertEqual(os.path.join(path, 'test.txt'), returned_path)

    def test_create_cache_folder_exists(self):
        map_mock = mock.Mock()
        map_mock.name = 'map_mock'
        path = os.path.join(self.config_mock.cache_dir, 'map_mock')
        if not os.path.exists(path):
            os.makedirs(path)
        output_path = utils.create_cache_folder(map_mock, mock.Mock())
        self.assertEqual(path, output_path)
        os.removedirs(output_path)

    def test_create_cache_folder_does_not_exist(self):
        map_mock = mock.Mock()
        map_mock.name = 'map_mock'
        out_path = utils.create_cache_folder(map_mock, mock.Mock())
        self.assertEqual(os.path.join(self.config_mock.cache_dir, 'map_mock'), out_path)
        self.assertTrue(os.path.exists(out_path))
        os.removedirs(out_path)

    @mock.patch('os.mkdir')
    def test_create_cache_folder_oserror(self, mkdir_mock):
        mkdir_mock.side_effect = OSError
        map_mock = mock.Mock()
        map_mock.name = 'map_mock'
        config_mock = mock.Mock()
        config_mock.cache_dir = self.resources_dir
        self.assertFalse(os.path.exists(os.path.join(self.resources_dir, 'map_mock')))
        self.assertIsNone(utils.create_cache_folder(map_mock, config_mock))
        self.assertFalse(os.path.exists(os.path.join(self.resources_dir, 'map_mock')))

    def test_get_unique_folder_subfolder_does_not_exist(self):
        path = os.path.join(self.config_mock.temp_dir, 'tmp')
        self.assertFalse(os.path.exists(path))
        utils.get_unique_folder(path, 'tmp')
        self.assertTrue(os.path.exists(path))

    def test_get_unique_folder_subfolder_folder_already_exists(self):
        path = os.path.join(self.config_mock.temp_dir, 'tmp')
        os.mkdir(path)
        self.assertNotEqual(path, utils.get_unique_folder(self.config_mock.temp_dir, 'tmp'))
