"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import mock
import os

from gpep_apps.api.gpep_exception import GPEPException
from gpep_apps.jobs import djcachemanager
from gpep_apps.tests import test_utils


class DJCacheManagerTest(test_utils.CacheManagerTest):
    def setUp(self):
        super(DJCacheManagerTest, self).setUp()
        self.create_yaml_model()

    def test_generate_caches_other_cache_type(self):
        config_mock = self.config_mock
        config_mock.cache_type = 'other_package_type'
        self.assertLessEqual(1, len(self.yaml_model.get_sources()))
        djcachemanager.generate_caches(self.yaml_model)
        passed = False
        for cache in self.yaml_model.get_caches():
            if '_cache' in cache.key and cache.cache is not None:
                self.assertEqual('file', cache.cache['type'])
                self.assertIn('directory', cache.cache)
                self.assertIn('directory_layout', cache.cache)
                passed = True
        assert passed

    def test_generate_caches_geopackage(self):
        config_mock = self.config_mock
        config_mock.cache_type = 'geopackage'
        self.assertLessEqual(1, len(self.yaml_model.get_sources()))
        djcachemanager.generate_caches(self.yaml_model)
        passed = False
        for cache in self.yaml_model.get_caches():
            if '_cache' in cache.key and cache.cache is not None:
                self.assertEqual('geopackage', cache.cache['type'])
                self.assertIn('filename', cache.cache)
                passed = True
        assert passed

    def test_generate_caches_overwrite_old_ones(self):
        config_mock = self.config_mock
        config_mock.cache_type = 'gpkg_sqlserver'
        config_mock.db_conn_file = 'db_conn_file'
        self.assertLessEqual(1, len(self.yaml_model.get_sources()))
        djcachemanager.generate_caches(self.yaml_model)
        num_caches = len(self.yaml_model.get_caches())
        djcachemanager.generate_caches(self.yaml_model)
        self.assertEqual(num_caches, len(self.yaml_model.get_caches()))

    def test_delete_cache_folder_exists(self):
        self.create_yaml_model()
        dir_path = os.path.join(self.config_mock.cache_dir, self.yaml_model.name)
        os.mkdir(dir_path)
        with open(os.path.join(dir_path, 'temp'), mode='a+') as temp_file:
            temp_file.write('text to be deleted')
        djcachemanager.delete_cache_folder(self.yaml_model, mock.Mock())
        self.assertFalse(os.path.exists(dir_path))

    def test_delete_cache_folder_does_not_exist(self):
        """Tests delete_cache_folder when the folder does not exist"""
        self.create_yaml_model()
        dir_path = os.path.join(self.config_mock.cache_dir, self.yaml_model.name)
        self.assertFalse(os.path.exists(dir_path))
        djcachemanager.delete_cache_folder(self.yaml_model, mock.Mock())
        self.assertFalse(os.path.exists(dir_path))

    @mock.patch("shutil.rmtree")
    def test_delete_cache_folder_cant_delete(self, shutil_mock):
        shutil_mock.side_effect = OSError
        self.create_yaml_model()
        dir_path = os.path.join(self.config_mock.cache_dir, self.yaml_model.name)
        os.mkdir(dir_path)
        log_mock = mock.Mock()
        self.assertFalse(djcachemanager.delete_cache_folder(self.yaml_model, log_mock))
        log_mock.exception.assert_called()
        os.rmdir(dir_path)

    def test_cleanup_file_no_replacement(self):
        self.create_yaml_model()
        dir_path = os.path.join(self.config_mock.log_dir, self.yaml_model.name)
        djcachemanager.cleanup_file(dir_path, 'not_a_log_file', self.yaml_model)
        self.assertFalse(os.path.exists(os.path.join(dir_path, 'not_a_log_file')))

    def test_disable_cache(self):
        self.create_yaml_model()
        djcachemanager.disable_cache(self.yaml_model)
        for layer in self.yaml_model.get_layers():
            self.assertNotIsInstance(layer.get_source(), str)
            self.assertIsNotNone(layer.get_source())

    def test_generate_caches_layer_with_no_source(self):
        self.create_yaml_model()
        for layer in self.yaml_model.get_layers():
            layer.set_source(None)
        for cache in self.yaml_model.get_caches():
            cache.delete()
        self.assertEqual(0, len(self.yaml_model.get_caches()))
        djcachemanager.generate_caches(self.yaml_model)
        self.assertEqual(0, len(self.yaml_model.get_caches()))

    def test_delete_map_proxy_app_bad_file_location(self):
        self.create_yaml_model()
        self.yaml_model.file_location = 'bad_path'
        self.yaml_model.save()
        djcachemanager.delete_map_proxy_app(self.yaml_model, mock.Mock())

    @mock.patch('gpep_apps.jobs.djcachemanager.update_sources')
    def test_create_cache_no_wmts(self, update_sources_mock):
        self.create_yaml_model()
        self.yaml_model.services.wmts = None
        self.yaml_model.services.save()
        djcachemanager.create_cache(self.yaml_model, mock.Mock)
        for cache in self.yaml_model.get_caches():
            self.assertNotEqual(cache.get_source(), cache)
        self.assertFalse(update_sources_mock.called)
