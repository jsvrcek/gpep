"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import mock

from gpep_apps.config import models
from gpep_apps.tests import test_utils


class ConfigModelsTest(test_utils.GPEPTest):
    @mock.patch('gpep_apps.config.models.is_linux')
    def test_get_defaults_linux(self, is_linux_mock):
        """Test get_defaults when os in linux."""
        is_linux_mock.return_value = True
        self.assertEqual(models.is_linux(), True)
        self.assertEqual((r'/opt/pep', 'conf', r'/opt/pep/bin/mapproxy-seed'), models.get_defaults())

    @mock.patch('gpep_apps.config.models.is_linux')
    def test_get_defaults_windows(self, is_linux_mock):
        """Tests get_defaults when os is windows."""
        is_linux_mock.return_value = False
        self.assertEqual(models.is_linux(), False)
        self.assertEqual((r'C:\ProgramData\GPEP', 'config', r'C:\Python27\Scripts\mapproxy-seed.exe'),
                         models.get_defaults())
