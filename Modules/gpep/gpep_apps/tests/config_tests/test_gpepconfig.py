"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import mock
import os

from gpep_apps.api.gpep_exception import GPEPException
from gpep_apps.config import gpepconfig, models
from gpep_apps.tests import test_utils


class GPEPConfigTest(test_utils.GPEPTest):
    def test_instance_uses_caching(self):
        """Ensures that gpepconfig.instance() acutally uses a cached model if one exists."""
        models.Config.objects.all().delete()
        self.assertIsInstance(gpepconfig.instance(), models.Config)
        self.assertEqual(1, len(models.Config.objects.all()))
        self.assertIsInstance(gpepconfig.instance(), models.Config)
        self.assertEqual(1, len(models.Config.objects.all()))

    @mock.patch("logging.Logger.error")
    def test_make_generic_paths_bad_path(self, logger_mock):
        """Test using make_generic_paths with a bad path."""
        gpepconfig.make_generic_paths(self.fake_path)
        self.assertTrue(logger_mock.called)


class ConfigModelCreatorTest(test_utils.CacheManagerTest):
    def test_create_model_no_config_file(self):
        """Test creating a GPEPConfig model with a bad config path."""
        creator = gpepconfig.ConfigModelCreator()
        creator._config_file = self.fake_path
        with self.assertRaises(GPEPException):
            creator.create_model()

    def test_create_model_bad_config_file(self):
        """Tests creating a GPEPConfig model with bad data inside of it."""
        creator = gpepconfig.ConfigModelCreator()
        creator.log = mock.Mock()
        config_path = os.path.join(self.config_mock.temp_dir, 'temp.cfg')
        with open(config_path, 'a') as f:
            f.write('thi$ s#ould n0t be /*- ab]e to be interpreted~~')
        creator._config_file = config_path
        with self.assertRaises(GPEPException):
            creator.create_model()
