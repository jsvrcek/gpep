"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

from gpep_apps.mpas import fields
from gpep_apps.tests import test_utils


class JSONFieldTest(test_utils.GPEPTest):
    def setUp(self):
        super(JSONFieldTest, self).setUp()
        self.json_field = fields.JSONField()

    def assertValueChanged(self, starting_value, middle_check=None, end_check=None):
        """
        Goes through the value setting process for the JSONField
        :param starting_value: what value to initially put in the field
        :param middle_check: if not none, this is what value is expected in the database
        :param end_check: what value is expected to be returned. if none, checks against the starting value
        """
        value = self.json_field.get_db_prep_save(value=starting_value, connection=None)
        if middle_check is not None:
            self.assertEqual(middle_check, value)
        if end_check is None:
            end_check = starting_value
        self.assertEqual(end_check, self.json_field.from_db_value(value, None, None, None))

    def test_json_field_number(self):
        self.assertValueChanged(123)

    def test_json_field_string(self):
        self.assertValueChanged('abc')

    def test_json_field_list(self):
        self.assertValueChanged((1, 2, 3))
        self.assertValueChanged(('1', '2', '3'))
        self.assertValueChanged((1, '2', 3, None))

    def test_json_field_none(self):
        self.assertValueChanged(None)
