"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

import ctypes
import mock
import os

from gpep_apps.status import file_utils
from gpep_apps.tests import test_utils


class FileUtilsTest(test_utils.CacheManagerTest):
    @mock.patch("gpep_apps.status.file_utils.get_windows_free_space")
    @mock.patch("gpep_apps.status.file_utils.get_statvfs")
    @mock.patch("gpep_apps.status.file_utils.is_linux")
    def test_get_available_storage_linux(self, is_linux_mock, stat_mock, windows_free_space_mock):
        """Test getting the available storage on linux."""
        is_linux_mock.return_value = True
        self.assertTrue(file_utils.is_linux())
        stat_mock_return_value = mock.Mock()
        stat_mock_return_value.f_frsize = 1000
        stat_mock_return_value.f_bavail = 2000
        stat_mock.return_value = stat_mock_return_value
        return_value = file_utils.get_available_storage(config=mock.Mock())
        self.assertEqual(1000 * 2000, return_value)
        windows_free_space_mock.assert_not_called()

    @mock.patch("gpep_apps.status.file_utils.get_statvfs")
    @mock.patch("gpep_apps.status.file_utils.get_windows_free_space")
    @mock.patch("gpep_apps.status.file_utils.is_linux")
    def test_get_available_storage_windows(self, is_linux_mock, windows_free_space_mock, get_statvfs_mock):
        """Test getting the available storage on windows."""
        is_linux_mock.return_value = False
        self.assertFalse(file_utils.is_linux())

        free_bytes = ctypes.c_ulonglong(1000)
        total_bytes = ctypes.c_ulonglong(2000)
        windows_free_space_mock.return_value = free_bytes, total_bytes

        return_value = file_utils.get_available_storage(config=mock.Mock())
        self.assertEqual(1000, return_value)
        get_statvfs_mock.assert_not_called()

    def test_get_size_with_folder(self):
        """Test getting the size of a folder with innter folders."""
        start_path = os.path.join(test_utils.STATIC_RESOURCES_DIR, 'test_size_24_bytes')
        return_value = file_utils.get_size(start_path=start_path)
        self.assertEqual(return_value, 24)

    def test_get_size(self):
        """Test getting the size of a folder without inner folders."""
        start_path = os.path.join(test_utils.STATIC_RESOURCES_DIR, 'test_size_24_bytes', 'test_size_16_bytes')
        return_value = file_utils.get_size(start_path=start_path)
        self.assertEqual(return_value, 16)

    def test_get_size_directory_does_not_exist(self):
        """Test getting the size of a directory that does not exist."""
        start_path = os.path.join(test_utils.STATIC_RESOURCES_DIR, 'test_size_24_bytes_does_not_exist')
        return_value = file_utils.get_size(start_path=start_path)
        self.assertEqual(return_value, 0)
