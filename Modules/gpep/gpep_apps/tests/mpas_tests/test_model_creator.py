"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
"""

from gpep_apps.api.gpep_exception import GPEPException
from gpep_apps.mpas import model_creator, models
from gpep_apps.tests import test_utils


class ModelCreatorTest(test_utils.GPEPTest):

    def setUp(self):
        super(ModelCreatorTest, self).setUp()
        self.yaml_model = model_creator.create_map_proxy_app_from_dict({})

    def test_create_model_no_save(self):
        """Tests creating a model and not saving it."""
        return_val = model_creator.create_model(models.Watermark, data={}, save_model=False)
        self.assertIsNone(return_val.id)

    def test_create_model_none(self):
        """Tests creating a model with None for data."""
        return_val = model_creator.create_model(models.Watermark, data=None)
        self.assertIsInstance(return_val, models.Watermark)

    def test_create_list_with_not_list(self):
        """Tests creating a list with something that is not a list."""
        with self.assertRaises(GPEPException):
            model_creator.create_list(in_list=123, creator=model_creator.create_cache)

    def test_create_list_with_none(self):
        """Tests creating a list with no data. This cannot throw an error."""
        try:
            model_creator.create_list(in_list=None, creator=model_creator.create_map_proxy_app_from_file)
        except Exception:
            assert False

    def test_create_source_wms(self):
        """Test creating a wms source."""
        source = model_creator.create_source({'type': 'wms', 'wms_opts': {}, 'image': {}}, self.yaml_model)
        self.assertIsInstance(source, models.WMSSource)
        self.assertIsInstance(source.wms_opts, models.WMSOpts)
        self.assertIsInstance(source.image, models.SourceImage)

    def test_create_source_mapserver(self):
        """Test creating a mapserver source."""
        source = model_creator.create_source({'type': 'mapserver', 'wms_opts': {}, 'mapserver': {}}, self.yaml_model)
        self.assertIsInstance(source, models.MapServerSource)
        self.assertIsInstance(source.wms_opts, models.WMSOpts)
        self.assertIsInstance(source.mapserver, models.MapServerOpts)

    def test_create_source_tile(self):
        """Test creating a tile source."""
        source = model_creator.create_source({'type': 'tile', 'http': {}}, self.yaml_model)
        self.assertIsInstance(source, models.TileSource)
        self.assertIsInstance(source.http, models.HTTPOpts)

    def test_create_source_mapnik(self):
        """Test creating a mapnik source."""
        source = model_creator.create_source({'type': 'mapnik', 'image': {}}, self.yaml_model)
        self.assertIsInstance(source, models.MapnikSource)
        self.assertIsInstance(source.image, models.ImageOpts)

    def test_create_source_debug(self):
        """Test creating a debug source."""
        source = model_creator.create_source({'type': 'debug'}, self.yaml_model)
        self.assertIsInstance(source, models.DebugSource)

    def test_create_source_not_implemented(self):
        """Test creating a source that is an invalid type."""
        with self.assertRaises(GPEPException):
            model_creator.create_source({'type': 'NOT A VALID SOURCE'}, None)

    def test_create_watermark(self):
        """Test creating a watermark."""
        self.assertIsNone(model_creator.create_watermark(None))
        watermark = model_creator.create_watermark({})
        self.assertIsNotNone(watermark)
        self.assertIsInstance(watermark, models.Watermark)

    def test_create_cache(self):
        """Test creating a cache."""
        cache = model_creator.create_cache({}, self.yaml_model)
        self.assertIsInstance(cache, models.Cache)

    def test_create_cache_with_existing(self):
        """Test creating a cache when there are existing caches with and without the same name."""
        self.assertEqual(0, len(self.yaml_model.get_caches()))
        model_creator.create_cache({}, self.yaml_model, key='cache0')
        self.assertEqual(1, len(self.yaml_model.get_caches()))
        model_creator.create_cache({}, self.yaml_model, key='cache1')
        self.assertEqual(2, len(self.yaml_model.get_caches()))
        model_creator.create_cache({}, self.yaml_model, key='cache0')
        self.assertEqual(2, len(self.yaml_model.get_caches()))

    def test_create_services_tms(self):
        """Test creating a tms service."""
        model = model_creator.create_services({'tms': {}})
        self.assertIsInstance(model.tms, models.TMSService)
        self.assertIsNone(getattr(model, 'kml'))
        self.assertIsNone(getattr(model, 'demo'))
        self.assertIsNone(getattr(model, 'wmts'))
        self.assertIsNone(getattr(model, 'wms'))

    def test_create_services_demo_kml(self):
        """Test creating a service object with demo and kml services."""
        model = model_creator.create_services({'demo': {}, 'kml': {}})
        self.assertIsInstance(model.demo, models.DemoService)
        self.assertIsInstance(model.kml, models.KMLService)
        self.assertIsNone(getattr(model, 'tms'))
        self.assertIsNone(getattr(model, 'wmts'))
        self.assertIsNone(getattr(model, 'wms'))

    def test_create_services_wmts_wms(self):
        """Test creating a service object with wms and wmts services."""
        model = model_creator.create_services({'wmts': {}, 'wms': {}})
        self.assertIsInstance(model.wmts, models.WMTSService)
        self.assertIsInstance(model.wms, models.WMSService)
        self.assertIsNone(getattr(model, 'kml'))
        self.assertIsNone(getattr(model, 'demo'))
        self.assertIsNone(getattr(model, 'tms'))
