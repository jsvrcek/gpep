"""
/* The MIT License (MIT)
 *
 * Copyright (c) 2017 Reinventing Geospatial, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

WSGI config for pepapi project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/


# SOURCES
# http://flask.pocoo.org/docs/0.11/patterns/appdispatch/
# https://www.toptal.com/django/django-flask-and-redis-sharing-user-sessions-between-frameworks


"""
from django.core.wsgi import get_wsgi_application
from mapproxy import multiapp
from werkzeug.exceptions import NotFound
from werkzeug.wsgi import DispatcherMiddleware
from whitenoise import WhiteNoise

from gpep_apps.config import gpepconfig
from gpep_apps.pepapi import settings

gpepapi_app = get_wsgi_application()

gpepConfig = gpepconfig.instance()
apps_dir = gpepConfig.app_dir
mapproxy_app = multiapp.make_wsgi_app(apps_dir, allow_listing=True)

application = DispatcherMiddleware(NotFound(), {
    '/api':     	gpepapi_app,
    '/mapproxy': 	mapproxy_app,
})

application = WhiteNoise(application, root=settings.STATIC_ROOT)
application.add_files(settings.STATIC_ROOT, prefix='/api/static/')
