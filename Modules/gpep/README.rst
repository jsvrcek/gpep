Gpep is an addon module for mapproxy. For a description of mapproxy's core features, see Modules/nsg-mapproxy/README.rst

The documentation is available at: http://mapproxy.org/docs/1.9.0/
