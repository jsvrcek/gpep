"use strict";

MapApp.controller("JobDetailsModalCtrl", function ($scope, $uibModalInstance, JobFactory, job) {
    
    var ctrl = this;

    JobFactory.getJobDetails(job.id).then(function(response){
      ctrl.job = response;
    },function(err){
      ctrl.job = job;
    })

    ctrl.closeModal = function () {
        $uibModalInstance.dismiss('cancel');
    };

    ctrl.seedCleanPackageJob = function(){
       if (ctrl.job){
         return ['seed','clean','package'].indexOf(ctrl.job.job_type) > -1
       }
         return false;
    }

});
