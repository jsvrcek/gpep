describe('Progress Controller', function() {

     beforeEach(angular.mock.module('ui.bootstrap'));
     beforeEach(angular.mock.module('toastr'));
     beforeEach(angular.mock.module('MapApp'));
     beforeEach(angular.mock.module('ServerApp'));
     beforeEach(angular.mock.module('UtilitiesApp'));
     beforeEach(angular.mock.module('ProgressApp'));

    var controller, scope, JobFactory;

    beforeEach(inject(function($controller, $rootScope, _JobFactory_) {
        scope = $rootScope.$new();

        // The injector unwraps the underscores (_) from around the parameter names when matching
        JobFactory = _JobFactory_;

        controller = $controller('ProgressController', {
            $scope: scope,
            $rootScope: scope,
            "JobFactory": JobFactory
        });
    }));

    // it('test something in the progress controller', function() {
    //     fail("Progress Controller test not yet implemented")
    // });
});
