"use strict";

MapApp.controller("CancelJobModalCtrl", function ($scope, $rootScope, $uibModalInstance,UtilityFactory, toastr, JobFactory, job, ModalFactory) {

    var ctrl = this;
    ctrl.job = job;

    ctrl.cancelJob = function(){

        var busyToast = UtilityFactory.spinnerToastWithTitle('Attempting To Cancel', 'Job ' + job.id);

        JobFactory.cancelJob(job.id).then(function(response){
            toastr.clear(busyToast)
            toastr.success("Successfully Cancelled", job.job_type + " job " + job.id);

            // TODO better to encapsulate jobs updater into server, but more
            // overhead logic.  quick fix for now.

            // update jobs immediately after job cleared
            $rootScope.$broadcast("updateJobs");
        },function(err){
            toastr.clear(busyToast);
            ModalFactory.basicError("Cancelling Failed",err);
        });
    }

    // actually deletes the job
    ctrl.clearJob = function(){

        var busyToast = UtilityFactory.spinnerToastWithTitle('Attempting To Clear', 'Job ' + job.id);

        JobFactory.deleteJob(job.id).then(function(response){
            toastr.clear(busyToast)
            toastr.success("Successfully Cleared", job.job_type + " job " + job.id);

            // TODO better to encapsulate jobs updater into server, but more
            // overhead logic.  quick fix for now.

            // update jobs immediately after job cleared
            $rootScope.$broadcast("updateJobs");
        },function(err){
            toastr.clear(busyToast);
            ModalFactory.basicError("Clearing Failed",err);
        });
    }

    ctrl.closeModal = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
