
 angular.module("ProgressApp").controller("PackageNameModalCtrl", function($scope,
     $filter, $uibModalInstance, tasks, job, JobFactory, ModalFactory, $httpParamSerializer, DOWNLOAD_FILE_ENDPOINT, NSG_GEOPACKAGE_VERSION, moment, $q) {
    console.log(tasks, job)
  var $ctrl = this;
  $ctrl.tasks = tasks;
  $ctrl.job = job;

  $ctrl.formData = {
      geoPackageProducer: "AGC",
      geographicCoverageArea: "",
      dataProduct: $ctrl.tasks[0].name
  }

    $ctrl.cancel=function(){
        $uibModalInstance.dismiss('cancel');
    };

    $ctrl.updateFilename = function(){
        // Nettwarrior geopackages don't use NSG naming conventions
        if ($ctrl.job.package_type == 'NW'){
            var nameArray = [
                $filter('space-to-dash')($ctrl.formData.dataProduct),
                $filter('space-to-dash')($ctrl.formData.geographicCoverageArea),
                $ctrl.job.min_zoom + "-" + $ctrl.job.max_zoom,
                moment().format('DDMMMYY').toUpperCase()
            ];
        } else{
            var nameArray = [
                $filter('space-to-dash')($ctrl.formData.geoPackageProducer),
                $filter('space-to-dash')($ctrl.formData.dataProduct),
                $filter('space-to-dash')($ctrl.formData.geographicCoverageArea),
                $ctrl.job.min_zoom + "-" + $ctrl.job.max_zoom,
                NSG_GEOPACKAGE_VERSION,
                moment().format('DDMMMYY').toUpperCase()
            ];
        }

        nameArray = nameArray.filter(function(item){return item.length})
        $ctrl.filename = nameArray.join("_") + ".gpkg";
        return $ctrl.filename;
    };

    $ctrl.updateFilename();

    // attempts to download package file.
    // first verifies that all inputs are correct, and if they aren't
    // triggers errors to show
    $ctrl.attemptToDownload = function() {
        // touch all input fields, triggering errors to show
          angular.forEach($scope.packageNameForm.$error, function (field) {
            angular.forEach(field, function(errorField){
                errorField.$setTouched();
            })
        });

        // if all inputs valid, then download package
        if($scope.packageNameForm.$valid) {
            $ctrl.createDownloadParams();
        }
    }

    $ctrl.createDownloadParams = function(){
        $uibModalInstance.dismiss('cancel');

        var params = [];
        if ($ctrl.tasks.length > 1){
            for (var i =0; i < $ctrl.tasks.length; i++){
                $ctrl.formData.dataProduct = $ctrl.tasks[i].name;
                params.push({taskId: $ctrl.tasks[i].id, filename: $ctrl.updateFilename()})
            }
        } else{
            params.push({taskId: $ctrl.tasks[0].id, filename: $ctrl.updateFilename()})
        }

        $ctrl.checkAllAvailable(params)
    };

    $ctrl.checkAllAvailable = function(params){
        var i;
        var promises=[];
        for (i = 0 ; i < params.length; i++){
            promises.push(JobFactory.fileAvailableForDownload(params[i].taskId, params[i].filename))
        }
        $q.all(promises).then(function(){
            for(i = 0; i < params.length; i++){
                JobFactory.downloadFile(params[i].taskId, params[i].filename);
            }
        }, function(err){
            ModalFactory.basicError("Download Failed", err);
        })
    }
});
