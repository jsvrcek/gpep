 "use strict";

var progressApp = angular.module("ProgressApp", ["ui.bootstrap", "angularMoment"]);

progressApp.filter('space-to-dash',function() {
    return function(input) {
        if (input) {
            var tmp = input.replace(/\s+/g, '-'); // remove spaces
            return tmp.replace(/_/g, '-'); // remove underscores

        } else{
            return ""
        }
    }
});

progressApp.filter('time_remaining',function() {
    return function(input) {
        var eta = moment(input)
        var now = moment()
        return [eta.diff(now, 'hours'),eta.diff(now, 'minutes'),eta.diff(now, 'seconds')].join(":")
    }
});


// TODO remove endpoint url constants, not needed
progressApp.controller("ProgressController", function ($scope, $rootScope, $uibModal,
  $interval, JobFactory, ModalFactory, $httpParamSerializer, toastr) {

    /**
      Holds information about each job.  Each job is a collection of 1 or more
      tasks, all of the same type of operation (seeding, packaging, or cleaning).
      Each task corresponds with an operation on a single map service.

    - jobs
      - job 1
          - task a
          - task b
          - task c
      - job 2
          - task d
    */
    $scope.jobs = [];

    $scope.jobTypeFilters = {
        seed: true,
        package: true,
        clean: true,
        export: true
    }

    $scope.jobStatusFilters = {
        complete: true,
        inProgress: true,
        failed: true,
        canceled: true
    }

    $scope.search = "";
    $scope.numTasksToShow = 3;
    $scope.showAllTasks = {}

    $scope.areAllTypesSelected = function() {
        return $scope.areAllValuesTrue($scope.jobTypeFilters);
    }

    $scope.areAllValuesTrue = function(object) {
        for(var i in object) {
            if(object[i] == false) {
                return false;
            }
        }
        return true;
    }

    $scope.areAllStatusesSelected = function() {
        return $scope.areAllValuesTrue($scope.jobStatusFilters);
    }


    $scope.jobFilter = function (job) {

        // only show jobs which have matching types and statuses to the filter
        // menu on the left
        // NOTE: if all job types are selected, then it shows all job types no matter
        // what (even unknown job types, even jobs without types).  do the same
        // for job statuses.
        if (!$scope.jobStatusFilters[job.status] && !$scope.areAllStatusesSelected()){
            return false;
        }
        if (!$scope.jobTypeFilters[job.job_type] && !$scope.areAllTypesSelected()){
            return false;
        }
        if($scope.search){
            var inSearch = job.tasks.some(function(task){
                return task.name.indexOf($scope.search) >=0;
            })
            if (!inSearch) return false;
        }

        return true
    };

    $scope.clearSearch = function(){
        $scope.search = "";
    }

    $scope.toggleShowAllTasks = function(job){
        if (job.id in $scope.showAllTasks){
            $scope.showAllTasks[job.id] = !$scope.showAllTasks[job.id]
        }else{
            $scope.showAllTasks[job.id] = true
        }
    }

    $scope.$on("updateJobs", function() {
        $scope.updateJobs();
    });

    var jobsHaveBeenUpdatedAtleastOnce = false;
    $scope.updateJobs = function() {
        var oldJobs = [];
        var newJobs = [];

        JobFactory.getJobs().then( function(jobs) {
            oldJobs = angular.copy($scope.jobs)
            $scope.jobs = jobs;
            newJobs = angular.copy(jobs);

            $scope.showToastsForCompletedJobs(oldJobs, newJobs);
            jobsHaveBeenUpdatedAtleastOnce = true;
        }, function(err) {
            // on error
            // TODO only show an error toast if previous job fetch was successful,
            // but current one failed (or on first job fetch).
            console.error("Failed to fetch jobs statuses");
            console.error(err);
        })
    }
    $scope.updateJobs();

    // update jobs every 5 seconds
    $interval($scope.updateJobs, 5000);

    // return an array of id's belong to all jobs which have the given status
    function getMatchingJobIds(jobs, status) {
        // get jobs that match input status`
        var matchingJobs = jobs.filter(function(job) {
            return job.status == status;
        })

        // return array of ID's
        return matchingJobs.map(function(job) {
            return job.id;
        })
    }

    $scope.showToastsForCompletedJobs = function(oldJobs, newJobs) {
        if(newJobs.length == 0) {
            return;
        }

        function sortByID(a, b) {
            return a.id - b.id;
        }

        oldJobs.sort(sortByID);
        newJobs.sort(sortByID);

        // an integer array of jobs ids, representing all jobs
        var oldIds = oldJobs.map(function(job) {
            return job.id;
        })

        // an array of ids representing old jobs (pulled from the last update) which
        // were in progress
        var oldInProgressIds = getMatchingJobIds(oldJobs, "inProgress");

        // an array of ids representing new jobs (just now pulled) which are complete
        var newCompleteIds   = getMatchingJobIds(newJobs, "complete");

        /* array of job ids.  each job reprented by an id in this array:
              - was in the previous list of pulled jobs (old jobs)
              - is in the most recent list of pulled jobs (new jobs)
              - status was "inProgress", and is now "complete"
        */
        var fromInProgressToCompleteIds = _.intersection(newCompleteIds, oldInProgressIds);

        // these job ids represent jobs which were not present in the previous pull
        var completedIdsNotInPreviousPull = _.difference(newCompleteIds, oldIds);

        // jobs which have just finished
        var newlyCompletedIds = [];
        newlyCompletedIds = newlyCompletedIds.concat(fromInProgressToCompleteIds);

        // only show completed jobs not present in the previous pull if the browser wasn't
        // just refreshed.
        // this prevents a million completed toast notifications from showing when the
        // user refreshes the browser.
        if(jobsHaveBeenUpdatedAtleastOnce) {
            newlyCompletedIds = newlyCompletedIds.concat(completedIdsNotInPreviousPull);
        }

        // show completed toasts for all newly completed jobs
        _.each(newlyCompletedIds, showCompletedToastForJobId);
    }

    // show a completed toast for a specific jobs
    function showCompletedToastForJobId(id) {
         var job;
         for(var i=0; i < $scope.jobs.length; i++) {
            job = $scope.jobs[i];
            if(id == job.id) {
                break;
            }
         }

         // for example, transforms "clean" to Cleaning
         function prettifyJobType(type) {
            switch(type) {
                case "clean":
                    return "Cleaning";
                case "seed":
                    return "Seeding";
                case "package":
                    return "Packaging";
                case "export":
                    return "Exporting";
                default:
                    return type;
            }
         }

         // TOAST settings
         // configures title, subtitle, and onTap function callbacks for all
         // 4 types of toasts
         var title = prettifyJobType(job.job_type) + " Job " + id;
         var subtitle;
         var onTap = null;

         switch(job.job_type) {
            case "seed":
            case "clean":
                subtitle = "Completed Successfully";
                break;

            case "package":
                subtitle = "Click to Download";
                title += " Complete";
                onTap = function() {
                    $scope.$broadcast("downloadAllGeopackages", id)
                }
                break;

            case "export":
                subtitle = "Click to Download";
                title += " Complete"
                onTap = function() {
                    $scope.$broadcast("downloadPepFile", id)
                }
                break;
         }
         // download on click for package and export jobs
         toastr.success(subtitle, title, {
             onTap: onTap
         })
    }

    $scope.openDeleteModal = function(activeTab, id) {
        $scope.activeTab = activeTab;
        $scope.jobID = id;
        $scope.stepOne = true;
        $scope.modalInstance = $uibModal.open({
            templateUrl: "components/progress/views/modal.html",
            scope: $scope,
            // Uncomment these two lines to make it impossible to escape
            // the modal without clicking the buttons.
            //backdrop: "static",
            //keyboard: false
        });
    }


});


progressApp.controller("DeleteModalController", function($scope, $uibModal, $templateCache, JobFactory, DELETE_JOB_ENDPOINT) {
    $scope.message = function() {
        return "Are you sure you want to cancel this " + $scope.activeTab.toLowerCase()
               + " job?"
    }

    $scope.response = function(yesOrNo, jobID) {
        if (yesOrNo) {
            (JobFactory.deleteJob(jobID)).then(function (result) {
                var successful = result.data;
                if (successful) {
                     $scope.cancelled = "The job was cancelled.";
                 }
                 else {
                     $scope.cancelled = "Failed to cancel the job.";
                 }
            });
        }
        else {
            $scope.cancelled = "The job was not cancelled.";
        }
        $scope.stepOne = false;
    }

    $scope.done = function() { $scope.modalInstance.close() }
});
