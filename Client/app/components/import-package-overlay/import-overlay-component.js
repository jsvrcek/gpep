/**
  The importOverlay component makes available the <import-overlay></import-overlay>
  html tag, and connects it with the ImportOverlayService.

  Use the import-overlay tags in your html to enable the dragging and dropping
  of files.
*/

angular.module('MapApp').component('importOverlay', {
    templateUrl: 'components/import-package-overlay/import-overlay.html',
    controller: ImportOverlayController,
    bindings: {
        // snapBbox: '<',
        // onZoomLevelsChange: '&', // zoomLevels -> {min: xxx, max: xxx}a
    }
});

function ImportOverlayController($scope, ImportOverlayService) {
    var ctrl = this;

    // gives html template access to overlay service,
    // so overlay html can check whether to hide or show itself
    ctrl.service = ImportOverlayService;
    ImportOverlayService.init();
}
