var MapApp = angular.module('MapApp');

/**
   ImportOverlayService keeps track of whether or not a user has drag and dropped
   a file onto the screen.  It also holds the data for the dropped file in
   ImportOverlayService.fileForUpload

   events broadcasted:
   "file-dropped-onto-import-overlay" event, with the dropped file as data
   payload, is broadcasted to $rootScope when a file is dropped

   IMPORTANT note:
   any drag operation on the screen will
   trigger the overlay (such is the nature of html limitations).
   if you have other drag and drop functionality on your UI, you
   need to disable the overlay before that other dragging occurs, then
   reenable it afterwords, using disableOverlay(), enableOverlay(), and
   possibly removeOverlay() to forcibly remove the overlay if it was
   previously triggered.
*/

MapApp.factory('ImportOverlayService', function ($rootScope, $timeout) {
    var fileForUpload = null; // file user has dragged onto overlay
    var overlayEnabled = true;
    var draggingActive = false;

    var dragElement;

    var o = {
        disableOverlay: disableOverlay, // disable overlay from triggering
        enableOverlay: enableOverlay,   // allow entire screen overlay to trigger
        removeOverlay: removeOverlay,   // for remove the overlay
        isOverlayEnabled: isOverlayEnabled, // check if overlay is enabled
        isDraggingActive: isDraggingActive, // is file being dragged on browser
        fileForUpload: fileForUpload,  // file for uploading
        init: init

    };
    return o; // exposes factory functions

    /**
      Obtains the DOM element for the drag and drop area, and sets up drag and
      drop event listeners for every single element in the browser, which our
      waiting to trigger the import overlay if an item is dragged onto them.
    */
    function init() {
        // TODO refactor, should not need to use elementById with components
        // (remnant for old code, refactored into component)
        dragElement = document.getElementById('drag-and-drop-area');

        if(dragElement == null) {
            console.warn("exiting import-overlay-service.js, no browser DOM found.  drag and dropping disabled.")
            console.warn("This could either mean you \
            are running tests, so the DOM doesn't exist; or it means you haven't added <import-overlay></import-overlay> \
            to your HTML.)");
            return;
        }

        // setups up bindings to listen for dragging and dropping
        dragElement.addEventListener('dragend', removeOverlay);
        dragElement.addEventListener('dragleave', removeOverlay);
        dragElement.addEventListener('drop', function() {
            removeOverlay();
            $rootScope.$broadcast("file-dropped-onto-import-overlay", fileForUpload);
        });

        document.addEventListener('dragenter', function(e) {
            draggingActive = true;
            angular.element(dragElement).addClass('dragging-active');
        });
    }

    function disableOverlay() {
        overlayEnabled = false;
    }

    function enableOverlay() {
        overlayEnabled = true;
    }

    function removeOverlay() {
        angular.element(dragElement).removeClass('dragging-active');
        draggingActive = false;
    }

    function isOverlayEnabled() {
        return overlayEnabled;
    }

    function isDraggingActive() {
        return draggingActive;
    }
});
