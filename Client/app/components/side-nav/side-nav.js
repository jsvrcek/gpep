angular.module('MapApp').component('sideNav', {
  templateUrl: 'components/side-nav/side-nav.html',
  controller: SideNavController,
  bindings: {
    onSelectLayer: '&', // accepts: layer, service (service is optional if the layer has a .url property)
    onDeselectLayer: '&',
    onToggleVisibility: '&',
    onReorderLayers: '&',
    // will trigger on base layer changes, as WELL as periodically
    // so just because it's called doesnt mean a base layer has been added
    // or removed
    // NOTE: each layer needs to have a url properly
    onBaseLayerUpdate: '&', // accepts: [list of current base layers from sidenav]
  }

});

function SideNavController($scope, $rootScope, $element, $attrs, $interval, ServiceStore,
$uibModal, UtilityFactory, toastr, ModalFactory, ImportOverlayService,
ServiceFactory, $timeout) {
    var ctrl = this;

    ctrl.selectedLayers = [];
    ctrl.services = ServiceStore.services;
    var servicesInitiallyPopulated = false;

    /**
      Wrapper for populate services, among other things which should be
      regularly updated
    */
    ctrl.pollBackend = function(baselayerLocallyChanged) {
        var previousBaseLayer = getBaseLayer()
        ServiceStore.populateServices().then(function() {
            // TODO base layers to backend
            ctrl.reorderLayers();
        }).then(function() {
            // so leaflet can keep track of which layers to add into the baselayer widget
            ctrl.onBaseLayerUpdate({baseLayer: getBaseLayer()});
        }).then(function() {
            // detect if the baselayer changed
            var newBaseLayer = getBaseLayer()

            if(previousBaseLayer != null && newBaseLayer != null
               && previousBaseLayer.id != newBaseLayer.id) {
                baselayer_just_changed = true;
            }

            // select the base layer, initially on page refresh (or if there's
            // a new one)
            if(!servicesInitiallyPopulated || baselayer_just_changed) {
                // only select newly user defined base layer if they don't have
                // another random layer selected in sidenav. but always select
                // user-defined default layer on browser open if base layer was
                // changed locally, always set the base layer in leaflet to
                // this new base layer
                if(ctrl.isLayerSelected(previousBaseLayer)
                  || servicesInitiallyPopulated == false
                  || baselayerLocallyChanged) {
                    ctrl.selectLayer(newBaseLayer);
                    if(previousBaseLayer != undefined
                      && previousBaseLayer.id != newBaseLayer.id) {
                        ctrl.deselectLayer(previousBaseLayer)
                    }
                }
            }
            servicesInitiallyPopulated = true;
        })
    }

    // select the base layer
    ctrl.selectBaseLayer = function() {
        base_layer = getBaseLayer();
        if(base_layer == undefined) {
            return;
        }
        ctrl.selectLayer(base_layer)
    }

    /* object with key values, where the key is a service id, and its
       value is true if the service is open, and false or undefined if
       the service is closed (in the service list)

       Example:
       {
          2: true;
          5: false;
       }
    */
    ctrl.openServicesKeys = {};

    ctrl.pollBackend();
    // auto add services every 5 seconds, also check if base layers changed
    $interval(function() { ctrl.pollBackend(false) }, 5000);

    // exposes the ability to enable/disable the pep/geopackage importing overlay
    // important, because the overlay is triggered by anything that drags.
    // therefore, the overlay needs to be disabled and then reenabled by the layer
    // reoderer in the sidenav while a user is using it to drag things
    ctrl.ImportOverlayService = ImportOverlayService;

    // returns true if service with provided id is opened in the side nav,
    // false otherwise
    ctrl.isServiceOpen = function(id) {
        return ctrl.openServicesKeys[id] == true;
    }

    // makes a service as open if its currently collapsed, or collapsed if it's
    // currently open
    ctrl.toggleServiceOpen = function(id) {
        ctrl.openServicesKeys[id] = !ctrl.openServicesKeys[id];
    }

    // returns true if layer with given id is selected, false otherwise
    ctrl.isLayerSelected = function(layer) {
        if(layer == undefined) {return false;}
        return ServiceStore.selectedLayersKeys[layer.id] == true;
    }

    ctrl.toggleLayer  = function(layer) {
        ServiceStore.selectedLayersKeys[layer.id] = !ServiceStore.selectedLayersKeys[layer.id];
        //ctrl.layerCheckboxes[layer.id] = !ctrl.layerCheckboxes[layer.id];
        if(ctrl.isLayerSelected(layer)) {
            ctrl.selectLayer(layer);
        } else {
            ctrl.deselectLayer(layer);
        }
    }

    var getService = function(layer){
        for(var i = 0; i < ctrl.services.length; i++){
            for (var j = 0; j< ctrl.services[i].layers.length; j++){
                if (ctrl.services[i].layers[j].id == layer.id){
                    return ctrl.services[i];
                }
            }
        }
        return null
    };

    // returns true if "layer" is a base layer
    ctrl.isBaseLayer = function(layer) {
        return layer.is_base_layer;
    }

    // tells backend to make a layer a base layer
    baselayer_just_changed = false;
    ctrl.setBaseLayer = function(layer) {
        ServiceFactory.setBaseLayer(layer.id, true).then(function(resp) {
            var previousBaseLayer = getBaseLayer()
            var newBaseLayer = layer
            // then, unselect the target layer, then readd it as a baselayer to the
            // leaflet map
            if(ctrl.isLayerSelected(newBaseLayer)) {
                ctrl.deselectLayer(newBaseLayer);
            }
            newBaseLayer.is_base_layer = true;
            baselayer_just_changed = true;
            ctrl.pollBackend(true);

        }, function(err) {
            console.error("Failed to make layer a base layer: " + JSON.stringify(newBaseLayer, null, '\t'));
            console.error(err);
        })
    }

    // makes backedn to make a layer not be a base layer
    ctrl.unsetBaseLayer = function(layer) {
        ServiceFactory.setBaseLayer(layer.id, false).then(function(resp) {
            if(ctrl.isLayerSelected(layer)) {
                ctrl.deselectLayer(layer);
            }
            layer.is_base_layer = false;
            ctrl.pollBackend();

        }, function(err) {
            console.error("Failed to make layer not be a base layer: " + JSON.stringify(layer, null, '\t'));
            console.error(err);
        })
    }

    /**
      Returns the current base layer if there is one.
      Otherwise, returns undefined.
    */
    function getBaseLayer() {
        for(var i = 0; i < ctrl.services.length; i++) {
            var service = ctrl.services[i]
            for(var k = 0; k < service.layers.length; k++) {
                var layer = service.layers[k]
                if(layer.is_base_layer) {
                    // - add the url properly to the layer so it can be passed
                    //   to the map component (leaflet) and leaflet has a url
                    //   to ping tiles for that layer
                    // - angular.copy because we don't want
                    //   the layer to be overriden when pulling new layers in from
                    //   the backend while leaflet processes it
                    layer.url = service.url
                    return angular.copy(layer)
                }
            }
        }
        return undefined;
    }

    // retreives a layer, given its id
    ctrl.getLayerById = function(id) {
        for(var i = 0; i < ctrl.services.length; i++) {
            var service = ctrl.services[i];

            var foundLayer = service.layers.find(function(layer) {
                return layer.id == id;
            })

            if(foundLayer != undefined) {
                return foundLayer;
            }
        }
    }

    // call when you want to programmatically select a certain layer
    ctrl.selectLayer = function(layer) {
        if(layer == undefined) {
            return;
        }
        // if layer is a baselayer, unselect all other base layers first before
        // selecting it
        if(layer.is_base_layer) {
            ctrl.services.forEach(function(service) {
                service.layers.forEach(function(layer) {
                    if(ctrl.isLayerSelected(layer) && layer.is_base_layer) {
                        ctrl.deselectLayer(layer);
                    }
                })
            })
        // if not a base layer, add layer to regular list of selected layers
        // (base layers are not stored in that list)
        } else {
            ctrl.selectedLayers.unshift(layer)
        }

        // whether or not the layer is a base layer, change its selected key
        // to true (so it shows as selected in the side nav),
        // and...
        ServiceStore.selectedLayersKeys[layer.id] = true;

        // trigger the onSelectLayer callback (so whomever is hooked in to the
        // side nav can act up a new layer being added.  in this case, it lets
        // leaflet add that layer to the leaflet map, so the layer shows in the
        // map view).
        var service = getService(layer)
        ctrl.onSelectLayer({layer:layer, service: service});
        ctrl.reorderLayers();
    }

    // when a new base layer is selected, make sure that the previously
    // selected base layer is unselected in the sidnav, and ensure that the new
    // base layer is selected in the side nav
    $scope.$on("baseLayerChanged", function(event, data) {

        // wrapping the selection and unselection of layers in $timeout forces
        // the sidenav DOM objects to update their state as soon as possible.
        // otherwise, angular does not know to check to update the hiding or
        // showing of checkmarks on the side nav
        //
        // in angular terms, this forces angular call $scope.apply() as soon
        // as possible, after the wrapped code, and after any currently executing
        // $scope.apply() cycles.
        $timeout(function() {
            // if the previous base layer exists in the side nav
            // (has a not undefined id) then unselect it
            if(data.oldBaseLayerId) {
                ServiceStore.selectedLayersKeys[data.oldBaseLayerId] = false;
            }

            // if the new base layer exists in the side nav
            // (has a not undefined id) then select it
            if(data.newBaseLayerId) {
                ServiceStore.selectedLayersKeys[data.newBaseLayerId] = true;
            }
        }, 0);

    })

    /**
      Removes a layer from the selected layers block at the top of the sidenav.
    */
    function removeLayerFromSelectedLayers(layer) {
        // finds the index in ctrl.selectedLayers where "layer" resides
        var index = _.findIndex(ctrl.selectedLayers, function(selectedLayer) {
            return selectedLayer.id == layer.id;
        });
        ctrl.selectedLayers.splice(index,1);
    }

    // call when you want to programmatically select a certain layer
    ctrl.deselectLayer = function(layer){
        ServiceStore.selectedLayersKeys[layer.id] = false;

        if(!layer.is_base_layer) {
            removeLayerFromSelectedLayers(layer);
        }

        ctrl.onDeselectLayer({layer:layer});
    };

    ctrl.openAddServiceModal = function(){
        var modalInstance = $uibModal.open({
          templateUrl: 'components/map/views/add_service_modal.html',
          controller: 'AddServiceModalController',
        });
    };

    ctrl.exportService = function(mapProxyApp) {
        var modalInstance = $uibModal.open({
          templateUrl: 'components/map/views/export_modal.html',
          controller: 'ExportModalController',
          resolve: {
                mapProxyApp: function() {
                    return mapProxyApp;
                }
            }
        });
    }

    $scope.$on('file-dropped-onto-import-overlay', function(event, data) {
        ctrl.openAddServiceFromFileModal();
    });

    ctrl.openAddServiceFromFileModal = function(){
        ImportOverlayService.disableOverlay();

        var modalInstance = $uibModal.open({
          templateUrl: 'components/map/views/add_service_from_file_modal.html',
          controller: 'AddServiceFromFileModalController',
        });

        modalInstance.closed.then(function() {
            ImportOverlayService.enableOverlay();
            ImportOverlayService.removeOverlay();
        })
    };

    ctrl.openDeleteServiceModal = function(service) {
        var modalInstance = $uibModal.open({
            templateUrl: 'components/map/views/delete_service_modal.html',
            controller: 'DeleteServiceModalController',
            resolve: {
                service: function() {
                    return service
                },
                sideNavController: function() {
                    return ctrl
                },

            }
        });
    };

    ctrl.openServiceInfoModal = function(service) {
        var modalInstance = $uibModal.open({
            templateUrl: 'components/map/views/service_info_modal.html',
            controller: 'ServiceInfoModalController',
            resolve: {
                serviceName: function() {
                    return service.name
                },
                serviceID: function() {
                    return service.id;
                },
            }
        });
    };

    ctrl.toggleVisibility = function (layer) {
        ctrl.onToggleVisibility({layer:layer});
    };

    ctrl.reorderLayers = function(){
        var layerIds = ctrl.selectedLayers.map(function(layer){return layer.id});
        ctrl.onReorderLayers({orderedLayerIds: layerIds});
    };

    var addServiceButtonElement = document.querySelector(".navbar-add-service-btn");
    ctrl.isSpeedDialOpened = false;
};
