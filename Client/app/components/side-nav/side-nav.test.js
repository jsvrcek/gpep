/*
testable functions:
    - zoomLevelToScale
    - setting the zoom levels from outside (min/max flipping) (may require some refactoring)
    - today

*/

describe('Side Nav Component: ', function() {
    var controller, scope, $uibModal, element, $q, ServiceStore, UtilityFactory, toastr, ModalFactory;

    var serviceWithOneLayer;
    var serviceWithMultipleLayers;

    beforeEach(angular.mock.module('ui.bootstrap'));
    beforeEach(angular.mock.module('toastr'));
    beforeEach(angular.mock.module('MapApp'));
    beforeEach(angular.mock.module('ServerApp'));
    beforeEach(angular.mock.module('UtilitiesApp'));

    beforeEach(inject(function ($componentController, $rootScope, _$q_, _ServiceStore_, _UtilityFactory_, _ModalFactory_) {
        scope = $rootScope.$new();
        $uibModal = {
            open: function() {}
        };
        toastr = {
            success: function() {}
        };
        $q = _$q_;

        ServiceStore = _ServiceStore_;
        UtilityFactory = _UtilityFactory_;
        ModalFactory = _ModalFactory_;

        // placeholder implementations for the functions available
        // via binding.
        // placeholder implementations for the functions available
        var bindings = {
            onSelectLayer: function(layer, service) {},
            onDeselectLayer: function(layer) {},
            onToggleVisibility: function(layer) {},
            onReorderLayers: function(orderedLayerIds) {}
        }

        ctrl = $componentController('sideNav', {
            // scope
            $scope : scope,

            // these two DOM-related services are necessary for controller to instantiate
            $element: "",
            $attrs: {},
            "$uibModal": $uibModal,
            "$q": $q,
            "toastr": toastr,

            "ServiceStore": ServiceStore,
            "UtilityFactory": UtilityFactory,
            "Modalactory": ModalFactory
        }, bindings);
    }));

    beforeEach(function() {
        serviceWithOneLayer = {
            "id": 3,
            "name": "north_carolina",
            "url": "http://localhost:8080/gpep/mapproxy/north_carolina/service",
            "layers": [{
                "id": 8,
                "name": "Orthoimagery_Latest",
                "title": "Orthoimagery_Latest",
                "bbox": [-180, -90, 180, 90],
            }],
        }

        serviceWithMultipleLayers = {"id":4,"name":"digital_globe","url":"http://localhost:8080/gpep/mapproxy/digital_globe/service","layers":[{"id":9,"name":null,"title":"DigitalGlobe Web Map Tile Service","bbox":[-180,-90,180,90],"selected":false},{"id":10,"name":"DigitalGlobe_FoundationGEOINT","title":"DigitalGlobe:FoundationGEOINT","bbox":[-180,-90,180,90]},{"id":11,"name":"DigitalGlobe_ImageInMosaicFootprint","title":"DigitalGlobe:ImageInMosaicFootprint","bbox":[-180,-90,180,90]},{"id":12,"name":"DigitalGlobe_ImageStrip","title":"DigitalGlobe:ImageStrip","bbox":[-180,-90,180,90]},{"id":13,"name":"DigitalGlobe_ImageryTileService","title":"DigitalGlobe:ImageryTileService","bbox":[-180,-90,180,90]},{"id":14,"name":"DigitalGlobe_NGAOtherProducts","title":"DigitalGlobe:NGAOtherProducts","bbox":[-180,-90,180,90]}]}
    })

        describe("checkSelected", function() {
            it("should call checkSelected when a layer is selected", function() {
                var layer = serviceWithOneLayer.layers[0];
                layer.selected = true;

                spyOn(ctrl, "checkSelected");
                ctrl.checkSelected(layer, serviceWithOneLayer);
                expect(ctrl.checkSelected).toHaveBeenCalledWith(layer, serviceWithOneLayer);
            })

            it("should call deSelectLayer when a layer is unselected", function() {
                var layer = serviceWithOneLayer.layers[0];

                spyOn(ctrl, "deselectLayer");
                ctrl.checkSelected(layer, serviceWithOneLayer);
                expect(ctrl.deselectLayer).toHaveBeenCalledWith(layer);
            })
        })

        describe("Layer Selection and Deselection", function() {
            var layer1, layer2;

            beforeEach(function() {
                spyOn(ctrl, "onSelectLayer");
                spyOn(ctrl, "onDeselectLayer");

                layer1 = serviceWithOneLayer.layers[0];
                layer1.selected = true;

                layer2 = serviceWithMultipleLayers.layers[1];
                layer2.selected = true;
            })

            it("selectLayer should work properly", function() {
                ctrl.checkSelected(layer1);
                expect(ctrl.selectedLayers).toEqual([layer1]);
                expect(ctrl.onSelectLayer).toHaveBeenCalled();

                ctrl.checkSelected(layer2);
                expect(ctrl.selectedLayers).toEqual([layer2, layer1]);
                expect(ctrl.onSelectLayer).toHaveBeenCalled();
            })

            it("deselectLayer should work properly", function() {
                ctrl.selectedLayers = [layer1, layer2];
                ctrl.deselectLayer(layer2);
                expect(ctrl.selectedLayers).toEqual([layer1]);
                expect(ctrl.onDeselectLayer).toHaveBeenCalled();

                ctrl.deselectLayer(layer1);
                expect(ctrl.selectedLayers).toEqual([]);
                expect(ctrl.onDeselectLayer).toHaveBeenCalled();
            })
        });


        it("should open the 'add service' modal", function() {
            spyOn($uibModal, "open");
            ctrl.openAddServiceModal();
            expect($uibModal.open).toHaveBeenCalled();
        })

        it("should open the Service Info Modal", function() {
            spyOn($uibModal, "open");
            ctrl.openServiceInfoModal();
            expect($uibModal.open).toHaveBeenCalled();
        })

        it("ensure toggle visibility works", function() {
            spyOn(ctrl, "onToggleVisibility");
            var layer = serviceWithOneLayer.layers[0];
            ctrl.toggleVisibility(layer);
            expect(ctrl.onToggleVisibility).toHaveBeenCalledWith({layer:layer});
        })

        it("make sure reorderLayers works properly", function() {
            spyOn(ctrl, "onReorderLayers")

            var layer1 = serviceWithOneLayer.layers[0];
            var layer2 = serviceWithMultipleLayers.layers[1];
            ctrl.selectedLayers = [layer2, layer1];
            ctrl.reorderLayers();
            expect(ctrl.onReorderLayers).toHaveBeenCalledWith({
                orderedLayerIds: [layer2.id, layer1.id]
            })
        })

        // FIXME fix deleteService test
        // for some reason service_list GET request from the ether is mucking
        // up this test.
        // it("ensure deleting a service works properly", function() {
        //   // var deferred = $q.defer();
        //   // var promise = deferred.promise;
        //   //
        //   // spyOn(toastr, "success");
        //   //
        //   // spyOn(ServiceStore, "deleteService").and.returnValue(promise);
        //
        //   spyOn(ServiceStore, "deleteService").and.callFake(function(service) {
        //       var deferred = $q.defer();
        //       deferred.resolve('deleteService resolving status');
        //       return deferred.promise;
        //   });
        //
        //   ctrl.deleteService(serviceWithOneLayer);
        //   // deferred.resolve("resolved successfully!")
        //   scope.$digest();
        //
        //   expect(toastr.success).toHaveBeenCalled();
        // })
});
