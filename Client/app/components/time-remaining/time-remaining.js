angular.module('ProgressApp').component('timeRemaining', {

/**
 * 
 * This component returns a countdown string formatted like this: 0:00:00
 * It updates every second or whenever the eta object gets updated.
 * eta can either be a string or a date object. It just has to be understandable
 * by the moment library to create a moment object.
 * 
 * '<' is a one way binging. so changes in component eta wont change
 * the parent eta. This also allows the controller to detect changes using $onChanges
 * 
 */


  templateUrl: 'components/time-remaining/time-remaining.html',
  controller: TimeRemainingController,
  bindings: {
    eta: '<',
  },
});

function TimeRemainingController($interval){
    var ctrl = this;
    var eta_m = moment(ctrl.eta);
    var now_m;

    ctrl.timeRemainingString = "";


    ctrl.$onChanges = function (changes) {
        ctrl.cancelUpdater();
        if (changes.eta) {
            eta_m = moment(ctrl.eta)
            ctrl.updater = $interval(ctrl.updateTimeRemaining, 1000)
        }
    };

    ctrl.updateTimeRemaining = function(){
        if(!ctrl.eta){
            ctrl.cancelUpdater();
            return;
        }

        now_m = moment();
        
        if (eta_m.diff(now_m, 'seconds') < 0){
            ctrl.timeRemainingString = "0:00:00";
            return;
        }

        ctrl.timeRemainingString =  [
            eta_m.diff(now_m, 'hours'),
            ("00" + eta_m.diff(now_m, 'minutes')%60).slice(-2),
            ("00" + eta_m.diff(now_m, 'seconds')%60).slice(-2)
        ].join(":") 
    }

    ctrl.updater = $interval(ctrl.updateTimeRemaining, 1000)
    
    ctrl.cancelUpdater = function(){
        $interval.cancel(ctrl.updater)
        ctrl.updater = undefined;
    }

    ctrl.updateMoment = function(){
        ctrl.cancelUpdater();
        if(ctrl.eta.length){
            eta_m = moment(ctrl.eta);
            ctrl.updateTimeRemaining();
        }
    };

    ctrl.$onDestroy = function () {
        ctrl.cancelUpdater();
    };
}
