/*
testable functions:
    - zoomLevelToScale
    - setting the zoom levels from outside (min/max flipping) (may require some refactoring)
*/

describe('Settings Panel Component: ', function() {
    var controller, scope, $timeout, toastr, ServiceStore, ServiceFactory, uibModal, toastr;

    beforeEach(angular.mock.module('ui.bootstrap'));
    beforeEach(angular.mock.module('toastr'));
    beforeEach(angular.mock.module('MapApp'));
    beforeEach(angular.mock.module('ServerApp'));
    beforeEach(angular.mock.module('UtilitiesApp'));

    beforeEach(inject(function ($componentController, $rootScope) {
        scope = $rootScope.$new();

        ServiceStore = {
            areAnyLayersSelected: function() {
                return true;
            }
        };

        // placeholder implementations for the functions available
        // via binding.
        var bindings = {
            onZoomLevelsChange: function(zoomLevels) {
            },

            onBboxChange: function(bbox) {
            }
        }

        ctrl = $componentController('settingsPanel', {
            $scope : scope,
            "ServiceStore": ServiceStore,
        }, bindings);
    }));

    describe("zoomLevelToScale(level)", function() {
        it("should convert zoom level 0 to proper corresponding world scale", function() {
            expect(ctrl.zoomLevelToScale(0)).toEqual("~1:524288000");
        })

        it("should convert zoom level 18 to proper corresponding world scale", function() {
            expect(ctrl.zoomLevelToScale(18)).toEqual("~1:2000");
        })

        it("should return undefined when given zoom level -1", function() {
            expect(ctrl.zoomLevelToScale(-1)).not.toBeDefined();
        })

        it("should return undefined when given zoom level greater than 18", function() {
            expect(ctrl.zoomLevelToScale(19)).not.toBeDefined();
        })
    });

    it("should update zoom values in sharedSettings properly", function() {
        ctrl.slider.minValue = 2;
        ctrl.slider.maxValue = 8;

        ctrl.updateSharedSettings();

        expect(ctrl.sharedSettings.startZoomLevel).toEqual(ctrl.slider.minValue);
        expect(ctrl.sharedSettings.endZoomLevel).toEqual(ctrl.slider.maxValue);
    });

    it("should update new snapped bounds", function() {

        var north = 1;
        var south = 2;
        var east  = 3;
        var west  = 4;

        scope.$broadcast("newSnappedBounds", {
            "north": north,
            "south": south,
            "east": east,
            "west": west
        });

        expect(ctrl.sharedSettings.snappedNorth).toEqual(north);
        expect(ctrl.sharedSettings.snappedSouth).toEqual(south);
        expect(ctrl.sharedSettings.snappedEast).toEqual(east);
        expect(ctrl.sharedSettings.snappedWest).toEqual(west);
    });

    it("should set a new max zoom level properly", function() {
      ctrl.slider.minValue = 2;
      ctrl.slider.maxValue = 7;

      var targetMax = 5;

      scope.$broadcast("newMaxZoomLevel", targetMax);

      expect(ctrl.slider.maxValue).toEqual(targetMax);
      expect(ctrl.slider.minValue).toEqual(2);

      // now we set the max to be less than the current min zoom
      // this should then turn the current min zoom level into the max zoom
      // level, and the current min to be the target max

      targetMax = 1;
      var tmp = ctrl.slider.minValue;
      scope.$broadcast("newMaxZoomLevel", targetMax);
      expect(ctrl.slider.minValue).toEqual(targetMax);
      expect(ctrl.slider.maxValue).toEqual(tmp);
    });

    it("should set a new min zoom level properly", function() {
      ctrl.slider.minValue = 2;
      ctrl.slider.maxValue = 7;

      var targetMin = 5;

      scope.$broadcast("newMinZoomLevel", targetMin);

      expect(ctrl.slider.minValue).toEqual(targetMin);
      expect(ctrl.slider.maxValue).toEqual(7);

      // if min is set to be greater than the current max zoom, then the max
      // and min slider knobs swap
      targetMin = 8;
      var tmp = ctrl.slider.maxValue;
      scope.$broadcast("newMinZoomLevel", targetMin);
      expect(ctrl.slider.maxValue).toEqual(targetMin);
      expect(ctrl.slider.minValue).toEqual(tmp);
    });

    it("ensure selectEntireWorld() sets the bbox bounds to encompass the globe", function() {
        ctrl.selectEntireWorld();

        expect(ctrl.sharedSettings.bboxN).toEqual(90);
        expect(ctrl.sharedSettings.bboxE).toEqual(180);
        expect(ctrl.sharedSettings.bboxS).toEqual(-90);
        expect(ctrl.sharedSettings.bboxW).toEqual(-180);
    });

    // it("expect newSelectionBoundsDrawn event to update shared settings", function() {
    //     var north = 35;
    //     var south = -23;
    //     var east  = 6;
    //     var west  = 42;

    //     scope.$broadcast("newSelectionBoundsDrawn", {
    //         "north": north,
    //         "south": south,
    //         "east": east,
    //         "west": west
    //     });

    //     expect(ctrl.sharedSettings.bboxN).toEqual(north);
    //     expect(ctrl.sharedSettings.bboxS).toEqual(south);
    //     expect(ctrl.sharedSettings.bboxE).toEqual(east);
    //     expect(ctrl.sharedSettings.bboxW).toEqual(west);
    // });

    it("ensure updateDrawnBounds() triggers onBboxChange() binding", function() {

      spyOn(ctrl, "onBboxChange");

      var north = 72;
      var south = 0;
      var east  = -7;
      var west  = 4;

      ctrl.sharedSettings.bboxN = north;
      ctrl.sharedSettings.bboxE = east;
      ctrl.sharedSettings.bboxS = south;
      ctrl.sharedSettings.bboxW = west;

      ctrl.updateDrawnBounds();

      expect(ctrl.onBboxChange).toHaveBeenCalledWith({
        bounds: {
              "north": north,
              "south": south,
              "east": east,
              "west": west
          }
      });
    });

    it("", function() {

    })

    it("", function() {

    })

    it("", function() {

    })

    it("", function() {

    })

    it("", function() {

    })

});
