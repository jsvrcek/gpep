"use strict";

/**
 * leaflet_controller.js manages the leaflet map view.  It does things like
 * initialize the map view, add/remove tile layers, and provide drawing tools
 * for drawing bounding boxes.
 */

var MapApp = angular.module('MapApp');

MapApp.controller('MapController', function ($scope, $rootScope, ServiceStore, $timeout, $interval, $http, ServiceFactory, JobFactory) {
    var DRAWN_BBOX_COLOR = "#ff0000";
    var SNAPPED_BBOX_COLOR = "green";

    // map parameters
    var map = L.map('mapid', {
        center: [50.634762, 10.406671],
        zoom: 6,
        zoomControl: false,
        crs: L.CRS.EPSG3857,
        attributionControl: false
    });

    // Set Max button
    var maxButton = L.easyButton({
        id: 'set-max-btn',  // an id for the generated button
        type: 'replace',          // set to animate when you're comfy with css
        leafletClasses: true,     // use leaflet classes to style the button?
        states:[{                 // specify different icons and responses for your button
            //stateName: 'get-center',
            onClick: function(button, map) {
                $scope.$broadcast("newMaxZoomLevel", map.getZoom())
            },
            title: 'set Maximum Zoom Level to current map view',
            icon: 'Set <b>Max</b></span>'
        }]
    })

    // Set Min button
    var minButton = L.easyButton({
        id: 'set-min-btn',  // an id for the generated button
        type: 'replace',          // set to animate when you're comfy with css
        leafletClasses: true,     // use leaflet classes to style the button?
        states:[{                 // specify different icons and responses for your button
            //stateName: 'get-center',
            onClick: function(button, map){
                $scope.$broadcast("newMinZoomLevel", map.getZoom());
                $scope.acceptNewZoomLevels({min: map.getZoom(), max:$scope.maxZoom});
            },
            title: 'set Minimum Zoom Level to current map view',
            icon: 'Set <b>Min</b>'
        }]
    })

    // add "Set Max" and "Set Min" buttons to the leaflet map
    L.easyBar([minButton, maxButton], {position: 'bottomright'}).addTo(map);

    // zoom control (+ - buttons)
    new L.control.zoom({
        position: 'bottomright'
    }).addTo(map);

    // displays current zoom level
    map.addControl(new zoomDisplayControl());


    /**************************** BASE LAYERS CONTROL *************************************/

    // instantiate hard-coded base layers for leaflet
    var OpenStreetMap_BlackAndWhite = L.tileLayer('https://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        noWrap: true
    });

    var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        noWrap: true
    });

    // initialize and add base map control to the leaflet map, and
    // add the two hard-coded maps above to the base map control...
    var baseMapsControl = L.control.layers();
    baseMapsControl.addTo(map);
    baseMapsControl.addBaseLayer(OpenStreetMap_BlackAndWhite, "Black and White");
    baseMapsControl.addBaseLayer(OpenStreetMap_Mapnik,        "Color");

    // leaflet defaults to open street map if it can't connect to backend
    map.addLayer(OpenStreetMap_Mapnik);
    var currentBaseLayer = OpenStreetMap_Mapnik;

    // but if it can connect to the backend, then it will pull in the default base layer
    // as defined in pep.cgf.  so...

    // attempt to retrieve default basemap url from the backend, and add
    // that to the basemap control
    var baseLayerFromBackendSuccessfullyRetreived = false;
    function getDefaultBaselayer() {
        ServiceFactory.getDefaultBaselayer().then(function(response) {
            var baseLayerFromBackendSuccessfullyRetreived = true;
            var baselayer = response.data;
            var default_leaflet_baselayer = L.tileLayer(baselayer.url, {
                attribution: 'Tiles &copy; Esri &mdash; Source: US National Park Service',
                noWrap: true,
                maxZoom: baselayer.max_zoom
            });
            baseMapsControl.addBaseLayer(default_leaflet_baselayer, baselayer.title);
            map.addLayer(default_leaflet_baselayer);

        }, function(err) {
            console.error("Failed to retrieve default baselayer.");
            console.error(err);
        })
    }

    getDefaultBaselayer();

    /**
      Key -> value storing of:  layer id -> leaflet layer
      Used to keep track of base layers created and added from the sidnav
    */
    var leafletBaseLayers = {};

    // when a new base layer is set, remove the previous base layer from
    // the leaflet map, because only one base layer can be shown at a time,
    // per our definition of what a base layer is
    map.on("baselayerchange", function(e) {
        tellSidenavAboutBaselayerChange(currentBaseLayer.options.id, e.layer.options.id);

        // ensure the previous base layer is remove from the map...
        // but only if the current and previous base layer are not the same
        if(currentBaseLayer != e.layer) {
            map.removeLayer(currentBaseLayer);
        }
        currentBaseLayer = e.layer;
    })

    // send a message to the sidenav letting it know that the baselayer has
    // been switched
    function tellSidenavAboutBaselayerChange(oldBaseLayerId, newBaseLayerId) {
        var data = {
            oldBaseLayerId: oldBaseLayerId,
            newBaseLayerId: newBaseLayerId
        }
        $scope.$broadcast("baseLayerChanged", data);
    }

    /**
      Calls periodically, passing in the current default base layer
      (not leaflet layer, just the regular layer object that the sidenav uses.

      This function will keep track of which base layer to keep in the base layer widget.
    */
    var previousBaseLayer = undefined;
    $scope.updateBaseLayer = function(baseLayer) {
        if(baseLayer == undefined) {
            // do nothing
        }
        // if this is the first time a base layer is being added, add it
        else if(previousBaseLayer == undefined) {
            addBaseLayer(baseLayer);
        }
        // if layers have changed, deselect previous and select new
        else if(baseLayer.id != previousBaseLayer.id) {
            addBaseLayer(baseLayer);
            removeBaseLayer(previousBaseLayer);
        }
        previousBaseLayer = baseLayer;
    }

    /**
      Turns a normal layer object (as retreived from the backend) into a leaflet
      layer, stores it and makes it available in the base layer control
    */
    function addBaseLayer(layer) {
        var leafletLayer = $scope.makeLeafletLayer(layer);
        leafletBaseLayers[layer.id] = leafletLayer;
        baseMapsControl.addBaseLayer(leafletLayer, layer.title);
    }

    /**
      Takes a layer object (as retreived from the backend), and removes it as
      a base layer.
    */
    function removeBaseLayer(layer) {
        // removes that layer from the base layer control widget
        baseMapsControl.removeLayer(leafletBaseLayers[layer.id]);
        // remove layer from map view (don't render map tiles from it)
        deselectBaseLayer(layer);
        // deletes that leaflet layer which was originally created
        delete leafletBaseLayers[layer.id];
    }

    /**
      Takes a layer object (as retreived from the backend) and adds
      it to the leaflet map
    */
    function selectBaseLayer(layer) {
        map.addLayer(leafletBaseLayers[layer.id]);
    }

    /**
      Takes a layer object (as retreived from the backend) and removes its
      associated leaflet layer from the map view
    */
    function deselectBaseLayer(layer) {
        map.removeLayer(leafletBaseLayers[layer.id]);
    }

    // Pass in the actual layer id (as stored on the backend), and the function
    // will return the leaflet layer associated with that ID
    function getLeafletLayerById(layerId) {
        return $scope.leafletLayers[layer.id];
    }

    // force refresh when view is switched
    // necessary to make leaflet rerender for new proper window dimensions
    // changing the view with leaflet to "display: none" messes with rendering dimensions
    // and causes tiles to load very slowly.  must force a reload to fix tile download speed bug.
    // FIXME not ideal, but temporary fix for persistant forms between views.
    // may be switching to openlayers soon in which this should not be necessary
    $scope.$on("pageChange", function () {
        // refresh needs to happen after leaflet controller has loaded, which can
        // be dependent on processor speed.  The 10ms timeout should be sufficient
        // on the vast majority of computers
        $timeout(map._onResize, 10);
        $timeout(map._onResize, 100);
        $timeout(map._onResize, 1000);
    });

    // reload leaflet map every few seconds, to make sure tile-loading bug
    // never happens
    $interval(map._onResize, 10000);

    // add searching by address functionality to leaflet
    // note: the search control is hidden at first...
    new L.Control.GeoSearch({
        position: 'topright',
        provider: new L.GeoSearch.Provider.OpenStreetMap()
    }).addTo(map);

    // ...then it is made visible if the frontend can connect to the geocoding
    // service
    // In the future, we may stand up our own reverse GeoCoding server
    // possible Nominatim?
    $http.get((location.protocol === 'https:' ? 'https:' : 'http:') +
        '//nominatim.openstreetmap.org/search').then(function() {
        var queryResult = document.querySelector('.leaflet-control-geosearch');
        angular.element(queryResult).css("display", "inline");
    }, function(err) {
        console.warn("Not connected to OpenStreetMap geocoding service. Searching by place disabled.");
    })

    /*********************** Rectangle Draw Control ***************************/

    // we use the snappingBoxGroup to hold a generated snapped-to-tiles bbox
    var snappingBoxGroup = new L.FeatureGroup();
    map.addLayer(snappingBoxGroup);

    // drawnItems holds the retangle which is drawn by the user
    var drawnItems = new L.FeatureGroup();
    map.addLayer(drawnItems);

    // Initialise the draw control and pass it the FeatureGroup of editable layers
    var drawControl = new L.Control.Draw({
        position: 'topright',
        edit: {
            featureGroup: drawnItems,
            edit: true
        },
        draw: {
            rectangle: {
                shapeOptions: {
                    color: DRAWN_BBOX_COLOR,
                    weight: 2
                },
            },
            polyline: false,
            circle: false,
            marker: false,
            polygon: false,
        }
    });

    // tooltips
    L.drawLocal.edit.toolbar.buttons = {
        edit: 'Edit Bounding Box',
        editDisabled: 'Edit Bounding Box',
        remove: 'Clear Bounding Box',
        removeDisabled: 'Clear Bounding Box'
    }

    L.drawLocal.draw.handlers.rectangle.tooltip.start = "Click and drag to specify target bounds";
    L.drawLocal.draw.handlers.simpleshape.tooltip.end = "Release to create target bounds";
    L.drawLocal.edit.toolbar.buttons.edit = "Edit Bounds";
    L.drawLocal.draw.toolbar.buttons.rectangle = "Draw Bounds";

    map.addControl(drawControl);
    angular.element(document.querySelector(".leaflet-draw")).addClass("icon");
    angular.element(document.querySelector(".leaflet-draw")).attr('id', 'draw-toolbar');
    angular.element(document.querySelector(".leaflet-tile-pane")).css({visibility: "visible"});

    // toggles showing grid lines on leaflet map
    var gridButton = L.easyButton({
        id: 'grid-btn',  // an id for the generated button
        type: 'replace',          // set to animate when you're comfy with css
        leafletClasses: true,     // use leaflet classes to style the button?
        position: 'topright',
        states:[{                 // specify different icons and responses for your button
            onClick: function(button, map){
                angular.element(document.querySelector('.leaflet-map-pane')).toggleClass('show-grid');
            },
            title: 'Toggle Grid Lines',
            icon: '<i class="fa fa-th" aria-hidden="true"></i>'
        }]
    })
    gridButton.addTo(map);

    $scope.leafletLayers = {};
    $scope.lastZIndex = 3;

    $scope.reorderLayers = function(orderedLayerIds){
        currentBaseLayer.setZIndex(0); // make base layer on bottom

        // set up proper layer order for non-base layers
        var reverseLayerIds = angular.copy(orderedLayerIds).reverse()
        $scope.lastZIndex=3;
        for (var i = 0; i < reverseLayerIds.length; i++){
            $scope.leafletLayers[reverseLayerIds[i]].setZIndex($scope.lastZIndex);
            $scope.lastZIndex += 1;
        }
    };

    $scope.toggleVisibility = function(layer){
        var leafletLayer = $scope.leafletLayers[layer.id]
        if (leafletLayer.options.opacity === 0){
            leafletLayer.setOpacity(1);
        } else{
            leafletLayer.setOpacity(0);
        }
    }

    $scope.selectLayer = function(layer, service){
        // operation for selecting a base layer is different that normal layers
        if(layer.is_base_layer) {
            selectBaseLayer(layer);
            return;
        }

        var leafletLayer = $scope.makeLeafletLayer(layer, service);

        $scope.lastZIndex += 1;
        $scope.leafletLayers[layer.id]= leafletLayer;
        map.addLayer(leafletLayer);
        map.fitBounds(leafletLayer.options.bounds);
    }

    $scope.deselectLayer = function(layer){
        // only deselect the layer if it's already added to the leaflet map
        if($scope.leafletLayers[layer.id] != undefined) {
            // operation for deselecting a base layer is different that normal layers
            if(layer.is_base_layer) {
                deselectBaseLayer(layer);
                return;
            }
            var leafletLayer =  $scope.leafletLayers[layer.id];
            map.removeLayer(leafletLayer);
            delete $scope.leafletLayers[layer.id];
        }
    }

    $scope.makeLeafletLayer = function(layer, service){
        // if the service isn't passed in, then try to grab the url
        // from the layer
        var url;
        if(service == null || service == undefined) {
            url = layer.url
        } else {
            url = service.url;
        }

        var swlat = layer.bbox[1];
        var swlong = layer.bbox[0];
        var nelat = layer.bbox[3];
        var nelong = layer.bbox[2];
        var southWest = L.latLng(swlat, swlong);
        var northEast = L.latLng(nelat, nelong);


        // WMTS - HARDCODED with DIGITAL GLOBE - works, but misaligned with WMS layes
        // var url = 'http://localhost:8080/gpep/mapproxy/digital_globe/wmts/' +
        // 'DigitalGlobe_ImageryTileService' + '/EPSG3857/{z}/{x}/{y}.png';
        // var tileLayer = L.tileLayer(url, {
        //         maxZoom: 19,
        //         attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        //     });

        // WMS NOTE currently only method that works
        var tileLayer = new L.tileLayer.wms(url, {
            id: layer.id,
            format: 'image/png',
            transparent: true,
            bounds: L.latLngBounds(southWest, northEast),
            layers: layer.name,
            title: layer.title,
            noWrap: true
        });

        return tileLayer
    }



    /***************************** Snapped BBOX *******************************/

    /**
     * This section deals with the maintaining of another overlaying bounding box which
     * snaps to tiles as the user is drawing his own bounding box.
     *
     * This snapped bounding box is "tile aligned" to tiles at the max zoom level
     * (as set in the Job Panel).  It includes all tiles at the max zoom level
     * which intersect the user-drawn bbox.
     *
     *  TODO: this should be moved to its own class, in its own file.
     * this class should be portable and work with arbitrary leaflet map objects.
     * it would accept a map object
     * upon creation, and would provide public-facing API methods for drawing
     * tile-aligned bounding boxes.
     */

     // tells what to snap the user drawn bounding box to
     // @param max snaps to the maximum (most zoomed in) zoom level
     // @param min snap to the minimum zoom level
    $scope.snapToLevel = "min"

    $scope.getSnappingLevel = function() {
        if($scope.snapToLevel == "min") {
            return $scope.minZoom;
        } else return $scope.maxZoom;
    }

    // returns the latidude and longitude bounds object representing the
    // user-drawn bounds in this format:
        // {north: xxx, south:xxx, east: xxx, west:xxx}
    $scope.getCurrentlyDrawnBounds = function() {
        var layer = drawnItems.getLayers()[0];
        var bounds = layerToBounds(layer);
        return bounds;
    }

    // calling this function with a new max zoom level will redraw the snapped
    // bounding box according to this zoom level
    $scope.acceptNewZoomLevels = function(newZoomLevels) {
        $scope.minZoom = newZoomLevels.min;
        $scope.maxZoom = newZoomLevels.max;

        var bounds = $scope.getCurrentlyDrawnBounds();

        // do not draw snapped grid if there is no bounds box drawn
        if(bounds === undefined) {
            return;
        }
        redrawSnappedGrid(bounds, $scope.getSnappingLevel());
    };

    // when a new bounding box is drawn, remove the previously drawn one
    map.on('draw:drawstart', function (e) {
        // clears the generated snapped bounding box
        snappingBoxGroup.clearLayers();
        // clears the drawn bounding box
        drawnItems.clearLayers();
        // lets child controlls of the leaflet contorller know that the draw
        // bounds tool has been activate.  in this case:
        // clear bounds from the settings panel
        $scope.$broadcast("drawBoundsToolActivated");
    })


    /* convenience functions, plucks bounds information from a leaflet.draw event
       which concerns a rectangle being drawn */
    function eToBounds(e) {
        return layerToBounds(e.layer);
    }

    function layerToBounds(layer) {
        if(layer === undefined) {
            return;
        }
        var bounds = {
            north: layer.getBounds().getNorth(),
            east:  layer.getBounds().getEast(),
            south: layer.getBounds().getSouth(),
            west:  layer.getBounds().getWest(),
        };
        //$scope.snappedBounds = bounds;
        return bounds;
    }

    // if user is in leaflet.draw edit mode, when they resize the bbox, update
    // the snapped bbox accordingly
    map.on('draw:editresize', function(e){
        redrawSnappedGrid(eToBounds(e), $scope.getSnappingLevel());
    })
    // if user is in leaflet.draw edit mode, when they move the bbox, update
    // the snapped bbox accordingly
    map.on('draw:editmove', function(e){
        redrawSnappedGrid(eToBounds(e), $scope.getSnappingLevel());
    })

    // this function is called every few milliseconds to make sure the
    // snapped bbox is always up to date
    function redrawSnappedGridDefaults() {
        if($scope.snappedBounds === undefined) {
            return;
        }

        var layer = drawnItems.getLayers()[0];

        // return if no bounds are currently drawn
        if(layer === undefined) {
            return;
        }
        redrawSnappedGrid(layerToBounds(layer), $scope.getSnappingLevel())
    }

    // constantly update green snapped bounding box, to correspond with box
    // user has drawn
    setInterval(redrawSnappedGridDefaults, 300);

    /**
    Goes under the hood to retrieve the latitude longitude bounds of the rectangle
    currently being drawn.

    While a rectangle is being drawn, there is no documented way to retrieve
    the bounds of the box, because it doesn't get added to any layers until
    the mouse is released and the rectangle is officially added to the map.

    This function hackily goes under the hood to swipe dem bounds while the
    rectangle is being drawn.  This is important for UX, so the user can see the
    grid bounds snap AS they draw, giving them an instant feedback loop

    It reteives the bounds by looking at the hidden layer objects in the leaflet
    map object, and trying to find the right layer which holds our tile-aligned
    bounding box.

    @important This is extremely prone to breaking.  A better solution would be
    to extend core funtionality of leaflet.draw itself to provide constant
    event fires while a rectangle is being drawn.

    @note Stability can be increased by checking for the specific color of bbox
    here, and ignoring boxes of other colors.  However, this also might increase
    the chance of it breaking down the road if another developer changes the color
    of the snapping bounding box
    */
    function snatchDrawingBounds() {
        var layers = drawnItems._map._layers;

        var bounds = {};

        for(var layerID in layers) {
            var layer = layers[layerID];
            if(layer.hasOwnProperty('_latlngs')) {
                var latlngs = layer._latlngs;
                bounds.south = latlngs[0].lat;
                bounds.north = latlngs[1].lat;
                bounds.west  = latlngs[1].lng;
                bounds.east  = latlngs[2].lng;
                return bounds;
            }
        }
    }

    // contantly update snapped grid while user is actively drawing
    // a bounding box
    function redrawSnappedGridWhileDrawing() {
        // only update while user is drawing
        if(isUserInBoxCreationMode) {
            $scope.snappedBounds = snatchDrawingBounds();
            redrawSnappedGrid($scope.snappedBounds, $scope.getSnappingLevel());
        }
    }
    setInterval(redrawSnappedGridWhileDrawing, 200);

    // used to determine when user is actively creating a box
    var isUserInBoxCreationMode = false;
    map.on('draw:drawstart', function(e) {
        isUserInBoxCreationMode = true;
    });

    map.on('draw:drawstop', function() {
        isUserInBoxCreationMode = false;
    });

    // When a new bounding box is drawn, broadcast it to everyone
    // Specifically, this broadcasted event can be used by the settings panel
    // to update map bounds for seeding, packaging, and cleaning panels
    map.on('draw:created', function(e) {
        var boundingBox = eToBounds(e);

        // let everyone know a knew bounding box has been drawn
        // this let's the settings panel update its bounding box inputs
        $rootScope.$broadcast("newSelectionBoundsDrawn", boundingBox);

        e.layer.layerType = e.layerType; // sets the layer type to rectangle

        redrawSnappedGrid(boundingBox, $scope.getSnappingLevel());

        // when a new bounding box is drawn, add it to the drawnItems layer.
        // this way, it will stick (won't instantly disappear)
        drawnItems.addLayer(e.layer); // add the drawn item to the
    });

    // triggered when the delete (trash can) button is pressed on the map
    map.on('draw:deletestart', function(e) {
        $timeout(function() {
            angular.element(document.querySelector('.leaflet-draw-draw-rectangle').click());
        }, 0);
    });

    // draw a bbox based on "bounds", but which snaps to tiles based on given
    // zoom level. any tile at the given "zoom" level touching "bounds" is part
    // of the snapped bounds, and will thus be included on the redrawn snapped bounds
    function redrawSnappedGrid(bounds, zoom) {
        if(bounds === undefined) {
            return;
        }
        var southWest = L.latLng(bounds.south, bounds.west);
        var northEast = L.latLng(bounds.north, bounds.east);
        var inputBounds = L.latLngBounds(southWest, northEast);

        var snappedBounds = getSnappedBounds(map, inputBounds, zoom);
        var snappedBoundsArray = [[snappedBounds.getNorth(), snappedBounds.getEast()],
          [snappedBounds.getSouth(), snappedBounds.getWest()]];

        var snappingBoxConfig = {
            color: SNAPPED_BBOX_COLOR,
            weight: 2,
        }

        // hide the snapped grid if user has selected packaging tab
        if(JobFactory.currentSettingsPanelTab != JobFactory.PACKAGE_JOBTYPE) {
            snappingBoxConfig.className = "hidden-snapped-bbox"
        }

        var snappingBox = L.rectangle(snappedBoundsArray, snappingBoxConfig);

        snappingBoxGroup.clearLayers();
        snappingBoxGroup.addLayer(snappingBox);

        $scope.snappedBounds = snappedBounds;

        // when new snapped bounds are calculated, the settings panel needs
        // to know about it.  we make sure to broadcast a generic
        // non-leaflet-specific bounds object, which our jobs panel can then
        // consume.
        $rootScope.snappedBoundsGeneric = {
            north: snappedBounds.getNorth(),
            east:  snappedBounds.getEast(),
            south: snappedBounds.getSouth(),
            west:  snappedBounds.getWest()
        }

        // new leaflet LatLngBounds object broadcasted with snappedBounds
        $scope.$broadcast('newSnappedBounds', $rootScope.snappedBoundsGeneric);
    }

    // remove snap bbox when originally drawn bbox is deleted
    map.on("draw:deleted", function(e){
        snappingBoxGroup.clearLayers();
    })

    map.on("draw:edited", function(e){
        var layer = e.layers.getLayers()[0];
        $rootScope.$broadcast("newSelectionBoundsDrawn", layerToBounds(layer));
    })

    // When the bounding box is modified from the settings panel, update
    // it in the leaflet map view
    $scope.drawBounds = function(bounds) {
        var boundsArray = [[bounds.north, bounds.east], [bounds.south, bounds.west]];

        // create a red rectangle
        var boundingBox = L.rectangle(boundsArray, {
            color: DRAWN_BBOX_COLOR,
            weight: 2
        });

        drawnItems.clearLayers();
        drawnItems.addLayer(boundingBox);
    };

    // calculate a bounds objects given input "bounds", but which snaps to and
    // includes all tiles which intersect with the "input" bounds
    function getSnappedBounds(map, bounds, zoom) {
        // leaflet internally uses these for projection bounds
        var NORTH_WEST_LIMIT_EPSG3857 = [L.Projection.SphericalMercator.MAX_LATITUDE, -180];
        var SOUTH_EAST_LIMIT_EPSG3857 = [-L.Projection.SphericalMercator.MAX_LATITUDE, 180];

        var tileSideAmount = Math.pow(2, zoom);

        var worldPixelBounds = getWorldPixelBounds();
        var worldPixelHeight =  worldPixelBounds.southPixel -
                                worldPixelBounds.northPixel;
        var worldPixelWidth =   worldPixelBounds.eastPixel -
                                worldPixelBounds.westPixel;

        // ******************* Tile Class ******************* //
        function Tile(x, y) {
            this.x = x;
            this.y = y;
        }

        // ************* Class Methods ************* //

        Tile.prototype.top = function() {
            return this.y * tileHeight + worldPixelBounds.northPixel;
        }

        Tile.prototype.bottom = function() {
            return (this.y + 1) * tileHeight + worldPixelBounds.northPixel;
        }

        Tile.prototype.left = function() {
            return (this.x) * tileWidth + worldPixelBounds.westPixel;
        }

        Tile.prototype.right = function() {
            return (this.x + 1) * tileWidth + worldPixelBounds.westPixel;
        }

        Tile.prototype.southWest = function() {
            var southWestPoint = L.point(this.left(), this.bottom());
            var latlng = map.layerPointToLatLng(southWestPoint);
            return latlng;
        }

        Tile.prototype.northEast = function() {
            return map.layerPointToLatLng( L.point(this.right(), this.top()) );
        }

        // pixel dimensions of each tile
        var tileWidth  = worldPixelWidth / tileSideAmount;
        var tileHeight  = worldPixelWidth / tileSideAmount;

        var southWestTile = getTileAtLatLng(bounds.getSouthWest());
        var northEastTile = getTileAtLatLng(bounds.getNorthEast());

        var southWestSnap = L.latLng(southWestTile.southWest().lat, southWestTile.southWest().lng);
        var northEastSnap = L.latLng(northEastTile.northEast().lat, northEastTile.northEast().lng);

        function getTileAtLatLng(latlng) {
            return getTileAtLayerPoint(map.latLngToLayerPoint(latlng));
        }

        function getTileAtLayerPoint(layerPoint, zoomLevel) {
            var relPoint = layerPointToRelPoint(layerPoint);

            var x = Math.floor(relPoint.x / tileWidth);
            // this if statement should not be necessary; there is a deeper source of this problem
            if (x == tileSideAmount) {
                x -= 1;
            }
            var y = Math.floor(relPoint.y / tileWidth);

            return new Tile(x, y);
        }

        //**************************** Utility **************************//
        function getWorldPixelBounds() {
            var worldBounds = {};

            var upperLeftPoint = map.latLngToLayerPoint(NORTH_WEST_LIMIT_EPSG3857);
            worldBounds.northPixel = upperLeftPoint.y;
            worldBounds.westPixel  = upperLeftPoint.x;

            var lowerRightPoint = map.latLngToLayerPoint(SOUTH_EAST_LIMIT_EPSG3857);
            worldBounds.southPixel = lowerRightPoint.y;
            worldBounds.eastPixel  = lowerRightPoint.x;

            return worldBounds;
        }

        // This adjusts adjusts pixel coordinates on the leaflet map, such that
        // a coordinate of (0, 0) corresponds to a latitude of -85 and a
        // longitude of -180.  The scaling is not changed.
        //
        // note: after this transformation, all (x, y) pixel coordinates are
        // positive, which makes math easier
        function layerPointToRelPoint(layerPoint) {
            var relPoint = L.point(layerPoint.x - worldPixelBounds.westPixel,
                layerPoint.y - worldPixelBounds.northPixel);

            return relPoint;
        }

        // convert back to proper layer points before converting back to
        // latitude and longitude
        function relPointToLayerPoint(relPoint) {
            return L.point(relPoint.x + worldBounds.westPixel,
                layerPoint.y + worldBounds.northPixel);
        }

        return L.latLngBounds(southWestSnap, northEastSnap);
    }

	 L.GeometryUtil.readableArea = function (area, isMetric) {
		var areaStr;
		if (isMetric) {
			if (area >= 10000) {
				areaStr = (area /Math.pow(10, 6)).toFixed(2) + ' km&sup2';
			} else {
				areaStr = area.toFixed(2) + ' m&sup2;';
			}
		} else {
			area /= 0.836127; // Square yards in 1 meter

			if (area >= 3097600) { //3097600 square yards in 1 square mile
				areaStr = (area / 3097600).toFixed(2) + ' mi&sup2;';
			} else if (area >= 4840) {//48040 square yards in 1 acre
				areaStr = (area / 4840).toFixed(2) + ' acres';
			} else {
				areaStr = Math.ceil(area) + ' yd&sup2;';
			}
		}
		return areaStr;
    }




});
