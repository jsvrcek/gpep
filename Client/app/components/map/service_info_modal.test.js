// describe('Service Info Modal Controller', function() {
//
//      beforeEach(angular.mock.module('ui.bootstrap'));
//      beforeEach(angular.mock.module('toastr'));
//      beforeEach(angular.mock.module('MapApp'));
//      beforeEach(angular.mock.module('ServerApp'));
//      beforeEach(angular.mock.module('UtilitiesApp'));
//      beforeEach(angular.mock.module('ProgressApp'));
//
//     var controller, scope, UtilityFactory, ServiceFactory, ModalFactory, $uibModalInstance;
//
//     // BUG uibModalInstance isn't being injected properly
//     // a fake modal probably needs to be mocked from the $modal service, which can
//     // create uibModalInstance objects
//     beforeEach(inject(function($controller, $rootScope, _$uibModalInstance_, _UtilityFactory_, _ServiceFactory_, _ModalFactory_) {
//         scope = $rootScope.$new();
//         $uibModalInstance = _$uibModalInstance_
//
//         // The injector unwraps the underscores (_) from around the parameter names when matching
//         UtilityFactory = _UtilityFactory_;
//         ServiceFactory = _ServiceFactory_;
//         ModalFactory = _ModalFactory_;
//
//         controller = $controller('ServiceInfoModalController', {
//             $scope: scope,
//             $rootScope: scope,
//             $uibModalInstance: _$uibModalInstance_,
//             UtilityFactory: UtilityFactory,
//             ServiceFactory: ServiceFactory,
//             ModalFactory: ModalFactory
//         });
//     }));
//
//     it('test something in the progress controller', function() {
//         fail("Progress Controller test not yet implemented")
//     });
// });
