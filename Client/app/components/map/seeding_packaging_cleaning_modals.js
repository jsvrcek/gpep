"use strict";

/**
 * seeding_packaging_cleaning_modals.js contains 3 controllers - one each for
 * the seeding, packaging, and cleaning modals.  These controllers contains
 * logic which will do the respective job actions that each modal is tasked
 * with doing.
 */

/***************************** Seeding Modal Controller *********************/

//controller for starting a new seed job
var MapApp = angular.module('MapApp');

MapApp.controller('SeedingModalController',
['$scope', '$uibModalInstance', 'toastr', 'sharedSettings', 'UtilityFactory', 'ModalFactory', 'JobFactory',
function ($scope, $uibModalInstance, toastr, sharedSettings, UtilityFactory, ModalFactory, JobFactory) {

  // modals need access to the job factory
  $scope.JobFactory = JobFactory;
  $scope.jobType = JobFactory.SEED_JOBTYPE;

  JobFactory.sizeEstimate(sharedSettings).then(function(data) {
        $scope.estimate = data.data;
      }, function(err) {
        $scope.estimateError = true;
  });


  function errorToPrettyString(err) {
    return "Status: " + err.status + "\nStatus Text: " + err.statusText +
    "\n\nData Response: " + JSON.stringify(err.data)
  }

  $scope.startSeeding = function () {
      var busyToast = UtilityFactory.spinnerToast('Attempting To Start Seeding');

      JobFactory.startJob(sharedSettings, JobFactory.SEED_JOBTYPE).then(function(data){
        toastr.clear(busyToast);
        toastr.success("Seeding job started successfully.");
      }, function(err){
        toastr.clear(busyToast);
        ModalFactory.basicError("Seeding Failed", err);
      });
  };

  $scope.closeModal = function () {
      $uibModalInstance.dismiss('cancel');
  };
}]);

/***************************** Packaging Modal Controller *********************/

MapApp.controller('PackagingModalController',
['$scope', '$uibModalInstance', 'toastr', 'sharedSettings', 'packageSettings', 'UtilityFactory', 'ModalFactory', 'JobFactory',
function ($scope, $uibModalInstance, toastr, sharedSettings, packageSettings, UtilityFactory, ModalFactory, JobFactory) {

  // modals need access to the job factory
  $scope.JobFactory = JobFactory;
  $scope.jobType = JobFactory.PACKAGE_JOBTYPE;


  JobFactory.sizeEstimate(sharedSettings).then(function(data) {
        $scope.estimate = data.data;
      }, function(err) {
        $scope.estimateError = true;
  });

  function errorToPrettyString(err) {
    return "Status: " + err.status + "\nStatus Text: " + err.statusText +
    "\n\nData Response: " + JSON.stringify(err.data)
  }

  function errorToBasicError(err) {
      var errors = [];
      console.log(err);
      return err;
  }

  $scope.startPackaging = function () {

      var busyToast = UtilityFactory.spinnerToast('Attempting To Start Packaging');
      var params = angular.extend(angular.copy(sharedSettings), packageSettings)
      JobFactory.startJob(params, JobFactory.PACKAGE_JOBTYPE).then(function(data){
        toastr.clear(busyToast);
        toastr.success("Packaging job started successfully.");
      },function(err){
        toastr.clear(busyToast);
        errorToBasicError(err);
        ModalFactory.basicError("Packaging Failed",err);
      });

  };

  $scope.closeModal = function () {
      $uibModalInstance.dismiss('cancel');
  };
}]);

/***************************** Cleaning Modal Controller *********************/

MapApp.controller('CleaningModalController',
['$scope', '$uibModalInstance', 'toastr', 'sharedSettings', 'cleanSettings', 'UtilityFactory', 'ModalFactory', 'JobFactory',
function ($scope, $uibModalInstance, toastr, sharedSettings, cleanSettings, UtilityFactory, ModalFactory, JobFactory) {

  // modals need access to the job factory
  $scope.JobFactory = JobFactory;
  $scope.jobType = JobFactory.SEED_JOBTYPE;


  function errorToPrettyString(err) {
    return "Status: " + err.status + "\nStatus Text: " + err.statusText +
    "\n\nData Response: " + JSON.stringify(err.data)
  }

  $scope.startCleaning = function () {

      var busyToast = UtilityFactory.spinnerToast('Attempting To Start Cleaning');
      var params = angular.extend(angular.copy(sharedSettings), cleanSettings)

      JobFactory.startJob(params, JobFactory.CLEAN_JOBTYPE).then(function(data){
        toastr.clear(busyToast);
        toastr.success("Cleaning job started successfully.");
      },function(err){
        toastr.clear(busyToast);
        ModalFactory.basicError("Cleaning Failed",err);
      });
  };

  $scope.closeModal = function () {
      $uibModalInstance.dismiss('cancel');
  };
}]);

/***************************** Export Modal Controller *********************/

MapApp.controller('ExportModalController',
  function ($scope, $uibModalInstance, toastr, mapProxyApp, UtilityFactory, ModalFactory, JobFactory) {

  // modals need access to the job factory
  $scope.JobFactory = JobFactory;
  $scope.jobType = JobFactory.EXPORT_JOBTYPE;

  $scope.mapProxyApp = mapProxyApp;

  $scope.calculating = function(){
    return !angular.isNumber(mapProxyApp.cache_size)
  }

  $scope.startExport = function () {
      var busyToast = UtilityFactory.spinnerToast('Attempting To Start Export');

      JobFactory.startExportJob(mapProxyApp.id).then(function(data){
        toastr.clear(busyToast);
        toastr.success("Export job started successfully.");
      },function(err){
        toastr.clear(busyToast);
        ModalFactory.basicError("Export Failed",
           "Export job failed to start. \n\n" + errorToPrettyString(err));
      });
  };

  $scope.closeModal = function () {
      $uibModalInstance.dismiss('cancel');
  };
});
