
angular.module('ProgressApp').component('jobProgress', {
    templateUrl: 'components/job-progress/job-progress.html',
    controller: JobProgressController,
    bindings: {
        job: '=',
        numTasksToShow: '=',
        showAllTasks: '='
  }

});

function JobProgressController($uibModal, $scope, JobFactory, $httpParamSerializer,
                               ModalFactory, ServiceFactory) {
    var ctrl = this;
    ctrl.ServiceFactory = ServiceFactory;

    /*
      When a job completes, if it was a package or export job, its Complete
      toast notification can be clicked to download its generated files.

      When clicked, an event is broadcasted from progress_control.js, which
      all JobProgressControllers listen for.
    */

    /*
      Downloads the Geopackage file of the job with the specified Job ID
      note: this event is broadcast in progres_control.js when
        a "package job completed" toast is clicked
    */
    $scope.$on("downloadAllGeopackages", function(event, id) {
        if(ctrl.job.id == id) {
            ctrl.downloadAllGeopackages();
        }
    })

    /*
      Downloads the PEP file of the job with the specified Job ID
      note: this event is broadcast in progres_control.js when
        an "export job completed" toast is clicked
    */
    $scope.$on("downloadPepFile", function(event, id) {
        if(ctrl.job.id == id) {
            ctrl.downloadTask(ctrl.job.tasks[0]);
        }
    })

    ctrl.toggleShowAllTasks = function() {
        if (ctrl.job.id in ctrl.showAllTasks) {
            ctrl.showAllTasks[ctrl.job.id] = !ctrl.showAllTasks[ctrl.job.id]
        } else {
            ctrl.showAllTasks[ctrl.job.id] = true
        }
    };

    ctrl.downloadTask = function(task) {
        if(ctrl.job.job_type == 'export') {
            // implement download code
            var filename = task.name + ".pep"
            JobFactory.fileAvailableForDownload(task.id, filename).then(function(){
                JobFactory.downloadFile(task.id, filename);
            }, function(err) {

            })

        } else if (ctrl.job.job_type == 'package') {
            ctrl.openPackageNameModal([task]);
        }
    };

    ctrl.openJobDetailsModal=function() {
        console.log(ctrl.job)
        ctrl.packageNameModalInstance = $uibModal.open({
            templateUrl: "components/progress/job_details_modal.html",
            controller: 'JobDetailsModalCtrl',
            controllerAs: '$ctrl',
            resolve: {
                job: function(){
                    return ctrl.job;
                },
            }
        });
    };

    ctrl.openCancelJobModal=function(){
        ctrl.packageNameModalInstance = $uibModal.open({
            templateUrl: "components/progress/cancel_job_modal.html",
            controller: 'CancelJobModalCtrl',
            controllerAs: '$ctrl',
            resolve: {
                job: function(){
                    return ctrl.job;
                },
            }
        });
    };

    ctrl.openPackageNameModal=function(tasks){
        ctrl.packageNameModalInstance = $uibModal.open({
            templateUrl: "components/progress/package_name_modal.html",
            controller: 'PackageNameModalCtrl',
            controllerAs: '$ctrl',
            resolve: {
                job: function(){
                    return ctrl.job;
                },
                tasks: function () {
                    return tasks;
                }
            }
        });
    };

    ctrl.downloadAllGeopackages=function(){
         ctrl.openPackageNameModal(ctrl.job.tasks);
    };

    ctrl.showTaskError = function(task) {
        ModalFactory.basicError("Task with ID " + task.id + " has errors.", task.failure_message)
    }
}
