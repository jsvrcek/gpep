
  var ServerApp = angular.module('ServerApp');

  ServerApp.factory('JobFactory', function (
    $http, $rootScope, ServiceStore, $httpParamSerializer, UtilityFactory, CREATE_JOB_ENDPOINT, GET_JOBS_ENDPOINT,
    DELETE_JOB_ENDPOINT, DOWNLOAD_FILE_ENDPOINT, FILE_AVAILABLE_FOR_DOWNLOAD_ENDPOINT, SIZE_ESTIMATE_ENDPOINT, CANCEL_JOB_ENDPOINT, $q) {

      var o = {
        Job: Job,
        SEED_JOBTYPE: "seed",
        PACKAGE_JOBTYPE: "package",
        CLEAN_JOBTYPE: "clean",
        EXPORT_JOBTYPE: "export",
        currentSettingsPanelTab: "seed", // should match selected tab on UI
        startJob: startJob,
        startExportJob: startExportJob,
        getJobs: getJobs,
        getJobDetails: getJobDetails,
        deleteJob: deleteJob,
        cancelJob: cancelJob,
        fileAvailableForDownload: fileAvailableForDownload,
        downloadFile: downloadFile,
        sizeEstimate: sizeEstimate,
        isJobTypeSupportedForService: isJobTypeSupportedForService,
        getSupportedServices: getSupportedServices,
        getUnsupportedSelectedServices: getUnsupportedSelectedServices,
      };

      return o

      // pulls jobs from GPEP
      function Job(object){
        angular.copy(object, this)
        this.created_at = new Date(this.created_at);
        this.percent = this.tasks.reduce(function(total,task){
            return total + task.progress.percent
        },0) / this.tasks.length;

        this.tasks = this.tasks.map(function(task) {
            task.started_at = new Date(task.start_date);
            if(task.progress.eta.length){
              task.progress.eta = new Date(task.progress.eta);
            }
            if (task.layers){
              task.layerNames = task.layers.map(function(layer) {return layer.name}).join(", ")
            }
            return task;
        })
    }



      function formatDate(date) {
          var d = new Date(date),
              month = '' + (d.getMonth() + 1),
              day = '' + d.getDate(),
              year = d.getFullYear();

          if (month.length < 2) month = '0' + month;
          if (day.length < 2) day = '0' + day;

          return [year, month, day].join('-');
      }

      // adds proper min/max x and y values, based on job type
      // "data" is an arbitrary object passed in
      // "params" contains snapped bounds AND actual bounds.  the proper
      // bounds will be appened and returned to the "data" object based
      // on job type
      function extendWithBbox(data, params, jobType) {
          // only used snapped bounds for packaging, other jobtypes use
          // actual bounds specified by the user
          if(jobType == o.PACKAGE_JOBTYPE) {
              data = angular.extend(data, {
                  min_x: params.snappedWest,
                  min_y: params.snappedSouth,
                  max_x: params.snappedEast,
                  max_y: params.snappedNorth,
              })
          } else {
              data = angular.extend(data, {
                  min_x: params.bboxW,
                  min_y: params.bboxS,
                  max_x: params.bboxE,
                  max_y: params.bboxN,
              })
          }
          return data;
      }

      function sizeEstimate(params){
        // get all layer IDs that are currently selected
        var layer_ids = ServiceStore.getSelectedLayerIDs();

        var data = {
              layer_ids: layer_ids,
              get_time: true,
              min_zoom: params.startZoomLevel,
              max_zoom: params.endZoomLevel,
        }
        data = extendWithBbox(data, params, o.currentSettingsPanelTab);

        return $http.get(SIZE_ESTIMATE_ENDPOINT, {params: data})
      }

      /**
        Return true if jobType is supported for specified service, false otherwise
      */
      function isJobTypeSupportedForService(jobType, service) {
          return !(service.imported_geopackage && jobType == o.SEED_JOBTYPE);
      }

      function getSelectedServices() {
          return ServiceStore.services.filter(function(service){
            return ServiceStore.numLayersSelected(service) > 0
          });
      }

      /**
        Returns only services for which at least one layer is selected,
        and that are supported for the specified job type.
      */
      function getSupportedServices(jobType) {
          return getSelectedServices().filter(function(service){
            return isJobTypeSupportedForService(jobType, service);
          })
      }

      /**
        Returns selected services not supported for specified job type
      */
      function getUnsupportedSelectedServices(jobType) {
          return getSelectedServices().filter(function(service){
            return !isJobTypeSupportedForService(jobType, service);
          })
      }

      function startJob(params, jobType) {
        // only keeps services that have at least one layer selected
        // and are supported for the specified jobType
        var services = getSupportedServices(jobType).map(function(service){
          return {
            id: service.id,
            layer_ids: service.layers.filter(function(layer){
              return ServiceStore.selectedLayersKeys[layer.id];
            }).map(function(layer){
              return layer.id
            })
          }
        })

        var data = {
              job_type: jobType,
              services: services,
              min_zoom: params.startZoomLevel,
              max_zoom: params.endZoomLevel,
        }
        data = extendWithBbox(data, params, jobType);

        switch (jobType){

          case o.SEED_JOBTYPE:
           data.num_cache_processes= 2;
           break;

          case o.CLEAN_JOBTYPE:
            data.date = formatDate(params.date);
            break;

          case o.PACKAGE_JOBTYPE:
            switch (params.exportType) {
              case 'NSG_GEO':
                data.package_type = 'NSG';
                data.srs = "EPSG:4326";
                break;
              case 'NSG_MERC':
                data.package_type = 'NSG';
                data.srs = "EPSG:3395";
                break;
              case 'WARRIOR':
                data.package_type = 'NW';
                data.srs = "EPSG:3857";
                break;
            }
            data.fetch_tiles = true;
            break;
        }
        return $http.post(CREATE_JOB_ENDPOINT, data);
      }

      function startExportJob(map_proxy_app_id){
        return $http.post(CREATE_JOB_ENDPOINT, {
           job_type: o.EXPORT_JOBTYPE,
           services: [{
             id: map_proxy_app_id
           }],
        })
      }


      function getJobs() {
          var deferred = $q.defer();
          $http.get(GET_JOBS_ENDPOINT).then(function(response){
            var jobs = response.data.map(function(job){
              return new o.Job(job)
            });
            deferred.resolve(jobs)
          },function(err){
            deferred.reject(err);
          })
          return deferred.promise
      }
      function getJobDetails(job_id) {
          var deferred = $q.defer();
          $http.get(GET_JOBS_ENDPOINT+ "/"+ job_id + "/").then(function(response){
             deferred.resolve(new o.Job(response.data))
          },function(err){
            deferred.reject(err);
          })
          return deferred.promise
      }

      function deleteJob(job_id) {
          return $http.post(DELETE_JOB_ENDPOINT, {id: job_id});
      }

      function fileAvailableForDownload(task_id, filename) {
          return $http.get(FILE_AVAILABLE_FOR_DOWNLOAD_ENDPOINT,{params: {task_id: task_id, filename: filename}})
      }

     function downloadFile(task_id, filename){
        var params = {task_id:task_id, filename: filename}
        var qs = $httpParamSerializer(params);
        var download_endpoint = DOWNLOAD_FILE_ENDPOINT + "?" + qs;
        var anchor = angular.element('<a/>');
        angular.element(document.body).append(anchor);
        anchor.attr({
            href: download_endpoint,
            target: '_self',
            download: true
        })[0].click();
     }

      function cancelJob(job_id){
        return $http.post(CANCEL_JOB_ENDPOINT, {'job_id': job_id});
      }

  });
