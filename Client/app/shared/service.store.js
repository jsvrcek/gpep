/**
  service.store.js
      - retreives, adds, and deletes deletes map services from the backend via $http
      - is used to keep track of which services are selected in the side-nav

  In summary, service.store.js glues together many parts of our app, all which
  need access to the different map services and layers in those services available
  on our GPEP server.
 */

var ServerApp = angular.module('ServerApp');

ServerApp.factory('ServiceStore', function ($http, $q, ADD_SERVICE_ENDPOINT,
    SERVICE_LIST_ENDPOINT, DELETE_SERVICE_ENDPOINT) {

    var o = {
        services: [],
        // similar to openServices:  an object with key values, where the key
        // is a layer id, and its value is whether or not that layer is selected
        selectedLayersKeys: {},
        addService: addService,
        deleteService: deleteService,
        populateServices: populateServices,
        areAnyLayersSelected: areAnyLayersSelected,
        numLayersSelected: numLayersSelected,
        getSelectedLayerIDs: getSelectedLayerIDs,
    };

    return o;

    // returns true if any layers are selected at all, for all services
    function areAnyLayersSelected() {
        return Object.keys(o.selectedLayersKeys).some(function(layerKey) {
            return o.selectedLayersKeys[layerKey];
        });
    }

    // returns the number of layers selected for the given service
    function numLayersSelected(service) {
        return service.layers.filter(function(layer) {
            return o.selectedLayersKeys[layer.id];
        }).length;
    }


    function getSelectedLayerIDs() {
        return Object.keys(o.selectedLayersKeys).filter(function(layerKey) {
            return o.selectedLayersKeys[layerKey];
        });
    }

    function addService(params) {
        var deferred = $q.defer();

        var data =  {
            name: params.service_name,
            external_url: params.service_url,
            enable_cache: params.enable_cache,
        }

        $http.post(ADD_SERVICE_ENDPOINT, data).then(function(res) {
            populateServices();
            deferred.resolve(data)

        },function(err){
            console.log("Error: ", err)
            deferred.reject(err);
        })

        return deferred.promise;
    }

    function deleteService(service){

        var deferred = $q.defer();

        data = {name: service.name};
        $http.post(DELETE_SERVICE_ENDPOINT, data).then(function(res){
            var index = o.services.indexOf(service);
            o.services.splice(index, 1)
            deferred.resolve();
        }, function(err){
            console.log("Error: ", err)
            deferred.reject(err);
        })

        return deferred.promise;
    }

    function populateServices(){
        return $http.get(SERVICE_LIST_ENDPOINT).then(function(resp){
            var services = resp.data.map(function(service){
                service.url = MAPPROXY_PREFIX + service.url
                return service;
            })
            angular.copy(services, o.services)
        },function(err){
            console.log("Error: ", err)
        })
    }

  });
