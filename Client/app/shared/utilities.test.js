'use strict';

describe('Utility Factory', function() {

  var UtilityFactory;
  var toastr;

  beforeEach(angular.mock.module('UtilitiesApp'));
  beforeEach(angular.mock.module('toastr'));

  beforeEach(inject(function(_UtilityFactory_, _toastr_) {
    UtilityFactory = _UtilityFactory_;
    toastr = _toastr_;
  }));

  it("should calculate WMS capability URLs properly", function() {
      expect(UtilityFactory.getWmsCapabilitiesUrl("myFavService"))
          .toEqual(location.protocol + '//' + location.hostname + ':8080'
              + '/gpep/mapproxy/myFavService/service?REQUEST=GetCapabilities&SERVICE=WMS')
  })

  it("should calculate WMTS capability URLs properly", function() {
      expect(UtilityFactory.getWmtsCapabilitiesUrl("myFavService"))
          .toEqual(location.protocol + '//' + location.hostname + ':8080'
              + '/gpep/mapproxy/myFavService/service?REQUEST=GetCapabilities&SERVICE=WMTS')
  })

  it('should exist', function() {
      expect(UtilityFactory).toBeDefined();
  });

  it('should calculate correct zoom levels', function() {
      expect(UtilityFactory.urlZoomLevels(4,7)).toEqual([4,5,6,7]);
  });

  it('should calculate date to YMD String', function() {
      var date = new Date(2016, 7, 27);
      expect(UtilityFactory.dateToYMDString(date)).toEqual("2016-08-27");
  });

  it("", function() {

  })

  it("", function() {

  })

  it("", function() {

  })

  it("", function() {

  })

});
