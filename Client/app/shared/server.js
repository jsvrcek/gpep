"use strict";

var ServerApp = angular.module('ServerApp', []);


/******************* Service Factory (get, add, delete) ******************/
// factory to call rest endpoints for services
ServerApp.factory('ServiceFactory',
    function($http, $rootScope, $interval, UtilityFactory, toastr, DELETE_SERVICE_ENDPOINT,
      CACHING_ENABLED_ENDPOINT, CACHE_SIZE_ENDPOINT, PING_ENDPOINT, SET_BASE_LAYER_ENDPOINT,
      GET_DEFAULT_BASELAYER_ENDPOINT) {

        var serviceFactory = {};

        /**
          Can turn a layer into a base layer.
          @param layerId The id of a layer
          @param should_set_to_base_layer True if you want to make target layer
                 a base layer, False if you want it to not be a base layer.
          @return Promise which returns true if the operation was successful.
        */
        serviceFactory.setBaseLayer = function(layerId, should_set_to_base_layer) {
            return $http.post(SET_BASE_LAYER_ENDPOINT, {id: layerId, is_base_layer: should_set_to_base_layer})
        }

        /**
          Retrieves a leaflet-style tile url, for the default base layer
          @example Returns promise with data: "http://server.arcgisonline.com/ArcGIS/rest/services/World_Physical_Map/MapServer/tile/{z}/{y}/{x}"
        */
        serviceFactory.getDefaultBaselayer = function() {
            return $http.get(GET_DEFAULT_BASELAYER_ENDPOINT)
        }

        /************************* MONITOR CONNECTION TO BACKEND ***************************/

        /**
          Promise, which will be succesful if the frontend can connect to the
          backend, or will error if the frontend can't connect to the backend.
        */
        var checkConnectionToBackend = function() {
            return $http.get(PING_ENDPOINT);
        }

        /**
          Called periodically.  Shows a 'bad connection' toast if the
          frontend cannot connect to the GPEP Backend server.  Will then show
          a "successfully reconnected" toast notificaton if the frontend reconnects.
        */
        var previousPingSuccessful = true;
        var badConnectionToast;
        var reconnectedToast;
        var _isConnectedToBackend = true;
        var maintainGPEPConnectionToasts = function() {
            checkConnectionToBackend().then(function(resp) {
                _isConnectedToBackend = true;
                // if successfully reconnected to backend, show success toast
                if(!previousPingSuccessful) {
                    toastr.clear(badConnectionToast);
                    reconnectedToast = toastr.success("Reconnected to GPEP Server");
                    previousPingSuccessful = true;
                    $rootScope.$broadcast("reconnectedToBackend");
                }

                // error - failure connecting to backend
                }, function(err) {
                    _isConnectedToBackend = false;

                    // only add a bad connection toast once, if the previous ping was successful
                    // and the current ping was unsuccessful.  this way, we won't generate a new
                    // toast on each unsuccessful ping to the backend.
                    if(previousPingSuccessful) {
                        toastr.clear(reconnectedToast);
                        badConnectionToast = UtilityFactory.spinnerToastWithTitle(
                          "Attempting to Reconnect", "Can't Connect to GPEP Server", "toast-bad-connection");
                        previousPingSuccessful = false;
                    }
                }
            )
        }
        maintainGPEPConnectionToasts();
        $interval(maintainGPEPConnectionToasts, 5000);

        /**
          Returns true if the frontend has recently made a successful connection
          to the backend.  False if the frontend recently attempt to connect to the
          backend, but failed.
        */
        serviceFactory.isConnectedToBackend = function() {
            return _isConnectedToBackend;

        }

        // return promise requesting GPEP server for used cache space and available
        // space for caching
        serviceFactory.cacheSize = function(map_proxy_app_id) {
            if(map_proxy_app_id === undefined){
                return $http.get(CACHE_SIZE_ENDPOINT);
            } else {
                var params = {map_proxy_app_id: map_proxy_app_id}
                return $http.get(CACHE_SIZE_ENDPOINT, params);
            }
        }

        serviceFactory.deleteService = function(service_id) {
            var data = {
                name: service_id
            };
            return $http.post(DELETE_SERVICE_ENDPOINT, data);
        };

        serviceFactory.isCachingEnabled = function(id) {
            return $http.get(CACHING_ENABLED_ENDPOINT, {
                params: {id: id},
            });
        };

        serviceFactory.enableCaching = function(id, enable_cache) {
            return $http.post(CACHING_ENABLED_ENDPOINT, {id: id, enable_cache: enable_cache});
        };

        return serviceFactory;
    }
);
