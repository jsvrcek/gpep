'use strict';

describe('Service Store', function() {

    var ServiceStore;
    var $http;
    var $q;
    var ADD_SERVICE_ENDPOINT;
    var SERVICE_LIST_ENDPOINT;
    var DELETE_SERVICE_ENDPOINT;

    var serviceWithOneLayer;
    var serviceWithMultipleLayers;


    beforeEach(angular.mock.module('UtilitiesApp'));
    beforeEach(angular.mock.module('ServerApp'));

    beforeEach(inject(function(_ServiceStore_, _$http_, _$q_, _ADD_SERVICE_ENDPOINT_,
        _SERVICE_LIST_ENDPOINT_, _DELETE_SERVICE_ENDPOINT_) {
        ServiceStore = _ServiceStore_;
        $http = _$http_;
        $q = _$q_;
        ADD_SERVICE_ENDPOINT = _ADD_SERVICE_ENDPOINT_;
        SERVICE_LIST_ENDPOINT = _SERVICE_LIST_ENDPOINT_;
        DELETE_SERVICE_ENDPOINT = _DELETE_SERVICE_ENDPOINT_;

        serviceWithOneLayer = {
            "id": 3,
            "name": "north_carolina",
            "url": "http://localhost:8080/gpep/mapproxy/north_carolina/service",
            "layers": [{
                "id": 8,
                "name": "Orthoimagery_Latest",
                "title": "Orthoimagery_Latest",
                "bbox": [-180, -90, 180, 90]
            }]
        }

        serviceWithMultipleLayers = {
            "id": 4,
            "name": "digital_globe",
            "url": "http://localhost:8080/gpep/mapproxy/digital_globe/service",
            "layers": [{
                "id": 9,
                "name": null,
                "title": "DigitalGlobe Web Map Tile Service",
                "bbox": [-180, -90, 180, 90]
            }, {
                "id": 10,
                "name": "DigitalGlobe_FoundationGEOINT",
                "title": "DigitalGlobe:FoundationGEOINT",
                "bbox": [-180, -90, 180, 90]
            }, {
                "id": 11,
                "name": "DigitalGlobe_ImageInMosaicFootprint",
                "title": "DigitalGlobe:ImageInMosaicFootprint",
                "bbox": [-180, -90, 180, 90]
            }, {
                "id": 12,
                "name": "DigitalGlobe_ImageStrip",
                "title": "DigitalGlobe:ImageStrip",
                "bbox": [-180, -90, 180, 90]
            }, {
                "id": 13,
                "name": "DigitalGlobe_ImageryTileService",
                "title": "DigitalGlobe:ImageryTileService",
                "bbox": [-180, -90, 180, 90]
            }, {
                "id": 14,
                "name": "DigitalGlobe_NGAOtherProducts",
                "title": "DigitalGlobe:NGAOtherProducts",
                "bbox": [-180, -90, 180, 90]
            }]
        }
        ServiceStore.services = [serviceWithOneLayer, serviceWithMultipleLayers];
    }));

    describe("layer selection querying", function() {
        beforeEach(function() {
            spyOn(ServiceStore, "areAnyLayersSelected").and.callThrough();
            spyOn(ServiceStore, "numLayersSelected").and.callThrough();
        })

        it("areAnyLayersSelected() returns false when no layers selected", function() {
            expect(ServiceStore.areAnyLayersSelected()).toEqual(false);
        })

        it("areAnyLayersSelected() returns true when one layer is selected", function() {
            // simulates the 4th layers of the 2nd service in the service list as being selected
            ServiceStore.services[1].layers[3].selected = true;
            expect(ServiceStore.areAnyLayersSelected()).toEqual(true);
        })

        it("ensure numLayersSelected works properly", function() {
            // no layers selected
            expect(ServiceStore.numLayersSelected(serviceWithOneLayer)).toEqual(0);

            // one layer selected
            serviceWithOneLayer.layers[0].selected = true;
            expect(ServiceStore.numLayersSelected(serviceWithOneLayer)).toEqual(1);

            // no layers selected
            var serviceWithMultipleLayers = ServiceStore.services[1];
            expect(ServiceStore.numLayersSelected(serviceWithMultipleLayers)).toEqual(0);

            // one layers selected
            serviceWithMultipleLayers.layers[2].selected = true;
            expect(ServiceStore.numLayersSelected(serviceWithMultipleLayers)).toEqual(1);

            // 3 layers selected
            serviceWithMultipleLayers.layers[0].selected = true;
            serviceWithMultipleLayers.layers[4].selected = true;
            expect(ServiceStore.numLayersSelected(serviceWithMultipleLayers)).toEqual(3);
        })
    })

    describe("dealing with services", function() {
        var $httpBackend;

        beforeEach(inject(function($injector) {
            $httpBackend = $injector.get('$httpBackend');
        }));

        afterEach(function() {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it("should add a service correctly", function() {
            ServiceStore.services = [];

            $httpBackend.when('POST', ADD_SERVICE_ENDPOINT).respond(serviceWithOneLayer);

            var params = {
                "service_name": "north_carolina",
                "service_url": "http://services.nconemap.gov/arcgis/services/Imagery/Orthoimagery_Latest/ImageServer/WMSServer",
                "cache_service": true,
                "portal_add": false
            }

            ServiceStore.addService(params);
            $httpBackend.flush();
            expect(ServiceStore.services).toEqual([serviceWithOneLayer]);

        })

        it("should pull in the service list correctly", function() {

            ServiceStore.services = [];

            // when updating, should populate ServiceStore.services with multiple services correctly
            var httpHandler = $httpBackend.when('GET', SERVICE_LIST_ENDPOINT) // configure $http.get to respond with
                .respond([serviceWithOneLayer, serviceWithMultipleLayers]);   // with two services
            ServiceStore.populateServices();
            $httpBackend.flush();
            expect(ServiceStore.services).toEqual([serviceWithOneLayer, serviceWithMultipleLayers])

            // when reupdating, should populate ServiceStore.services with single service correctly
            httpHandler.respond([serviceWithOneLayer]); // reconfigure $http.get to respond with only a single service
            ServiceStore.populateServices();
            $httpBackend.flush();
            expect(ServiceStore.services).toEqual([serviceWithOneLayer]);
        })

        it("should delete services correctly", function() {
            ServiceStore.services = [serviceWithOneLayer, serviceWithMultipleLayers];

            // configure $http.get to respond with 200 OK status
            //
            // (an actual data response is not used by the deleteService function,
            // so we don't need to return actual data for our GET response.
            // deleteService knows if a delete on the backend was successful simply
            // by checking if the return number is a success code. 200 is a success
            // code.)
            //
            var httpHandler = $httpBackend.when('POST', DELETE_SERVICE_ENDPOINT)
                .respond(200);

            // when updating, should populate ServiceStore.services with multiple services correctly
            ServiceStore.deleteService(serviceWithOneLayer);
            $httpBackend.flush();

            // the service with one layer should now be deleted
            expect(ServiceStore.services).toEqual([serviceWithMultipleLayers])

            // delete the final service
            ServiceStore.deleteService(serviceWithMultipleLayers);
            $httpBackend.flush();
            // the service list should now be empty, as all services have been deleted
            expect(ServiceStore.services).toEqual([])
        })
    })

    it("", function() {


    })

});
