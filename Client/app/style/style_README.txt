All styling information is stored in partial files in sass_partials\

If adding a new .scss file to sass_partials\, make sure to add an @import
statement for that new file in app.scss.  Without doing this, styles in that
file will not affect the UI.

Each file styles a different part of the UI.  After "npm start" is called,
app.css will constantly be updated to reflect changes in the scss partials.

app.min.css is only built once, each time "npm start" is called.  It should
only be used for production.  use app.css for development :)
