/**
 on the terminal in the Client directory, run:
    node find_todos.js

 This will print a nice list of all occurences in the frontend where any of the
 following keywords are used:
      NOTE OPTIMIZE TODO HACK XXX FIXME BUG

 Examples:

 // NOTE: This is the sample output for a note!
 // OPTIMIZE (FilmCoder): This is the sample output for an optimize with an author!
 // TODO: This is the sample output for a todo!
 // HACK: This is the sample output for a hack! Don't commit hacks!
 // XXX: This is the sample output for a XXX! XXX's need attention too!
 // FIXME (the intern guy): This is the sample output for a fixme! Seriously fix this...
 // BUG: This is the sample output for a bug! Who checked in a bug?!
*/

var fixme = require('fixme');

fixme({
  path:                 process.cwd(),
  ignored_directories:  ['node_modules/**',
                         'app/bower_components/**',
                         'app/coverage/**',
                         'find_todos.js',
                         '.git/**',
                         '.hg/**'],
  file_patterns:        ['**/*.js', 'Makefile', '**/*.sh'],
  file_encoding:        'utf8',
  line_length_limit:    1000
});
