# PEP Angular client code

## Getting Started

Install node https://nodejs.org/en/download/

Install bower
```
npm install -g bower
npm install -g jshint
```
(note that adding -g installs these libraries globally, so all projects have access to them)

Start Server
```
npm start
```
Use this for production.

### Start Server with SCSS auto-compiling:
```
npm run start-local
```
This means that if a scss style file is modified while the frontend is running,
the node-sass compiler will automatically recompile it into app.css.
Use this is you are actively developing the frontend.

## Compiling Scss Files

`npm start` automatically compiles all of our .scss files into a single app.css
file.  However, to do this manually, navigate to pep/Client and run:
```
npm run build-app-css
```

## Adding a new dependency
```
bower install X
```
For front-end packages:
    - that need to be in both production and dev builds:
        - bower install X --save
    - that only need to be in dev builds
        - bower install X --save-dev

This installs the dependency in the app/bower_components directory
Now add the dependency in index.html. You might also need to add it as a module in app.js.


## Before submitting a pull request

Run jshint to confirm you wrote quality js
```
jshint app
```

Make sure your code doesn't break any existing unit tests
```
npm test
```

Create unit tests for new functionality. At least add a placeholder and description of the tests that are needed.

## Tests

### Run Tests

To run the tests, navigate to pep/Client, and in the terminal run:
```
npm test
```

In the terminal you will see a list of all the tests which passed and failed.

### View Coverage

In Client/app/tests/coverage, you will find one or more folders representing each
browser the tests were run in.  Go into one of the folders, then open index.html
in your browser.  You will see test coverage.

### Write Unit Tests

We use the Jasmine-Karma framework to for our unit tests.

For each xxx.js file, we create another file in the same directory named
xxx.test.js

Whenever a new test is added, we must add additional entries to karma.conf.js
so include that test.
  1. In the "files" list, find your xxx.js file.  If it is not included, add it.
  2. Then add your xxx.test.js file directoy after your xxx.js file.  The test file
     MUST come after the code it is testing on, as test has a dependency on the code
     it is testing.
  3. Add a coverage entry for xxx.js to the "preprocessors" entry.

TODO write a package which scans the directory and can differentiate test files versus
non test files, then include all files in the right order, if possible.  The ultimate
goal is to make karma.conf.js self-maintaining.  note: use minimatch for glob expansion

There are plenty of tutorials online which can help you get started.  In particular,
search for "how to write unit tests for Angular with Jasmine and Karma"

### Debugging Unit Tests

For Karma to monitor code coverage, it needs to sprinkle checking code all throughout
our source, which obfuscates it severely.  To effectly debug, you'll need to go turn
code coverage off.
  1. In karma.conf.js, comment out all entries with the value 'coverages'.
  2. Run "npm test" in your terminal.
  3. Notice that a browser opens.  Click the "Debug" button on the browser.
  4. Notice a new debug tab opens.  Open the debugger for your browser.
  5. Refresh the page, and you'll see all the test run in the console.  Feel free
     to add breakpoints to your test files, or even to your source code being
     tested.
