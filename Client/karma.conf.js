//jshint strict: false

//NOTE: turning code coverage on sprinkles extra code into your tests for
// coverage detection.  turn coverage off when debugging tests
module.exports = function(config) {
  config.set({

    basePath: './app',

    files: [
      'bower_components/jquery/dist/jquery.min.js',
      'bower_components/angular/angular.js',
      'bower_components/angular-route/angular-route.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'bower_components/angular-toastr/dist/angular-toastr.js',
      'bower_components/leaflet/dist/leaflet.js',
      'bower_components/leaflet-draw/dist/leaflet.draw.js',
      'bower_components/angularjs-slider/dist/rzslider.js',
      'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
      'bower_components/moment/moment.js',
      'bower_components/angular-moment/angular-moment.js',

      'app.js',

      'components/map/map.js',
      'components/map/leaflet_controller.js',
      'components/map/add_service_modal.js',
      'components/map/seeding_packaging_cleaning_modals.js',
      'components/map/service_info_modal.js',
      'components/map/service_info_modal.test.js',
      'components/progress/progress_control.js',
      'components/progress/progress_control.test.js',
      'components/import-package-overlay/import-overlay-service.js',

      'shared/utilities.js',
      'shared/utilities.test.js',
      'shared/server.js',
      'shared/service.store.js',
      'shared/service.store.test.js',
      'shared/job.factory.js',
      'shared/job.factory.test.js',

      'components/side-nav/side-nav.js',
      'components/side-nav/side-nav.test.js',
      'components/settings-panel/settings-panel.js',
      'components/settings-panel/settings-panel.test.js'

      // 'shared/server.js',
      // 'shared/utilities.js',
      // 'shared/*.js',
      // 'components/map/map.js', // imported first to initialize the modules for testing
      // 'shared/service.store.js',
      // 'components/settings-panel/settings-panel.js',
      // 'components/settings-panel/settings-panel.test.js'
    ],

    preprocessors: {
      'components/settings-panel/settings-panel.js': 'coverage',
      'shared/utilities.js':             'coverage',
      'shared/job.factory.js':           'coverage',
      'shared/service.store.js':         'coverage',
      'components/side-nav/side-nav.js': 'coverage',
      'components/progress/progress_control.js': 'coverage',
      'components/map/service_info_modal.js': 'coverage'
    },

    autoWatch: true,

    frameworks: ['jasmine'],

    browsers: ['PhantomJS'],
    // browsers: ['Chrome'],

    plugins: [
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-jasmine',
      'karma-junit-reporter',
      'karma-spec-reporter',
      'karma-coverage',
      'karma-jasmine-diff-reporter',
      'karma-phantomjs-launcher'
    ],

    coverageReporter: {
        dir: 'tests',
        subdir:'coverage',
        reporters: [
          {type: 'text-summary'}
        ]
    },

    junitReporter: {
      outputDir: 'tests/junit',
      outputFile: 'unit.xml',
      suite: 'unit'
    },

    // prettifies output (ugly and no indentation without)
    jasmineDiffReporter: {
        pretty: true,
        json: true,
        multiline: true,
    },
    phantomjsLauncher: {
      exitOnResourceError: true
    },
    //reporters: ['jasmine-diff', 'spec']
    reporters: ['jasmine-diff', 'spec', 'coverage']


  });
};
